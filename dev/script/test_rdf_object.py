#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os

import graphviz
import networkx as nx
from networkx.drawing.nx_agraph import write_dot
import pathlib
import rdflib
import sys,time,copy,json
import pydot
from xsdata.models.enums import DataType
from xsdata.models.enums import QNames
from xsdata.formats.converter import QNameConverter
from xsdata.utils.namespaces import build_qname

from jinja2 import Environment, FileSystemLoader
from SPARQLWrapper import SPARQLWrapper, JSON
from rdflib import Graph, URIRef, Namespace, RDF, Literal
import rdfextras
from urllib.parse import urldefrag
import textwrap
import importlib
import dill
import traceback


from rdfobj import *

 

from owlready2 import *

def describe_ontology(ontofile):
  print(ontofile)
  onto = get_ontology("file://"+ontofile).load()
  #onto = Ontology("http://test.org/onto.owl")
  #onto= get_ontology("http://test.org/onto.owl").load()
  #print(dir(onto))
  cllist=list(onto.classes())
  for cl in cllist:
    print("==================")
    print(cl.name)
    print(cl.is_a)
    print(list(cl.subclasses()))
    print(cl.ancestors())
    #print(dir(cl))
    #print(cl.__dict__)



  proplist=list(onto.object_properties())
  for prop in proplist:
    print("  obj prop:%s , domain:%s, range:%s"%(prop,prop.domain,prop.range))
    
  proplist=list(onto.data_properties())
  for prop in proplist:
    print("  data prop:%s , domain:%s, range:%s"%(prop,prop.domain,prop.range))
    
    #print(dir(cl))

 
 
ontofilename='biopax-level3.owl'

ontofilePath = pathlib.Path().resolve().parent.absolute() / 'input/' / ontofilename
ontofile ="%s" %(ontofilePath)
describe_ontology(ontofile)

 


# In[ ]:




