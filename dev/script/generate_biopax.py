import os

 
import pathlib
 
import sys,time,copy,json
 
from urllib.parse import urldefrag
import textwrap
import importlib
import shutil
import traceback
print("==starting reading the OWL specification==")
 
############main


datasetPath = pathlib.Path().resolve().parent.absolute() / 'input/biopax-level3.owl'
datasetPathstr ="%s" %(datasetPath)
 
 
scriptDir = pathlib.Path().resolve().absolute()
     
domain="http://www.biopax.org/release/biopax-level3.owl#"
prefix="biopax"
print("=========2")

try:
  from rdfobj  import ModelProcessor, ModelCodeGenerator

  mp=ModelProcessor(datasetPathstr,scriptDir)
  print("=========3")
  mp.createObjectModelGraph(domain,prefix)
  mp.defineClassHierarchy()

  for k in mp.classDict.keys():
    cm=mp.classDict[k]
    if "<" not in k:
      print("--------")
      print("k:  ",k)
      print(cm.toJSON())

    
    
  dpath = pathlib.Path().resolve().parent.absolute() / 'input/mp.dill'
  dpath="%s" %(dpath)     

  mp.dump_meta_model(dpath)




  package_name="biopax"
  mcodeg=ModelCodeGenerator(package_name,mp.classDict,mp.prefix,mp.domain)
  mcodeg.prepare_codegen()
  mcodeg.codegen()
  shutil.rmtree("biopax_explorer/biopax") 
  shutil.move("biopax", "biopax_explorer/")
  print("==end==")
except Exception as error:
    print(error)
   


