import rdfobj  as rdfo
from biopax_explorer.biopax import *
from biopax_explorer.biopax.utils.validate_utils import CValidateArgType,raise_error

import unittest
from unittest.mock import Mock

# Mocking the biopax.Entity class for the tests
class MockEntity:
    pass

# Mocking the raise_error function for testing
def test_raise_error(msg, func_name, logger):
    raise Exception('error in %s: %s' % (func_name, msg))

# Assume validator and set_controlled functions are defined as per the provided code

fullvalidator = CValidateArgType(test_raise_error, logger=None,
                             disable_nullable=False,   disable_list=False, 
                             disable_min=False,   disable_max=False)
validator = CValidateArgType(test_raise_error, logger=None,
                             disable_nullable=True,   disable_list=True, 
                             disable_min=True,   disable_max=True
                             )
 
@validator(value='biopax.Entity',nullable=True)
def set_controlled(self, value):
    self._controlled = value

 
@fullvalidator(value='biopax.Protein',nullable=False)
def set_not_nullable_controlled(self, value):
   
    self._controlled = value

@validator(value='biopax.Entity', list=True, min=2, max=10)
def set_controlled_list(self, value):
    self._controlled_list = value




class TestSetControlled(unittest.TestCase):
    
    def setUp(self):
        self.obj = Mock()
        self.obj.set_controlled = set_controlled.__get__(self.obj)
        self.obj.set_controlled_list = set_controlled_list.__get__(self.obj)
        self.obj.set_not_nullable_controlled = set_not_nullable_controlled.__get__(self.obj)

    def test_valid_entity(self):
        try:
            self.obj.set_controlled(Protein())
        except Exception as e:
            self.fail(f"set_controlled raised Exception unexpectedly: {e}")

    def test_invalid_entity(self):
        with self.assertRaises(Exception) as context:
            self.obj.set_controlled(MockEntity())
        self.assertFalse('has the type <class \'MockEntity\'>, not <class \'biopax.Entity\'>' in str(context.exception))

    def test_none_value_allowed(self):
        try:
            self.obj.set_controlled(None)
        except Exception as e:
            self.fail(f"set_controlled raised Exception unexpectedly: {e}")
        
 
    def test_none_value_not_allowed(self):
        te=0
        msg=""
        try:
            self.obj.set_not_nullable_controlled(None)
        except Exception as e:
            te=1 
            msg=f"{e}"
        if te==0:
           self.fail(f" set_not_nullable_controlled allows none value !  {msg}")
           
        
    
    def test_construction1(self):
        try:
                ent=Protein()
                c= Control()
                c.set_controlled(ent)
                print(c)

        except Exception as e:
            self.fail(f"fail: {e}")

    def test_not_valid_entity_list(self):
        te=0
        msg=""
        try:
            self.obj.set_controlled_list(Protein())
        except Exception as e:
            te=1 
            msg=f"{e}"
        if te==0:
           self.fail(f" list allows set without list !  {msg}")
    
    def test_valid_entity_list_cardinality(self):

        te=0
        msg=""
        try:
            self.obj.set_controlled_list([Protein()])
        except Exception as e:
            te=1 
            msg=f"{e}"
        if te==0:
           self.fail(f"cardinality in list  not properly managed!  {msg}")
   

      
    def test_valid_entity_list1(self):
        try:
            self.obj.set_controlled_list([Protein(),Protein()])
            #print(self.obj)
        except Exception as e:
            self.fail(f"list disallows set   list !: {e}")      


    def test_valid_entity_list2(self):

        te=0
        msg=""
        try:
            self.obj.set_controlled_list([MockEntity(),MockEntity()])
        except Exception as e:
            te=1 
            msg=f"{e}"
        if te==0:
           self.fail(f"inner type in list not controlelled by validator !  {msg}")
   
        
if __name__ == '__main__':
    unittest.main()



