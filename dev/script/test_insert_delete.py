#!/usr/bin/env python
 
import os,pathlib

from SPARQLWrapper import SPARQLWrapper, JSON, POST, DIGEST
 


from rdfobj import *
 

#########################

dpath = pathlib.Path().resolve().parent.absolute() / 'input/mp.dill'
dpath="%s" %(dpath)          
classDict=load_cls_instance(dpath)

sc = StoreClient(classDict)

#########################


prefix="biopax3"
domain_schema_uri="http://www.biopax.org/release/biopax-level3.owl#"
dataset="g6p"
db="http://db:3030/%s/query" %(dataset)
unwanted_subject_uri="http://localhost:3030/%s/data" %(dataset)
limit=1000

expath = pathlib.Path().resolve().parent.absolute() 
exfile="%s/input/export_%s.xml" % (expath,dataset) 



g=sc.store_to_graph(db,prefix,domain_schema_uri,unwanted_subject_uri,limit)
sc.save_graph_as_xml(exfile)
print("__end__")




dataset="test"
db="http://db:3030/%s/update" %(dataset)

sparql = SPARQLWrapper(db)
#sparql.setHTTPAuth(DIGEST)
sparql.setCredentials("admin", "VyADgCvgP54l0vm")
sparql.setMethod(POST)

prefix="eos"
domain="http://www.gruppomcr.com/2020/06/eos-ontology-meets#"
uri_id="eos:barilla2"

sc.delete_from_store_by_uri_id(sparql,uri_id,prefix,domain)


 

from codegen  import *




  
   
voc=RelationshipTypeVocabulary(pk="http://localhost:3030/g6p/RelationshipTypeVocabulary_ac7de6f2f302971b64781fc96cc97c86" ,comment="no_comment")



rel=RelationshipXref(pk="http://www.reactome.org/biopax/56/71387#RelationshipXref90")
rel.set_comment("Database 'x' identifier. Use this URL to connect to the web page of this instance in Reactome")
rel.set_relationshiptype(voc)
rel.set_db("database1")
             

dataset="test"
db="http://db:3030/%s/update" %(dataset)

sparql = SPARQLWrapper(db)
sparql.setCredentials("admin", "VyADgCvgP54l0vm")
sparql.setMethod(POST)



sc.insert_instance(sparql,rel)

sc.update_or_insert_instance(sparql,rel)


   

