##generated class SequenceModificationVocabulary
#############################
##   Definiiton: A reference to the PSI Molecular Interaction ontology (MI) of
##   covalent sequence modifications. Homepage at http://www.psidev.info/.  Browse at
##   http://www.ebi.ac.uk/ontology-
##   lookup/browse.do?ontName=MI&termId=MI%3A0252&termName=biological%20feature. Only
##   children that are covelent modifications at specific positions can be used.

##############################
 
from codegen.controlledvocabulary import ControlledVocabulary
##############################
 



from codegen.class_utils import tostring
from codegen.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class SequenceModificationVocabulary(ControlledVocabulary) :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        super().__init__(*args, **kwargs) 
        self.rdf_type="http://www.biopax.org/release/biopax-level3.owl#SequenceModificationVocabulary"
  

##########getter
  
##########setter
  




    def object_attributes(self):

      object_attribute_list=super().object_attributes() 
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=super().type_attributes() 
      return type_attribute_list
 


