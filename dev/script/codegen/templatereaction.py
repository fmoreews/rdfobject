##generated class TemplateReaction
#############################
##   Definiton: An interaction where a macromolecule is polymerized from a
##   template macromolecule.   Rationale: This is an abstraction over multiple (not
##   explicitly stated) biochemical      reactions. The ubiquitous molecules (NTP and
##   amino acids) consumed are also usually     omitted. Template reaction is non-
##   stoichiometric, does not obey law of      mass conservation and temporally non-
##   atomic. It, however, provides a      mechanism to capture processes that are
##   central to all living organisms.    Usage: Regulation of TemplateReaction, e.g.
##   via a transcription factor can be      captured using
##   TemplateReactionRegulation. TemplateReaction can also be      indirect  for
##   example, it is not necessary to represent intermediary mRNA      for describing
##   expression of a protein. It was decided to not subclass      TemplateReaction to
##   subtypes such as transcription of translation for the      sake of  simplicity.
##   If needed these subclasses can be added in the      future.   Examples:
##   Transcription, translation, replication, reverse transcription. E.g.      DNA to
##   RNA is transcription, RNA to protein is translation and DNA to      protein is
##   protein expression from DNA.

##############################
 
from codegen.interaction import Interaction
##############################
 



from codegen.class_utils import tostring
from codegen.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class TemplateReaction(Interaction) :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        super().__init__(*args, **kwargs) 
        self.rdf_type="http://www.biopax.org/release/biopax-level3.owl#TemplateReaction"
##   The product of a template reaction.

        self._product=kwargs.get('product',None)  
##   The template molecule that is used in this template reaction.

        self._template=kwargs.get('template',None)  
##   The direction of the template reaction on the template.

        self._templateDirection=kwargs.get('templateDirection',None)  
  

##########getter
     
    def get_product(self):
        return self._product  
     
    def get_template(self):
        return self._template  
     
    def get_templateDirection(self):
        return self._templateDirection  
  
##########setter
    
    @validator(value='codegen.Entity')  
 
    def set_product(self,value):
        self._product=value  
    
    @validator(value='codegen.Entity')  
 
    def set_template(self,value):
        self._template=value  
    
    @validator(value=str)  
 
    def set_templateDirection(self,value):
        self._templateDirection=value  
  




    def object_attributes(self):

      object_attribute_list=super().object_attributes() 
      satt=['product', 'template']
      for elem in satt:
        object_attribute_list.append(elem)
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=super().type_attributes() 
      satt=['templateDirection']
      for elem in satt:
        type_attribute_list.append(elem)
      return type_attribute_list
 


