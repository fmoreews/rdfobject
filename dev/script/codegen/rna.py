##generated class Rna
#############################
##   Definition: A physical entity consisting of a sequence of ribonucleotide
##   monophosphates; a ribonucleic acid. Usage: RNA should be used for pools of
##   individual RNA molecules. For describing subregions on those molecules use
##   RNARegion. Examples: messengerRNA, microRNA, ribosomalRNA. A specific example is
##   the let-7 microRNA.

##############################
 
from codegen.physicalentity import PhysicalEntity
##############################
 



from codegen.class_utils import tostring
from codegen.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class Rna(PhysicalEntity) :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        super().__init__(*args, **kwargs) 
        self.rdf_type="http://www.biopax.org/release/biopax-level3.owl#Rna"
##   Reference entity for this physical entity.

        self._entityReference=kwargs.get('entityReference',None)  
  

##########getter
     
    def get_entityReference(self):
        return self._entityReference  
  
##########setter
    
    @validator(value='codegen.EntityReference')  
 
    def set_entityReference(self,value):
        self._entityReference=value  
  




    def object_attributes(self):

      object_attribute_list=super().object_attributes() 
      satt=['entityReference']
      for elem in satt:
        object_attribute_list.append(elem)
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=super().type_attributes() 
      return type_attribute_list
 


