##generated class BiochemicalPathwayStep
#############################
##   Definition: Imposes ordering on a step in a biochemical pathway.  Retionale: A
##   biochemical reaction can be reversible by itself, but can be physiologically
##   directed in the context of a pathway, for instance due to flux of reactants and
##   products.  Usage: Only one conversion interaction can be ordered at a time, but
##   multiple catalysis or modulation instances can be part of one step.

##############################
 
from codegen.pathwaystep import PathwayStep
##############################
 



from codegen.class_utils import tostring
from codegen.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class BiochemicalPathwayStep(PathwayStep) :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        super().__init__(*args, **kwargs) 
        self.rdf_type="http://www.biopax.org/release/biopax-level3.owl#BiochemicalPathwayStep"
##   The central process that take place at this step of the biochemical pathway.

        self._stepConversion=kwargs.get('stepConversion',None)  
##   Direction of the conversion in this particular pathway context.  This property
##   can be used for annotating direction of enzymatic activity. Even if an enzyme
##   catalyzes a reaction reversibly, the flow of matter through the pathway will
##   force the equilibrium in a given direction for that particular pathway.

        self._stepDirection=kwargs.get('stepDirection',None)  
  

##########getter
     
    def get_stepConversion(self):
        return self._stepConversion  
     
    def get_stepDirection(self):
        return self._stepDirection  
  
##########setter
    
    @validator(value='codegen.Conversion')  
 
    def set_stepConversion(self,value):
        self._stepConversion=value  
    
    @validator(value=str)  
 
    def set_stepDirection(self,value):
        self._stepDirection=value  
  




    def object_attributes(self):

      object_attribute_list=super().object_attributes() 
      satt=['stepConversion']
      for elem in satt:
        object_attribute_list.append(elem)
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=super().type_attributes() 
      satt=['stepDirection']
      for elem in satt:
        type_attribute_list.append(elem)
      return type_attribute_list
 


