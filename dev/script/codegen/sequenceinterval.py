##generated class SequenceInterval
#############################
##   Definition: An interval on a sequence.  Usage: Interval is defined as an ordered
##   pair of SequenceSites. All of the sequence from the begin site to the end site
##   (inclusive) is described, not any subset.

##############################
 
from codegen.sequencelocation import SequenceLocation
##############################
 



from codegen.class_utils import tostring
from codegen.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class SequenceInterval(SequenceLocation) :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        super().__init__(*args, **kwargs) 
        self.rdf_type="http://www.biopax.org/release/biopax-level3.owl#SequenceInterval"
##   The begin position of a sequence interval.

        self._sequenceIntervalBegin=kwargs.get('sequenceIntervalBegin',None)  
##   The end position of a sequence interval.

        self._sequenceIntervalEnd=kwargs.get('sequenceIntervalEnd',None)  
  

##########getter
     
    def get_sequenceIntervalBegin(self):
        return self._sequenceIntervalBegin  
     
    def get_sequenceIntervalEnd(self):
        return self._sequenceIntervalEnd  
  
##########setter
    
    @validator(value='codegen.SequenceSite')  
 
    def set_sequenceIntervalBegin(self,value):
        self._sequenceIntervalBegin=value  
    
    @validator(value='codegen.SequenceSite')  
 
    def set_sequenceIntervalEnd(self,value):
        self._sequenceIntervalEnd=value  
  




    def object_attributes(self):

      object_attribute_list=super().object_attributes() 
      satt=['sequenceIntervalBegin', 'sequenceIntervalEnd']
      for elem in satt:
        object_attribute_list.append(elem)
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=super().type_attributes() 
      return type_attribute_list
 


