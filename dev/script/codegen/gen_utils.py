

#utilities to manipulate the classes generated 

##from codegen.interaction import Interaction
from codegen.interaction import Interaction
##from codegen.conversion import Conversion
from codegen.conversion import Conversion
##from codegen.dnareference import Dnareference
from codegen.dnareference import DnaReference
##from codegen.fragmentfeature import Fragmentfeature
from codegen.fragmentfeature import FragmentFeature
##from codegen.complexassembly import Complexassembly
from codegen.complexassembly import ComplexAssembly
##from codegen.biosource import Biosource
from codegen.biosource import BioSource
##from codegen.stoichiometry import Stoichiometry
from codegen.stoichiometry import Stoichiometry
##from codegen.catalysis import Catalysis
from codegen.catalysis import Catalysis
##from codegen.protein import Protein
from codegen.protein import Protein
##from codegen.entityfeature import Entityfeature
from codegen.entityfeature import EntityFeature
##from codegen.degradation import Degradation
from codegen.degradation import Degradation
##from codegen.templatereaction import Templatereaction
from codegen.templatereaction import TemplateReaction
##from codegen.experimentalformvocabulary import Experimentalformvocabulary
from codegen.experimentalformvocabulary import ExperimentalFormVocabulary
##from codegen.deltag import Deltag
from codegen.deltag import DeltaG
##from codegen.covalentbindingfeature import Covalentbindingfeature
from codegen.covalentbindingfeature import CovalentBindingFeature
##from codegen.biochemicalreaction import Biochemicalreaction
from codegen.biochemicalreaction import BiochemicalReaction
##from codegen.molecularinteraction import Molecularinteraction
from codegen.molecularinteraction import MolecularInteraction
##from codegen.transportwithbiochemicalreaction import Transportwithbiochemicalreaction
from codegen.transportwithbiochemicalreaction import TransportWithBiochemicalReaction
##from codegen.relationshiptypevocabulary import Relationshiptypevocabulary
from codegen.relationshiptypevocabulary import RelationshipTypeVocabulary
##from codegen.transport import Transport
from codegen.transport import Transport
##from codegen.complex import Complex
from codegen.complex import Complex
##from codegen.xref import Xref
from codegen.xref import Xref
##from codegen.cellvocabulary import Cellvocabulary
from codegen.cellvocabulary import CellVocabulary
##from codegen.smallmolecule import Smallmolecule
from codegen.smallmolecule import SmallMolecule
##from codegen.score import Score
from codegen.score import Score
##from codegen.rnaregionreference import Rnaregionreference
from codegen.rnaregionreference import RnaRegionReference
##from codegen.modulation import Modulation
from codegen.modulation import Modulation
##from codegen.relationshipxref import Relationshipxref
from codegen.relationshipxref import RelationshipXref
##from codegen.bindingfeature import Bindingfeature
from codegen.bindingfeature import BindingFeature
##from codegen.experimentalform import Experimentalform
from codegen.experimentalform import ExperimentalForm
##from codegen.control import Control
from codegen.control import Control
##from codegen.publicationxref import Publicationxref
from codegen.publicationxref import PublicationXref
##from codegen.sequencesite import Sequencesite
from codegen.sequencesite import SequenceSite
##from codegen.controlledvocabulary import Controlledvocabulary
from codegen.controlledvocabulary import ControlledVocabulary
##from codegen.kprime import Kprime
from codegen.kprime import KPrime
##from codegen.interactionvocabulary import Interactionvocabulary
from codegen.interactionvocabulary import InteractionVocabulary
##from codegen.dnaregionreference import Dnaregionreference
from codegen.dnaregionreference import DnaRegionReference
##from codegen.pathway import Pathway
from codegen.pathway import Pathway
##from codegen.physicalentity import Physicalentity
from codegen.physicalentity import PhysicalEntity
##from codegen.provenance import Provenance
from codegen.provenance import Provenance
##from codegen.rna import Rna
from codegen.rna import Rna
##from codegen.modificationfeature import Modificationfeature
from codegen.modificationfeature import ModificationFeature
##from codegen.dnaregion import Dnaregion
from codegen.dnaregion import DnaRegion
##from codegen.chemicalstructure import Chemicalstructure
from codegen.chemicalstructure import ChemicalStructure
##from codegen.proteinreference import Proteinreference
from codegen.proteinreference import ProteinReference
##from codegen.smallmoleculereference import Smallmoleculereference
from codegen.smallmoleculereference import SmallMoleculeReference
##from codegen.geneticinteraction import Geneticinteraction
from codegen.geneticinteraction import GeneticInteraction
##from codegen.tissuevocabulary import Tissuevocabulary
from codegen.tissuevocabulary import TissueVocabulary
##from codegen.sequenceregionvocabulary import Sequenceregionvocabulary
from codegen.sequenceregionvocabulary import SequenceRegionVocabulary
##from codegen.phenotypevocabulary import Phenotypevocabulary
from codegen.phenotypevocabulary import PhenotypeVocabulary
##from codegen.unificationxref import Unificationxref
from codegen.unificationxref import UnificationXref
##from codegen.biochemicalpathwaystep import Biochemicalpathwaystep
from codegen.biochemicalpathwaystep import BiochemicalPathwayStep
##from codegen.sequencemodificationvocabulary import Sequencemodificationvocabulary
from codegen.sequencemodificationvocabulary import SequenceModificationVocabulary
##from codegen.sequenceinterval import Sequenceinterval
from codegen.sequenceinterval import SequenceInterval
##from codegen.entityreferencetypevocabulary import Entityreferencetypevocabulary
from codegen.entityreferencetypevocabulary import EntityReferenceTypeVocabulary
##from codegen.dna import Dna
from codegen.dna import Dna
##from codegen.cellularlocationvocabulary import Cellularlocationvocabulary
from codegen.cellularlocationvocabulary import CellularLocationVocabulary
##from codegen.evidence import Evidence
from codegen.evidence import Evidence
##from codegen.pathwaystep import Pathwaystep
from codegen.pathwaystep import PathwayStep
##from codegen.sequencelocation import Sequencelocation
from codegen.sequencelocation import SequenceLocation
##from codegen.evidencecodevocabulary import Evidencecodevocabulary
from codegen.evidencecodevocabulary import EvidenceCodeVocabulary
##from codegen.gene import Gene
from codegen.gene import Gene
##from codegen.rnareference import Rnareference
from codegen.rnareference import RnaReference
##from codegen.entityreference import Entityreference
from codegen.entityreference import EntityReference
##from codegen.templatereactionregulation import Templatereactionregulation
from codegen.templatereactionregulation import TemplateReactionRegulation
##from codegen.rnaregion import Rnaregion
from codegen.rnaregion import RnaRegion
##from codegen.entity import Entity
from codegen.entity import Entity
##from codegen.utilityclass import Utilityclass
from codegen.utilityclass import UtilityClass
  



#return an instance of a class corresponding to the input keywork
def define_model_instance(clsn):


  if clsn is None:
    inst=None
  else:
    clsn=str(clsn).strip().lower()
    if clsn =='':
      inst=None 
    elif clsn.lower() == 'interaction':
      inst = Interaction() 
    elif clsn.lower() == 'conversion':
      inst = Conversion() 
    elif clsn.lower() == 'dnareference':
      inst = Dnareference() 
    elif clsn.lower() == 'fragmentfeature':
      inst = Fragmentfeature() 
    elif clsn.lower() == 'complexassembly':
      inst = Complexassembly() 
    elif clsn.lower() == 'biosource':
      inst = Biosource() 
    elif clsn.lower() == 'stoichiometry':
      inst = Stoichiometry() 
    elif clsn.lower() == 'catalysis':
      inst = Catalysis() 
    elif clsn.lower() == 'protein':
      inst = Protein() 
    elif clsn.lower() == 'entityfeature':
      inst = Entityfeature() 
    elif clsn.lower() == 'degradation':
      inst = Degradation() 
    elif clsn.lower() == 'templatereaction':
      inst = Templatereaction() 
    elif clsn.lower() == 'experimentalformvocabulary':
      inst = Experimentalformvocabulary() 
    elif clsn.lower() == 'deltag':
      inst = Deltag() 
    elif clsn.lower() == 'covalentbindingfeature':
      inst = Covalentbindingfeature() 
    elif clsn.lower() == 'biochemicalreaction':
      inst = Biochemicalreaction() 
    elif clsn.lower() == 'molecularinteraction':
      inst = Molecularinteraction() 
    elif clsn.lower() == 'transportwithbiochemicalreaction':
      inst = Transportwithbiochemicalreaction() 
    elif clsn.lower() == 'relationshiptypevocabulary':
      inst = Relationshiptypevocabulary() 
    elif clsn.lower() == 'transport':
      inst = Transport() 
    elif clsn.lower() == 'complex':
      inst = Complex() 
    elif clsn.lower() == 'xref':
      inst = Xref() 
    elif clsn.lower() == 'cellvocabulary':
      inst = Cellvocabulary() 
    elif clsn.lower() == 'smallmolecule':
      inst = Smallmolecule() 
    elif clsn.lower() == 'score':
      inst = Score() 
    elif clsn.lower() == 'rnaregionreference':
      inst = Rnaregionreference() 
    elif clsn.lower() == 'modulation':
      inst = Modulation() 
    elif clsn.lower() == 'relationshipxref':
      inst = Relationshipxref() 
    elif clsn.lower() == 'bindingfeature':
      inst = Bindingfeature() 
    elif clsn.lower() == 'experimentalform':
      inst = Experimentalform() 
    elif clsn.lower() == 'control':
      inst = Control() 
    elif clsn.lower() == 'publicationxref':
      inst = Publicationxref() 
    elif clsn.lower() == 'sequencesite':
      inst = Sequencesite() 
    elif clsn.lower() == 'controlledvocabulary':
      inst = Controlledvocabulary() 
    elif clsn.lower() == 'kprime':
      inst = Kprime() 
    elif clsn.lower() == 'interactionvocabulary':
      inst = Interactionvocabulary() 
    elif clsn.lower() == 'dnaregionreference':
      inst = Dnaregionreference() 
    elif clsn.lower() == 'pathway':
      inst = Pathway() 
    elif clsn.lower() == 'physicalentity':
      inst = Physicalentity() 
    elif clsn.lower() == 'provenance':
      inst = Provenance() 
    elif clsn.lower() == 'rna':
      inst = Rna() 
    elif clsn.lower() == 'modificationfeature':
      inst = Modificationfeature() 
    elif clsn.lower() == 'dnaregion':
      inst = Dnaregion() 
    elif clsn.lower() == 'chemicalstructure':
      inst = Chemicalstructure() 
    elif clsn.lower() == 'proteinreference':
      inst = Proteinreference() 
    elif clsn.lower() == 'smallmoleculereference':
      inst = Smallmoleculereference() 
    elif clsn.lower() == 'geneticinteraction':
      inst = Geneticinteraction() 
    elif clsn.lower() == 'tissuevocabulary':
      inst = Tissuevocabulary() 
    elif clsn.lower() == 'sequenceregionvocabulary':
      inst = Sequenceregionvocabulary() 
    elif clsn.lower() == 'phenotypevocabulary':
      inst = Phenotypevocabulary() 
    elif clsn.lower() == 'unificationxref':
      inst = Unificationxref() 
    elif clsn.lower() == 'biochemicalpathwaystep':
      inst = Biochemicalpathwaystep() 
    elif clsn.lower() == 'sequencemodificationvocabulary':
      inst = Sequencemodificationvocabulary() 
    elif clsn.lower() == 'sequenceinterval':
      inst = Sequenceinterval() 
    elif clsn.lower() == 'entityreferencetypevocabulary':
      inst = Entityreferencetypevocabulary() 
    elif clsn.lower() == 'dna':
      inst = Dna() 
    elif clsn.lower() == 'cellularlocationvocabulary':
      inst = Cellularlocationvocabulary() 
    elif clsn.lower() == 'evidence':
      inst = Evidence() 
    elif clsn.lower() == 'pathwaystep':
      inst = Pathwaystep() 
    elif clsn.lower() == 'sequencelocation':
      inst = Sequencelocation() 
    elif clsn.lower() == 'evidencecodevocabulary':
      inst = Evidencecodevocabulary() 
    elif clsn.lower() == 'gene':
      inst = Gene() 
    elif clsn.lower() == 'rnareference':
      inst = Rnareference() 
    elif clsn.lower() == 'entityreference':
      inst = Entityreference() 
    elif clsn.lower() == 'templatereactionregulation':
      inst = Templatereactionregulation() 
    elif clsn.lower() == 'rnaregion':
      inst = Rnaregion() 
    elif clsn.lower() == 'entity':
      inst = Entity() 
    elif clsn.lower() == 'utilityclass':
      inst = Utilityclass() 
   
    else:
      inst=None
  

  return inst

#return an dictionary class_name->[children class_name]
def classes_children():

  mchildren=dict()
  mchildren['Interaction']= ['Conversion', 'ComplexAssembly', 'Degradation', 'BiochemicalReaction', 'TransportWithBiochemicalReaction', 'Transport', 'TransportWithBiochemicalReaction', 'TemplateReaction', 'MolecularInteraction', 'Control', 'Catalysis', 'Modulation', 'TemplateReactionRegulation', 'GeneticInteraction']
  mchildren['Conversion']= ['ComplexAssembly', 'Degradation', 'BiochemicalReaction', 'TransportWithBiochemicalReaction', 'Transport', 'TransportWithBiochemicalReaction']
  mchildren['DnaReference']= []
  mchildren['FragmentFeature']= []
  mchildren['ComplexAssembly']= []
  mchildren['BioSource']= []
  mchildren['Stoichiometry']= []
  mchildren['Catalysis']= []
  mchildren['Protein']= []
  mchildren['EntityFeature']= ['FragmentFeature', 'BindingFeature', 'CovalentBindingFeature', 'ModificationFeature', 'CovalentBindingFeature']
  mchildren['Degradation']= []
  mchildren['TemplateReaction']= []
  mchildren['ExperimentalFormVocabulary']= []
  mchildren['DeltaG']= []
  mchildren['CovalentBindingFeature']= []
  mchildren['BiochemicalReaction']= ['TransportWithBiochemicalReaction']
  mchildren['MolecularInteraction']= []
  mchildren['TransportWithBiochemicalReaction']= []
  mchildren['RelationshipTypeVocabulary']= []
  mchildren['Transport']= ['TransportWithBiochemicalReaction']
  mchildren['Complex']= []
  mchildren['Xref']= ['RelationshipXref', 'PublicationXref', 'UnificationXref']
  mchildren['CellVocabulary']= []
  mchildren['SmallMolecule']= []
  mchildren['Score']= []
  mchildren['RnaRegionReference']= []
  mchildren['Modulation']= []
  mchildren['RelationshipXref']= []
  mchildren['BindingFeature']= ['CovalentBindingFeature']
  mchildren['ExperimentalForm']= []
  mchildren['Control']= ['Catalysis', 'Modulation', 'TemplateReactionRegulation']
  mchildren['PublicationXref']= []
  mchildren['SequenceSite']= []
  mchildren['ControlledVocabulary']= ['ExperimentalFormVocabulary', 'RelationshipTypeVocabulary', 'CellVocabulary', 'InteractionVocabulary', 'TissueVocabulary', 'SequenceRegionVocabulary', 'PhenotypeVocabulary', 'SequenceModificationVocabulary', 'EntityReferenceTypeVocabulary', 'CellularLocationVocabulary', 'EvidenceCodeVocabulary']
  mchildren['KPrime']= []
  mchildren['InteractionVocabulary']= []
  mchildren['DnaRegionReference']= []
  mchildren['Pathway']= []
  mchildren['PhysicalEntity']= ['Protein', 'Complex', 'SmallMolecule', 'Rna', 'DnaRegion', 'Dna', 'RnaRegion']
  mchildren['Provenance']= []
  mchildren['Rna']= []
  mchildren['ModificationFeature']= ['CovalentBindingFeature']
  mchildren['DnaRegion']= []
  mchildren['ChemicalStructure']= []
  mchildren['ProteinReference']= []
  mchildren['SmallMoleculeReference']= []
  mchildren['GeneticInteraction']= []
  mchildren['TissueVocabulary']= []
  mchildren['SequenceRegionVocabulary']= []
  mchildren['PhenotypeVocabulary']= []
  mchildren['UnificationXref']= []
  mchildren['BiochemicalPathwayStep']= []
  mchildren['SequenceModificationVocabulary']= []
  mchildren['SequenceInterval']= []
  mchildren['EntityReferenceTypeVocabulary']= []
  mchildren['Dna']= []
  mchildren['CellularLocationVocabulary']= []
  mchildren['Evidence']= []
  mchildren['PathwayStep']= ['BiochemicalPathwayStep']
  mchildren['SequenceLocation']= ['SequenceSite', 'SequenceInterval']
  mchildren['EvidenceCodeVocabulary']= []
  mchildren['Gene']= []
  mchildren['RnaReference']= []
  mchildren['EntityReference']= ['DnaReference', 'RnaRegionReference', 'DnaRegionReference', 'ProteinReference', 'SmallMoleculeReference', 'RnaReference']
  mchildren['TemplateReactionRegulation']= []
  mchildren['RnaRegion']= []
  mchildren['Entity']= ['Interaction', 'Conversion', 'ComplexAssembly', 'Degradation', 'BiochemicalReaction', 'TransportWithBiochemicalReaction', 'Transport', 'TransportWithBiochemicalReaction', 'TemplateReaction', 'MolecularInteraction', 'Control', 'Catalysis', 'Modulation', 'TemplateReactionRegulation', 'GeneticInteraction', 'Pathway', 'PhysicalEntity', 'Protein', 'Complex', 'SmallMolecule', 'Rna', 'DnaRegion', 'Dna', 'RnaRegion', 'Gene']
  mchildren['UtilityClass']= ['BioSource', 'Stoichiometry', 'EntityFeature', 'FragmentFeature', 'BindingFeature', 'CovalentBindingFeature', 'ModificationFeature', 'CovalentBindingFeature', 'DeltaG', 'Xref', 'RelationshipXref', 'PublicationXref', 'UnificationXref', 'Score', 'ExperimentalForm', 'ControlledVocabulary', 'ExperimentalFormVocabulary', 'RelationshipTypeVocabulary', 'CellVocabulary', 'InteractionVocabulary', 'TissueVocabulary', 'SequenceRegionVocabulary', 'PhenotypeVocabulary', 'SequenceModificationVocabulary', 'EntityReferenceTypeVocabulary', 'CellularLocationVocabulary', 'EvidenceCodeVocabulary', 'KPrime', 'Provenance', 'ChemicalStructure', 'Evidence', 'PathwayStep', 'BiochemicalPathwayStep', 'SequenceLocation', 'SequenceSite', 'SequenceInterval', 'EntityReference', 'DnaReference', 'RnaRegionReference', 'DnaRegionReference', 'ProteinReference', 'SmallMoleculeReference', 'RnaReference']
  
  return mchildren

def class_children(cln):
   mchildren=classes_children()
   if cln in mchildren.keys():
      return mchildren[cln]
   return None   

 