##generated class Provenance
#############################
##   Definition: The direct source of pathway data or score. Usage: This does not
##   store the trail of sources from the generation of the data to this point, only
##   the last known source, such as a database, tool or algorithm. The xref property
##   may contain a publicationXref referencing a publication describing the data
##   source (e.g. a database publication). A unificationXref may be used when
##   pointing to an entry in a database of databases describing this database.
##   Examples: A database, scoring method or person name.

##############################
 
from codegen.utilityclass import UtilityClass
##############################
 



from codegen.class_utils import tostring
from codegen.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class Provenance(UtilityClass) :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        super().__init__(*args, **kwargs) 
        self.rdf_type="http://www.biopax.org/release/biopax-level3.owl#Provenance"
##   Values of this property define external cross-references from this entity to
##   entities in external databases.

        self._xref=kwargs.get('xref',None)  
##   An abbreviated name for this entity, preferably a name that is short enough to
##   be used in a visualization application to label a graphical element that
##   represents this entity. If no short name is available, an xref may be used for
##   this purpose by the visualization application.  Warning:  Subproperties of name
##   are functional, that is we expect to have only one standardName and shortName
##   for a given entity. If a user decides to assign a different name to standardName
##   or shortName, they have to remove the old triplet from the model too. If the old
##   name should be retained as a synonym a regular "name" property should also be
##   introduced with the old name.

        self._displayName=kwargs.get('displayName',None)  
##   Synonyms for this entity.  standardName and shortName are subproperties of this
##   property and if declared they are automatically considered as names.   Warning:
##   Subproperties of name are functional, that is we expect to have only one
##   standardName and shortName for a given entity. If a user decides to assign a
##   different name to standardName or shortName, they have to remove the old triplet
##   from the model too. If the old name should be retained as a synonym a regular
##   "name" property should also be introduced with the old name.

        self._name=kwargs.get('name',None)  
##   The preferred full name for this entity, if exists assigned by a standard
##   nomenclature organization such as HUGO Gene Nomenclature Committee.  Warning:
##   Subproperties of name are functional, that is we expect to have only one
##   standardName and shortName for a given entity. If a user decides to assign a
##   different name to standardName or shortName, they have to remove the old triplet
##   from the model too. If the old name should be retained as a synonym a regular
##   "name" property should also be introduced with the old name.

        self._standardName=kwargs.get('standardName',None)  
  

##########getter
     
    def get_xref(self):
        return self._xref  
     
    def get_displayName(self):
        return self._displayName  
     
    def get_name(self):
        return self._name  
     
    def get_standardName(self):
        return self._standardName  
  
##########setter
    
    @validator(value='codegen.Xref')  
 
    def set_xref(self,value):
        self._xref=value  
    
    @validator(value=str)  
 
    def set_displayName(self,value):
        self._displayName=value  
    
    @validator(value=str)  
 
    def set_name(self,value):
        self._name=value  
    
    @validator(value=str)  
 
    def set_standardName(self,value):
        self._standardName=value  
  




    def object_attributes(self):

      object_attribute_list=super().object_attributes() 
      satt=['xref']
      for elem in satt:
        object_attribute_list.append(elem)
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=super().type_attributes() 
      satt=['displayName', 'name', 'standardName']
      for elem in satt:
        type_attribute_list.append(elem)
      return type_attribute_list
 


