 
###documentation helper
class utilityclass_DocHelper():
    
  def __init__(self):
    self.dmap=self.definitions()

  def classInfo(self):
    cln='UtilityClass'
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       return m['class']
    return None
  
  def attributeNames(self):
    cln='UtilityClass'
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s\n" %(k)    
    return s

  def attributesInfo(self):
    cln='UtilityClass'
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s:" %(k)
         s+="\n%s" %(atm[attn])
    return s

  def attributeInfo(self,attn):
    cln='UtilityClass'
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       if attn in atm.keys():
          return atm[attn]
    return None

  def definitions(self):
    dmap=dict()
    ####################################
    # class UtilityClass
    dmap['UtilityClass']=dict()
    dmap['UtilityClass']['class']="""
Definition: This is a placeholder for classes, used for annotating the "Entity" and its subclasses. Mostly, these are not  an "Entity" themselves. Examples include references to external databases, controlled vocabularies, evidence and provenance.

Rationale: Utility classes are created when simple slots are insufficient to describe an aspect of an entity or to increase compatibility of this ontology with other standards.  

Usage: The utilityClass class is actually a metaclass and is only present to organize the other helper classes under one class hierarchy; instances of utilityClass should never be created.
    """
    dmap['UtilityClass']['attribute']=dict()
  
    dmap['UtilityClass']['attribute']['comment']="""
Comment on the data in the container class. This property should be used instead of the OWL documentation elements (rdfs:comment) for instances because information in 'comment' is data to be exchanged, whereas the rdfs:comment field is used for metadata about the structure of the BioPAX ontology.
    """
  
    return dmap