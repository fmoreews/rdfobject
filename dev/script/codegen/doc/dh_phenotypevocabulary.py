 
###documentation helper
class phenotypevocabulary_DocHelper():
    
  def __init__(self):
    self.dmap=self.definitions()

  def classInfo(self):
    cln='PhenotypeVocabulary'
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       return m['class']
    return None
  
  def attributeNames(self):
    cln='PhenotypeVocabulary'
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s\n" %(k)    
    return s

  def attributesInfo(self):
    cln='PhenotypeVocabulary'
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s:" %(k)
         s+="\n%s" %(atm[attn])
    return s

  def attributeInfo(self,attn):
    cln='PhenotypeVocabulary'
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       if attn in atm.keys():
          return atm[attn]
    return None

  def definitions(self):
    dmap=dict()
    ####################################
    # class PhenotypeVocabulary
    dmap['PhenotypeVocabulary']=dict()
    dmap['PhenotypeVocabulary']['class']="""
Definition: The phenotype measured in the experiment e.g. growth rate or viability of a cell. This is only the type, not the value e.g. for a synthetic lethal interaction, the phenotype is viability, specified by ID: PATO:0000169, "viability", not the value (specified by ID: PATO:0000718, "lethal (sensu genetics)". A single term in a phenotype controlled vocabulary can be referenced using the xref, or the PhenoXML describing the PATO EQ model phenotype description can be stored as a string in PATO-DATA.
    """
    dmap['PhenotypeVocabulary']['attribute']=dict()
  
    dmap['PhenotypeVocabulary']['attribute']['patoData']="""
The phenotype data from PATO, formatted as PhenoXML (defined at http://www.fruitfly.org/~cjm/obd/formats.html)
    """
    dmap['PhenotypeVocabulary']['attribute']['xref']="""
Values of this property define external cross-references from this entity to entities in external databases.
    """
    dmap['PhenotypeVocabulary']['attribute']['term']="""
The external controlled vocabulary term.
    """
  
    return dmap