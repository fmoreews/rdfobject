 
###documentation helper
class biochemicalpathwaystep_DocHelper():
    
  def __init__(self):
    self.dmap=self.definitions()

  def classInfo(self):
    cln='BiochemicalPathwayStep'
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       return m['class']
    return None
  
  def attributeNames(self):
    cln='BiochemicalPathwayStep'
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s\n" %(k)    
    return s

  def attributesInfo(self):
    cln='BiochemicalPathwayStep'
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s:" %(k)
         s+="\n%s" %(atm[attn])
    return s

  def attributeInfo(self,attn):
    cln='BiochemicalPathwayStep'
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       if attn in atm.keys():
          return atm[attn]
    return None

  def definitions(self):
    dmap=dict()
    ####################################
    # class BiochemicalPathwayStep
    dmap['BiochemicalPathwayStep']=dict()
    dmap['BiochemicalPathwayStep']['class']="""
Definition: Imposes ordering on a step in a biochemical pathway. 
Retionale: A biochemical reaction can be reversible by itself, but can be physiologically directed in the context of a pathway, for instance due to flux of reactants and products. 
Usage: Only one conversion interaction can be ordered at a time, but multiple catalysis or modulation instances can be part of one step.
    """
    dmap['BiochemicalPathwayStep']['attribute']=dict()
  
    dmap['BiochemicalPathwayStep']['attribute']['stepConversion']="""
The central process that take place at this step of the biochemical pathway.
    """
    dmap['BiochemicalPathwayStep']['attribute']['stepDirection']="""
Direction of the conversion in this particular pathway context. 
This property can be used for annotating direction of enzymatic activity. Even if an enzyme catalyzes a reaction reversibly, the flow of matter through the pathway will force the equilibrium in a given direction for that particular pathway.
    """
    dmap['BiochemicalPathwayStep']['attribute']['evidence']="""
Scientific evidence supporting the existence of the entity as described.
    """
    dmap['BiochemicalPathwayStep']['attribute']['nextStep']="""
The next step(s) of the pathway.  Contains zero or more pathwayStep instances.  If there is no next step, this property is empty. Multiple pathwayStep instances indicate pathway branching.
    """
    dmap['BiochemicalPathwayStep']['attribute']['stepConversion']="""
The central process that take place at this step of the biochemical pathway.
    """
    dmap['BiochemicalPathwayStep']['attribute']['stepProcess']="""
An interaction or a pathway that are a part of this pathway step.
    """
  
    return dmap