
#utilities to manipulate the doc classes 

 
 
from codegen.doc.dh_interaction import interaction_DocHelper
 
 
from codegen.doc.dh_conversion import conversion_DocHelper
 
 
from codegen.doc.dh_dnareference import dnareference_DocHelper
 
 
from codegen.doc.dh_fragmentfeature import fragmentfeature_DocHelper
 
 
from codegen.doc.dh_complexassembly import complexassembly_DocHelper
 
 
from codegen.doc.dh_biosource import biosource_DocHelper
 
 
from codegen.doc.dh_stoichiometry import stoichiometry_DocHelper
 
 
from codegen.doc.dh_catalysis import catalysis_DocHelper
 
 
from codegen.doc.dh_protein import protein_DocHelper
 
 
from codegen.doc.dh_entityfeature import entityfeature_DocHelper
 
 
from codegen.doc.dh_degradation import degradation_DocHelper
 
 
from codegen.doc.dh_templatereaction import templatereaction_DocHelper
 
 
from codegen.doc.dh_experimentalformvocabulary import experimentalformvocabulary_DocHelper
 
 
from codegen.doc.dh_deltag import deltag_DocHelper
 
 
from codegen.doc.dh_covalentbindingfeature import covalentbindingfeature_DocHelper
 
 
from codegen.doc.dh_biochemicalreaction import biochemicalreaction_DocHelper
 
 
from codegen.doc.dh_molecularinteraction import molecularinteraction_DocHelper
 
 
from codegen.doc.dh_transportwithbiochemicalreaction import transportwithbiochemicalreaction_DocHelper
 
 
from codegen.doc.dh_relationshiptypevocabulary import relationshiptypevocabulary_DocHelper
 
 
from codegen.doc.dh_transport import transport_DocHelper
 
 
from codegen.doc.dh_complex import complex_DocHelper
 
 
from codegen.doc.dh_xref import xref_DocHelper
 
 
from codegen.doc.dh_cellvocabulary import cellvocabulary_DocHelper
 
 
from codegen.doc.dh_smallmolecule import smallmolecule_DocHelper
 
 
from codegen.doc.dh_score import score_DocHelper
 
 
from codegen.doc.dh_rnaregionreference import rnaregionreference_DocHelper
 
 
from codegen.doc.dh_modulation import modulation_DocHelper
 
 
from codegen.doc.dh_relationshipxref import relationshipxref_DocHelper
 
 
from codegen.doc.dh_bindingfeature import bindingfeature_DocHelper
 
 
from codegen.doc.dh_experimentalform import experimentalform_DocHelper
 
 
from codegen.doc.dh_control import control_DocHelper
 
 
from codegen.doc.dh_publicationxref import publicationxref_DocHelper
 
 
from codegen.doc.dh_sequencesite import sequencesite_DocHelper
 
 
from codegen.doc.dh_controlledvocabulary import controlledvocabulary_DocHelper
 
 
from codegen.doc.dh_kprime import kprime_DocHelper
 
 
from codegen.doc.dh_interactionvocabulary import interactionvocabulary_DocHelper
 
 
from codegen.doc.dh_dnaregionreference import dnaregionreference_DocHelper
 
 
from codegen.doc.dh_pathway import pathway_DocHelper
 
 
from codegen.doc.dh_physicalentity import physicalentity_DocHelper
 
 
from codegen.doc.dh_provenance import provenance_DocHelper
 
 
from codegen.doc.dh_rna import rna_DocHelper
 
 
from codegen.doc.dh_modificationfeature import modificationfeature_DocHelper
 
 
from codegen.doc.dh_dnaregion import dnaregion_DocHelper
 
 
from codegen.doc.dh_chemicalstructure import chemicalstructure_DocHelper
 
 
from codegen.doc.dh_proteinreference import proteinreference_DocHelper
 
 
from codegen.doc.dh_smallmoleculereference import smallmoleculereference_DocHelper
 
 
from codegen.doc.dh_geneticinteraction import geneticinteraction_DocHelper
 
 
from codegen.doc.dh_tissuevocabulary import tissuevocabulary_DocHelper
 
 
from codegen.doc.dh_sequenceregionvocabulary import sequenceregionvocabulary_DocHelper
 
 
from codegen.doc.dh_phenotypevocabulary import phenotypevocabulary_DocHelper
 
 
from codegen.doc.dh_unificationxref import unificationxref_DocHelper
 
 
from codegen.doc.dh_biochemicalpathwaystep import biochemicalpathwaystep_DocHelper
 
 
from codegen.doc.dh_sequencemodificationvocabulary import sequencemodificationvocabulary_DocHelper
 
 
from codegen.doc.dh_sequenceinterval import sequenceinterval_DocHelper
 
 
from codegen.doc.dh_entityreferencetypevocabulary import entityreferencetypevocabulary_DocHelper
 
 
from codegen.doc.dh_dna import dna_DocHelper
 
 
from codegen.doc.dh_cellularlocationvocabulary import cellularlocationvocabulary_DocHelper
 
 
from codegen.doc.dh_evidence import evidence_DocHelper
 
 
from codegen.doc.dh_pathwaystep import pathwaystep_DocHelper
 
 
from codegen.doc.dh_sequencelocation import sequencelocation_DocHelper
 
 
from codegen.doc.dh_evidencecodevocabulary import evidencecodevocabulary_DocHelper
 
 
from codegen.doc.dh_gene import gene_DocHelper
 
 
from codegen.doc.dh_rnareference import rnareference_DocHelper
 
 
from codegen.doc.dh_entityreference import entityreference_DocHelper
 
 
from codegen.doc.dh_templatereactionregulation import templatereactionregulation_DocHelper
 
 
from codegen.doc.dh_rnaregion import rnaregion_DocHelper
 
 
from codegen.doc.dh_entity import entity_DocHelper
 
 
from codegen.doc.dh_utilityclass import utilityclass_DocHelper
 
 
  

def entries():
      cl=list()
      cl.append("Interaction")
      cl.append("Conversion")
      cl.append("DnaReference")
      cl.append("FragmentFeature")
      cl.append("ComplexAssembly")
      cl.append("BioSource")
      cl.append("Stoichiometry")
      cl.append("Catalysis")
      cl.append("Protein")
      cl.append("EntityFeature")
      cl.append("Degradation")
      cl.append("TemplateReaction")
      cl.append("ExperimentalFormVocabulary")
      cl.append("DeltaG")
      cl.append("CovalentBindingFeature")
      cl.append("BiochemicalReaction")
      cl.append("MolecularInteraction")
      cl.append("TransportWithBiochemicalReaction")
      cl.append("RelationshipTypeVocabulary")
      cl.append("Transport")
      cl.append("Complex")
      cl.append("Xref")
      cl.append("CellVocabulary")
      cl.append("SmallMolecule")
      cl.append("Score")
      cl.append("RnaRegionReference")
      cl.append("Modulation")
      cl.append("RelationshipXref")
      cl.append("BindingFeature")
      cl.append("ExperimentalForm")
      cl.append("Control")
      cl.append("PublicationXref")
      cl.append("SequenceSite")
      cl.append("ControlledVocabulary")
      cl.append("KPrime")
      cl.append("InteractionVocabulary")
      cl.append("DnaRegionReference")
      cl.append("Pathway")
      cl.append("PhysicalEntity")
      cl.append("Provenance")
      cl.append("Rna")
      cl.append("ModificationFeature")
      cl.append("DnaRegion")
      cl.append("ChemicalStructure")
      cl.append("ProteinReference")
      cl.append("SmallMoleculeReference")
      cl.append("GeneticInteraction")
      cl.append("TissueVocabulary")
      cl.append("SequenceRegionVocabulary")
      cl.append("PhenotypeVocabulary")
      cl.append("UnificationXref")
      cl.append("BiochemicalPathwayStep")
      cl.append("SequenceModificationVocabulary")
      cl.append("SequenceInterval")
      cl.append("EntityReferenceTypeVocabulary")
      cl.append("Dna")
      cl.append("CellularLocationVocabulary")
      cl.append("Evidence")
      cl.append("PathwayStep")
      cl.append("SequenceLocation")
      cl.append("EvidenceCodeVocabulary")
      cl.append("Gene")
      cl.append("RnaReference")
      cl.append("EntityReference")
      cl.append("TemplateReactionRegulation")
      cl.append("RnaRegion")
      cl.append("Entity")
      cl.append("UtilityClass")
  
      return cl


 

def select(cln):
      lcn=cln.lower()
      if lcn is None :
        return None
 
      elif lcn=="interaction" :
        return  interaction_DocHelper()
 
      elif lcn=="conversion" :
        return  conversion_DocHelper()
 
      elif lcn=="dnareference" :
        return  dnareference_DocHelper()
 
      elif lcn=="fragmentfeature" :
        return  fragmentfeature_DocHelper()
 
      elif lcn=="complexassembly" :
        return  complexassembly_DocHelper()
 
      elif lcn=="biosource" :
        return  biosource_DocHelper()
 
      elif lcn=="stoichiometry" :
        return  stoichiometry_DocHelper()
 
      elif lcn=="catalysis" :
        return  catalysis_DocHelper()
 
      elif lcn=="protein" :
        return  protein_DocHelper()
 
      elif lcn=="entityfeature" :
        return  entityfeature_DocHelper()
 
      elif lcn=="degradation" :
        return  degradation_DocHelper()
 
      elif lcn=="templatereaction" :
        return  templatereaction_DocHelper()
 
      elif lcn=="experimentalformvocabulary" :
        return  experimentalformvocabulary_DocHelper()
 
      elif lcn=="deltag" :
        return  deltag_DocHelper()
 
      elif lcn=="covalentbindingfeature" :
        return  covalentbindingfeature_DocHelper()
 
      elif lcn=="biochemicalreaction" :
        return  biochemicalreaction_DocHelper()
 
      elif lcn=="molecularinteraction" :
        return  molecularinteraction_DocHelper()
 
      elif lcn=="transportwithbiochemicalreaction" :
        return  transportwithbiochemicalreaction_DocHelper()
 
      elif lcn=="relationshiptypevocabulary" :
        return  relationshiptypevocabulary_DocHelper()
 
      elif lcn=="transport" :
        return  transport_DocHelper()
 
      elif lcn=="complex" :
        return  complex_DocHelper()
 
      elif lcn=="xref" :
        return  xref_DocHelper()
 
      elif lcn=="cellvocabulary" :
        return  cellvocabulary_DocHelper()
 
      elif lcn=="smallmolecule" :
        return  smallmolecule_DocHelper()
 
      elif lcn=="score" :
        return  score_DocHelper()
 
      elif lcn=="rnaregionreference" :
        return  rnaregionreference_DocHelper()
 
      elif lcn=="modulation" :
        return  modulation_DocHelper()
 
      elif lcn=="relationshipxref" :
        return  relationshipxref_DocHelper()
 
      elif lcn=="bindingfeature" :
        return  bindingfeature_DocHelper()
 
      elif lcn=="experimentalform" :
        return  experimentalform_DocHelper()
 
      elif lcn=="control" :
        return  control_DocHelper()
 
      elif lcn=="publicationxref" :
        return  publicationxref_DocHelper()
 
      elif lcn=="sequencesite" :
        return  sequencesite_DocHelper()
 
      elif lcn=="controlledvocabulary" :
        return  controlledvocabulary_DocHelper()
 
      elif lcn=="kprime" :
        return  kprime_DocHelper()
 
      elif lcn=="interactionvocabulary" :
        return  interactionvocabulary_DocHelper()
 
      elif lcn=="dnaregionreference" :
        return  dnaregionreference_DocHelper()
 
      elif lcn=="pathway" :
        return  pathway_DocHelper()
 
      elif lcn=="physicalentity" :
        return  physicalentity_DocHelper()
 
      elif lcn=="provenance" :
        return  provenance_DocHelper()
 
      elif lcn=="rna" :
        return  rna_DocHelper()
 
      elif lcn=="modificationfeature" :
        return  modificationfeature_DocHelper()
 
      elif lcn=="dnaregion" :
        return  dnaregion_DocHelper()
 
      elif lcn=="chemicalstructure" :
        return  chemicalstructure_DocHelper()
 
      elif lcn=="proteinreference" :
        return  proteinreference_DocHelper()
 
      elif lcn=="smallmoleculereference" :
        return  smallmoleculereference_DocHelper()
 
      elif lcn=="geneticinteraction" :
        return  geneticinteraction_DocHelper()
 
      elif lcn=="tissuevocabulary" :
        return  tissuevocabulary_DocHelper()
 
      elif lcn=="sequenceregionvocabulary" :
        return  sequenceregionvocabulary_DocHelper()
 
      elif lcn=="phenotypevocabulary" :
        return  phenotypevocabulary_DocHelper()
 
      elif lcn=="unificationxref" :
        return  unificationxref_DocHelper()
 
      elif lcn=="biochemicalpathwaystep" :
        return  biochemicalpathwaystep_DocHelper()
 
      elif lcn=="sequencemodificationvocabulary" :
        return  sequencemodificationvocabulary_DocHelper()
 
      elif lcn=="sequenceinterval" :
        return  sequenceinterval_DocHelper()
 
      elif lcn=="entityreferencetypevocabulary" :
        return  entityreferencetypevocabulary_DocHelper()
 
      elif lcn=="dna" :
        return  dna_DocHelper()
 
      elif lcn=="cellularlocationvocabulary" :
        return  cellularlocationvocabulary_DocHelper()
 
      elif lcn=="evidence" :
        return  evidence_DocHelper()
 
      elif lcn=="pathwaystep" :
        return  pathwaystep_DocHelper()
 
      elif lcn=="sequencelocation" :
        return  sequencelocation_DocHelper()
 
      elif lcn=="evidencecodevocabulary" :
        return  evidencecodevocabulary_DocHelper()
 
      elif lcn=="gene" :
        return  gene_DocHelper()
 
      elif lcn=="rnareference" :
        return  rnareference_DocHelper()
 
      elif lcn=="entityreference" :
        return  entityreference_DocHelper()
 
      elif lcn=="templatereactionregulation" :
        return  templatereactionregulation_DocHelper()
 
      elif lcn=="rnaregion" :
        return  rnaregion_DocHelper()
 
      elif lcn=="entity" :
        return  entity_DocHelper()
 
      elif lcn=="utilityclass" :
        return  utilityclass_DocHelper()
 
 
      else:
        return None 