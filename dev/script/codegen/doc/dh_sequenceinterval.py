 
###documentation helper
class sequenceinterval_DocHelper():
    
  def __init__(self):
    self.dmap=self.definitions()

  def classInfo(self):
    cln='SequenceInterval'
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       return m['class']
    return None
  
  def attributeNames(self):
    cln='SequenceInterval'
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s\n" %(k)    
    return s

  def attributesInfo(self):
    cln='SequenceInterval'
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s:" %(k)
         s+="\n%s" %(atm[attn])
    return s

  def attributeInfo(self,attn):
    cln='SequenceInterval'
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       if attn in atm.keys():
          return atm[attn]
    return None

  def definitions(self):
    dmap=dict()
    ####################################
    # class SequenceInterval
    dmap['SequenceInterval']=dict()
    dmap['SequenceInterval']['class']="""
Definition: An interval on a sequence. 
Usage: Interval is defined as an ordered pair of SequenceSites. All of the sequence from the begin site to the end site (inclusive) is described, not any subset.
    """
    dmap['SequenceInterval']['attribute']=dict()
  
    dmap['SequenceInterval']['attribute']['sequenceIntervalBegin']="""
The begin position of a sequence interval.
    """
    dmap['SequenceInterval']['attribute']['sequenceIntervalEnd']="""
The end position of a sequence interval.
    """
  
    return dmap