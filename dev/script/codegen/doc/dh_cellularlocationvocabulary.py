 
###documentation helper
class cellularlocationvocabulary_DocHelper():
    
  def __init__(self):
    self.dmap=self.definitions()

  def classInfo(self):
    cln='CellularLocationVocabulary'
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       return m['class']
    return None
  
  def attributeNames(self):
    cln='CellularLocationVocabulary'
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s\n" %(k)    
    return s

  def attributesInfo(self):
    cln='CellularLocationVocabulary'
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s:" %(k)
         s+="\n%s" %(atm[attn])
    return s

  def attributeInfo(self,attn):
    cln='CellularLocationVocabulary'
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       if attn in atm.keys():
          return atm[attn]
    return None

  def definitions(self):
    dmap=dict()
    ####################################
    # class CellularLocationVocabulary
    dmap['CellularLocationVocabulary']=dict()
    dmap['CellularLocationVocabulary']['class']="""
Definition: A reference to the Gene Ontology Cellular Component (GO CC) ontology. Homepage at http://www.geneontology.org.  Browse at http://www.ebi.ac.uk/ontology-lookup/browse.do?ontName=GO
    """
    dmap['CellularLocationVocabulary']['attribute']=dict()
  
    dmap['CellularLocationVocabulary']['attribute']['xref']="""
Values of this property define external cross-references from this entity to entities in external databases.
    """
    dmap['CellularLocationVocabulary']['attribute']['term']="""
The external controlled vocabulary term.
    """
  
    return dmap