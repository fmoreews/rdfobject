##generated class SmallMoleculeReference
#############################
##   A small molecule reference is a grouping of several small molecule entities
##   that have the same chemical structure.  Members can differ in celular location
##   and bound partners. Covalent modifications of small molecules are not considered
##   as state changes but treated as different molecules.

##############################
 
from codegen.entityreference import EntityReference
##############################
 



from codegen.class_utils import tostring
from codegen.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class SmallMoleculeReference(EntityReference) :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        super().__init__(*args, **kwargs) 
        self.rdf_type="http://www.biopax.org/release/biopax-level3.owl#SmallMoleculeReference"
##   Defines the chemical structure and other information about this molecule, using
##   an instance of class chemicalStructure.

        self._structure=kwargs.get('structure',None)  
##   The chemical formula of the small molecule. Note: chemical formula can also be
##   stored in the STRUCTURE property (in CML). In case of disagreement between the
##   value of this property and that in the CML file, the CML value takes precedence.

        self._chemicalFormula=kwargs.get('chemicalFormula',None)  
##   Defines the molecular weight of the molecule, in daltons.

        self._molecularWeight=kwargs.get('molecularWeight',None)  
  

##########getter
     
    def get_structure(self):
        return self._structure  
     
    def get_chemicalFormula(self):
        return self._chemicalFormula  
     
    def get_molecularWeight(self):
        return self._molecularWeight  
  
##########setter
    
    @validator(value='codegen.ChemicalStructure')  
 
    def set_structure(self,value):
        self._structure=value  
    
    @validator(value=str)  
 
    def set_chemicalFormula(self,value):
        self._chemicalFormula=value  
    
    @validator(value=float)  
 
    def set_molecularWeight(self,value):
        self._molecularWeight=value  
  




    def object_attributes(self):

      object_attribute_list=super().object_attributes() 
      satt=['structure']
      for elem in satt:
        object_attribute_list.append(elem)
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=super().type_attributes() 
      satt=['chemicalFormula', 'molecularWeight']
      for elem in satt:
        type_attribute_list.append(elem)
      return type_attribute_list
 


