##generated class TemplateReactionRegulation
#############################
##   Definition: Regulation of an expression reaction by a controlling element such
##   as a transcription factor or microRNA.   Usage: To represent the binding of the
##   transcription factor to a regulatory element in the TemplateReaction, create a
##   complex of the transcription factor and the regulatory element and set that as
##   the controller.

##############################
 
from codegen.control import Control
##############################
 



from codegen.class_utils import tostring
from codegen.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class TemplateReactionRegulation(Control) :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        super().__init__(*args, **kwargs) 
        self.rdf_type="http://www.biopax.org/release/biopax-level3.owl#TemplateReactionRegulation"
  

##########getter
  
##########setter
  




    def object_attributes(self):

      object_attribute_list=super().object_attributes() 
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=super().type_attributes() 
      return type_attribute_list
 


