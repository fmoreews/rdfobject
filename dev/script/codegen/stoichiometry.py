##generated class Stoichiometry
#############################
##   Definition: Stoichiometric coefficient of a physical entity in the context of a
##   conversion or complex. Usage: For each participating element there must be 0 or
##   1 stoichiometry element. A non-existing stoichiometric element is treated as
##   unknown. This is an n-ary bridge for left, right and component properties.
##   Relative stoichiometries ( e.g n, n+1) often used for describing polymerization
##   is not supported.

##############################
 
from codegen.utilityclass import UtilityClass
##############################
 



from codegen.class_utils import tostring
from codegen.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class Stoichiometry(UtilityClass) :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        super().__init__(*args, **kwargs) 
        self.rdf_type="http://www.biopax.org/release/biopax-level3.owl#Stoichiometry"
##   The physical entity to be annotated with stoichiometry.

        self._physicalEntity=kwargs.get('physicalEntity',None)  
##   Stoichiometric coefficient for one of the entities in an interaction or complex.
##   This value can be any rational number. Generic values such as "n" or "n+1"
##   should not be used - polymers are currently not covered.

        self._stoichiometricCoefficient=kwargs.get('stoichiometricCoefficient',None)  
  

##########getter
     
    def get_physicalEntity(self):
        return self._physicalEntity  
     
    def get_stoichiometricCoefficient(self):
        return self._stoichiometricCoefficient  
  
##########setter
    
    @validator(value='codegen.PhysicalEntity')  
 
    def set_physicalEntity(self,value):
        self._physicalEntity=value  
    
    @validator(value=float)  
 
    def set_stoichiometricCoefficient(self,value):
        self._stoichiometricCoefficient=value  
  




    def object_attributes(self):

      object_attribute_list=super().object_attributes() 
      satt=['physicalEntity']
      for elem in satt:
        object_attribute_list.append(elem)
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=super().type_attributes() 
      satt=['stoichiometricCoefficient']
      for elem in satt:
        type_attribute_list.append(elem)
      return type_attribute_list
 


