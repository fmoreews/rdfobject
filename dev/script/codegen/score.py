##generated class Score
#############################
##   Definition: A score associated with a publication reference describing how the
##   score was determined, the name of the method and a comment briefly describing
##   the method. Usage:  The xref must contain at least one publication that
##   describes the method used to determine the score value. There is currently no
##   standard way of describing  values, so any string is valid. Examples: The
##   statistical significance of a result, e.g. "p<0.05".

##############################
 
from codegen.utilityclass import UtilityClass
##############################
 



from codegen.class_utils import tostring
from codegen.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class Score(UtilityClass) :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        super().__init__(*args, **kwargs) 
        self.rdf_type="http://www.biopax.org/release/biopax-level3.owl#Score"
##   This property defines the source of the scoring methodology -- a publication or
##   web site describing the scoring methodology and the range of values.

        self._scoreSource=kwargs.get('scoreSource',None)  
##   The value of the score. This can be a numerical or categorical value.

        self._value=kwargs.get('value',None)  
  

##########getter
     
    def get_scoreSource(self):
        return self._scoreSource  
     
    def get_value(self):
        return self._value  
  
##########setter
    
    @validator(value='codegen.Provenance')  
 
    def set_scoreSource(self,value):
        self._scoreSource=value  
    
    @validator(value=str)  
 
    def set_value(self,value):
        self._value=value  
  




    def object_attributes(self):

      object_attribute_list=super().object_attributes() 
      satt=['scoreSource']
      for elem in satt:
        object_attribute_list.append(elem)
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=super().type_attributes() 
      satt=['value']
      for elem in satt:
        type_attribute_list.append(elem)
      return type_attribute_list
 


