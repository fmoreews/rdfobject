#! /usr/bin/env python3

import graphviz
import networkx as nx
import pathlib
import rdflib
from networkx.drawing.nx_agraph import write_dot
import sys
import time


sparqlPrefixes = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX dct: <http://purl.org/dc/terms/>
"""


def formatURI(classID, uriPrefix):
    if uriPrefix.startswith("http"):
        return "<" + uriPrefix + classID + ">"
    return uriPrefix + ":" + classID


def getLocalName(uri, uriPrefix):
    if uri.startswith("<"):
        return uri[1:-1].replace(uriPrefix, "")
    if uri.startswith("http"):
        return uri.replace(uriPrefix, "")
    if uriPrefix.endswith(":"):
        if uri.startswith(uriPrefix):
            return uri.replace(uriPrefix, "")
    if uri.startswith(uriPrefix + ":"):
        return uri.replace(uriPrefix + ":", "")
    return uri
    


def retrieveURIprefixes(rdfGraph, localID):
    sparqlQuery = sparqlPrefixes + """
SELECT DISTINCT ?entity
WHERE {
  { ?entity ?p ?o . }
  union
  { ?s ?p ?entity . }
  FILTER (isIRI(?entity) && strends(str(?entity),\"""" + localID + """\"))
}
"""
    qres = rdfGraph.query(sparqlQuery)
    uriPrefixes = []
    for row in qres:
        uriPrefixes.append(str(row[0]).replace(localID, ""))
    return uriPrefixes


def createGraphDescendants(rdfGraph, classID, uriPrefix, filePath=""):
    if filePath == "":
        filePath = classID + "-descendants.dot"
    sparqlQuery = sparqlPrefixes + """
SELECT ?class ?classLabel ?superClass ?superClassLabel
WHERE {
  ?class rdf:type owl:Class .
  OPTIONAL { ?class rdfs:label ?classLabel . }
  FILTER NOT EXISTS {
    ?class owl:deprecated "true"^^xsd:boolean .
  }
  ?class rdfs:subClassOf ?superClass .
  ?superClass rdf:type owl:Class .
  OPTIONAL { ?superClass rdfs:label ?superClassLabel . }
  FILTER NOT EXISTS {
    ?superClass owl:deprecated "true"^^xsd:boolean .
  }

  ?superClass rdfs:subClassOf* """ + formatURI(classID, uriPrefix) + """ .
}
"""
    #print(sparqlQuery)
    qres = rdfGraph.query(sparqlQuery)
    G = nx.DiGraph(rankdir="BT")
    for row in qres:
        #print("{}\t{}\t->\t{}\t{}".format(getLocalName(row[0], uriPrefix), row[1], getLocalName(row[2], uriPrefix), row[3]))
        nodeSrc = getLocalName(row[0], uriPrefix)
        nodeDest = getLocalName(row[2], uriPrefix)
        G.add_node(nodeSrc, shape="box", label=row[1] + "\n" + nodeSrc)
        G.add_node(nodeDest, shape="box", label=row[3] + "\n" + nodeDest)
        G.add_edge(nodeSrc, nodeDest)
    write_dot(G, filePath)


def createGraphAncestors(rdfGraph, classID, uriPrefix, filePath=""):
    if filePath == "":
        filePath = classID + "-ancestors.dot"
    sparqlQuery = sparqlPrefixes + """
SELECT ?class ?classLabel ?superClass ?superClassLabel
WHERE {
  ?class rdf:type owl:Class .
  OPTIONAL { ?class rdfs:label ?classLabel . }
  FILTER NOT EXISTS {
    ?class owl:deprecated "true"^^xsd:boolean .
  }
  ?class rdfs:subClassOf ?superClass .
  ?superClass rdf:type owl:Class .
  OPTIONAL { ?superClass rdfs:label ?superClassLabel . }
  FILTER NOT EXISTS {
    ?superClass owl:deprecated "true"^^xsd:boolean .
  }

  #?superClass rdfs:subClassOf* """ + formatURI(classID, uriPrefix) + """ .
  """ + formatURI(classID, uriPrefix) + """ rdfs:subClassOf* ?class .
}
"""
    qres = rdfGraph.query(sparqlQuery)
    G = nx.DiGraph(rankdir="BT")
    for row in qres:
        #print("{}\t{}\t->\t{}\t{}".format(getLocalName(row[0], uriPrefix), row[1], getLocalName(row[2], uriPrefix), row[3]))
        nodeSrc = getLocalName(row[0], uriPrefix)
        nodeDest = getLocalName(row[2], uriPrefix)
        G.add_node(nodeSrc, shape="box", label=row[1] + '\n' + nodeSrc)
        G.add_node(nodeDest, shape="box", label=row[3] + '\n' + nodeDest)
        G.add_edge(nodeSrc, nodeDest)
    write_dot(G, filePath)


def createOntologySchemaGraph(rdfGraph, filePath=""):
    schemaGraph = graphviz.Digraph('G')
    #schemaGraph.attr(rankdir="LR")
    schemaGraph.attr(rankdir="BT")

    prefixToNamespace = {}
    namespaceToPrefix = {}
    for currentNS in rdfGraph.namespace_manager.namespaces():
        prefixToNamespace[currentNS[0]] = currentNS[1]
        namespaceToPrefix[currentNS[1]] = currentNS[0]


    scriptDir = pathlib.Path(__file__).parent.absolute()
    sparqlQuery = pathlib.Path(scriptDir / 'queries/getClasses.rq').read_text()
    qres = rdfGraph.query(sparqlQuery)
    for row in qres:
        #nodeIdent = getLocalName(str(row[0]), 'http://www.univ-rennes1.fr/odameron/simpleOntologySchema.owl#')
        nodeIdent = rdfGraph.namespace_manager.normalizeUri(str(row[0])).replace(":", "-")
        nodeLabel = str(row[1])
        schemaGraph.node(nodeIdent, label=nodeLabel + "\n" + rdfGraph.namespace_manager.normalizeUri(str(row[0])), shape='box', color='black', fontcolor='black')

    sparqlQuery = pathlib.Path(scriptDir / 'queries/getClassesHierarchy.rq').read_text()
    qres = rdfGraph.query(sparqlQuery)
    for row in qres:
        sourceIdent = rdfGraph.namespace_manager.normalizeUri(str(row[0])).replace(":", "-")
        destIdent = rdfGraph.namespace_manager.normalizeUri(str(row[1])).replace(":", "-")
        schemaGraph.edge(sourceIdent, destIdent, arrowhead='onormal')

    sparqlQuery = pathlib.Path(scriptDir / 'queries/getProperties.rq').read_text()
    qres = rdfGraph.query(sparqlQuery)
    for row in qres:
        if row[3] == None or row[4] == None:
            continue
        propIdent = rdfGraph.namespace_manager.normalizeUri(str(row[0])).replace(":", "-")
        propLabel = str(row[1])
        if propLabel == "":
            propLabel = rdfGraph.namespace_manager.normalizeUri(str(row[0]))
        else:
            propLabel += "\n" + rdfGraph.namespace_manager.normalizeUri(str(row[0]))
        propType = rdfGraph.namespace_manager.normalizeUri(str(row[2]))
        sourceIdent = rdfGraph.namespace_manager.normalizeUri(str(row[3])).replace(":", "-")
        destIdent = rdfGraph.namespace_manager.normalizeUri(str(row[4])).replace(":", "-")
        if propType == 'owl:ObjectProperty':
            schemaGraph.edge(sourceIdent, destIdent, label=propLabel)
        elif propType == 'owl:DatatypeProperty':
            destIdent = 'str' + str(time.time())
            schemaGraph.node(destIdent, label=rdfGraph.namespace_manager.normalizeUri(str(row[4])), shape='box', color='black', fontcolor='black', style='rounded')
            schemaGraph.edge(sourceIdent, destIdent, label=propLabel)

    sparqlQuery = pathlib.Path(scriptDir / 'queries/getPropertyRestrictions.rq').read_text()
    qres = rdfGraph.query(sparqlQuery)
    for row in qres:
        propIdent = rdfGraph.namespace_manager.normalizeUri(str(row[0])).replace(":", "-")
        propLabel = str(row[1])
        if propLabel == "":
            propLabel = rdfGraph.namespace_manager.normalizeUri(str(row[0]))
        restrType = rdfGraph.namespace_manager.normalizeUri(str(row[2]))
        sourceIdent = rdfGraph.namespace_manager.normalizeUri(str(row[3])).replace(":", "-")
        destIdent = rdfGraph.namespace_manager.normalizeUri(str(row[4])).replace(":", "-")
        if restrType == 'owl:someValuesFrom':
            schemaGraph.edge(sourceIdent, destIdent, label=propLabel + " (some) ", color='grey', fontcolor='grey', arrowhead='odot')
        elif restrType == 'owl:allValuesFrom':
            schemaGraph.edge(sourceIdent, destIdent, label=propLabel + " (all) ", color='grey', fontcolor='grey', arrowhead='oinv')

    schemaGraph.save(filename=filePath)



if __name__ == "__main__":
    rdflibFormat = {}
    rdflibFormat['.owl'] = 'xml'
    rdflibFormat['.ttl'] = 'ttl'

    if len(sys.argv) == 1:
        #datasetPath = 'example/simpleOntologySchema.owl'
        #datasetPath = 'example/uniprot-core-20210604.owl'
        #datasetPath = 'example/modal-abstraction.ttl'
        #datasetPath = 'example/uniprot-mappedAnnotation.owl'
        #datasetPath = 'example/Mus_musculus.GRCm38.98.5.gff3_abstraction_domain_knowledge.ttl'
        datasetPath = pathlib.Path(__file__).parent.absolute() / 'example/regulatorycircuits.owl'
    elif len(sys.argv) == 2:
        datasetPath = sys.argv[1]
    else:
        print("Error: Wrong number of arguments")
        print("Usage: " + sys.argv[0] + " [ontologyFilePath]")
        sys.exit(1)
    datasetFormat = rdflibFormat[pathlib.PurePath(datasetPath).suffix]
    schemaFileName = pathlib.PurePath(datasetPath).stem + "-schema.dot"
    schemaPath = pathlib.PurePath(datasetPath).with_name(schemaFileName)

    rdfGraph = rdflib.Graph()
    rdfGraph.load(datasetPath, format=datasetFormat)


    #createGraphDescendants(rdfGraph, classOfInterest, prefixOfInterest, 'example/' + classOfInterest + '-descendants.dot')
    #createGraphAncestors(rdfGraph, classOfInterest, prefixOfInterest, 'example/' + classOfInterest + '-ancestors.dot')
    createOntologySchemaGraph(rdfGraph, schemaPath)

    print("Generated diagram: " + str(schemaPath))
    print("  convert to png with: dot -Tpng " + str(schemaPath) + " -o " + str(schemaPath)[:-3] + "png")
