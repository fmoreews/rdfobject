{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Audit trails for hierarchy objects in ReGraph (aka versioning)\n",
    "\n",
    "ReGraph implements a framework for the version control (VC) of graph transformations in hierarchies.\n",
    "\n",
    "The data structure `VersionedHierarchy` allows to store the history of transformations of a hierarchy and perform the following VC operations:\n",
    "\n",
    "- _Rewrite_: perform a rewriting of the hierarchy with a commit to the revision history\n",
    "- _Branch_: create a new branch (with a diverged version of the graph object)\n",
    "- _Merge branches_: merge branches\n",
    "- _Rollback_: rollback to a point in the history of transformations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from regraph import NXGraph, NXHierarchy\n",
    "from regraph.audit import VersionedHierarchy\n",
    "from regraph.rules import Rule\n",
    "from regraph import print_graph, plot_rule, plot_graph"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us start by creating a small hierarchy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "hierarchy = NXHierarchy()\n",
    "\n",
    "shapes = NXGraph()\n",
    "shapes.add_nodes_from([\"circle\", \"square\"])\n",
    "hierarchy.add_graph(\"shapes\", shapes)\n",
    "\n",
    "colors = NXGraph()\n",
    "colors.add_nodes_from([\"white\", \"black\"])\n",
    "hierarchy.add_graph(\"colors\", colors)\n",
    "\n",
    "ag = NXGraph()\n",
    "ag.add_nodes_from(\n",
    "    [\"wc\", \"bc\", \"ws\", \"bs\"])\n",
    "hierarchy.add_graph(\"metamodel\", ag)\n",
    "\n",
    "nugget = NXGraph()\n",
    "nugget.add_nodes_from(\n",
    "    [\"wc1\", \"wc2\", \"bc1\", \"ws1\", \"bs2\"])\n",
    "hierarchy.add_graph(\"data\", nugget)\n",
    "\n",
    "hierarchy.add_typing(\n",
    "    \"metamodel\", \"shapes\", {\n",
    "        \"wc\": \"circle\",\n",
    "        \"bc\": \"circle\",\n",
    "        \"ws\": \"square\",\n",
    "        \"bs\": \"square\"\n",
    "    })\n",
    "hierarchy.add_typing(\n",
    "    \"metamodel\", \"colors\", {\n",
    "        \"wc\": \"white\",\n",
    "        \"bc\": \"black\",\n",
    "        \"ws\": \"white\",\n",
    "        \"bs\": \"black\"\n",
    "    })\n",
    "hierarchy.add_typing(\n",
    "    \"data\", \"metamodel\", {\n",
    "        \"wc1\": \"wc\",\n",
    "        \"wc2\": \"wc\",\n",
    "        \"bc1\": \"bc\",\n",
    "        \"ws1\": \"ws\",\n",
    "        \"bs2\": \"bs\"\n",
    "    })\n",
    "hierarchy.add_typing(\n",
    "    \"data\", \"colors\", {\n",
    "        \"wc1\": \"white\",\n",
    "        \"wc2\": \"white\",\n",
    "        \"bc1\": \"black\",\n",
    "        \"ws1\": \"white\",\n",
    "        \"bs2\": \"black\"\n",
    "    })\n",
    "\n",
    "base = NXGraph()\n",
    "base.add_nodes_from([\"node\"])\n",
    "hierarchy.add_graph(\"base\", base)\n",
    "hierarchy.add_typing(\n",
    "    \"colors\",\n",
    "    \"base\", {\n",
    "        \"white\": \"node\",\n",
    "        \"black\": \"node\"\n",
    "    })"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us have a look at the hierarchy and its graphs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Graphs:\n",
      "\n",
      "shapes {}\n",
      "\n",
      "colors {}\n",
      "\n",
      "metamodel {}\n",
      "\n",
      "data {}\n",
      "\n",
      "base {}\n",
      "\n",
      "Typing homomorphisms: \n",
      "colors -> base: {}\n",
      "metamodel -> shapes: {}\n",
      "metamodel -> colors: {}\n",
      "data -> metamodel: {}\n",
      "data -> colors: {}\n",
      "\n",
      "Relations:\n",
      "\n",
      "Graph:  shapes  nodes:  ['circle', 'square']\n",
      "Graph:  colors  nodes:  ['white', 'black']\n",
      "Graph:  metamodel  nodes:  ['wc', 'bc', 'ws', 'bs']\n",
      "Graph:  data  nodes:  ['wc1', 'wc2', 'bc1', 'ws1', 'bs2']\n",
      "Graph:  base  nodes:  ['node']\n"
     ]
    }
   ],
   "source": [
    "print(hierarchy)\n",
    "\n",
    "for g in hierarchy.graphs():\n",
    "    print(\"Graph: \", g, \" nodes: \", hierarchy.get_graph(g).nodes())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We pass the hierarchy to the `VersionedHierarchy` wrapper that will take care of the version control."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Branches:  ['master']\n",
      "Current branch:  master\n"
     ]
    }
   ],
   "source": [
    "h = VersionedHierarchy(hierarchy)\n",
    "print(\"Branches: \", h.branches())\n",
    "print(\"Current branch: \", h.current_branch())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us create a new branch `test1`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Branches:  ['master', 'test1']\n",
      "Current branch:  test1\n"
     ]
    }
   ],
   "source": [
    "h.branch(\"test1\")\n",
    "print(\"Branches: \", h.branches())\n",
    "print(\"Current branch: \", h.current_branch())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will now rewrite our hierarchy at the current branch of the audit trail"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "pattern = NXGraph()\n",
    "pattern.add_nodes_from([\"s\"])\n",
    "rule = Rule.from_transform(pattern)\n",
    "rule.inject_remove_node(\"s\")\n",
    "\n",
    "rhs_instances, commit_id = h.rewrite(\n",
    "    \"shapes\",\n",
    "    rule, {\"s\": \"square\"},\n",
    "    message=\"Remove square in shapes\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `rewrite` method of `VersionedHierarchy` returns the instances of the RHS of the applied rule in different graphs and the id of the newly created commit corresponding to this rewrite."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "RHS instances {}\n",
      "Commit ID:  a32bc526-b9c6-4122-85b3-c30ad55658b4\n"
     ]
    }
   ],
   "source": [
    "print(\"RHS instances\", rhs_instances)\n",
    "print(\"Commit ID: \", commit_id)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We switch back to the `master` branch."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "h.switch_branch(\"master\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will now rewrite the hierarchy corresponding to the current branch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "14/02/2023 09:37:54 2e852899-2302-450e-b66d-b52044b07d8c master Initial commit\n",
      "14/02/2023 09:37:55 f9b151ba-d383-4958-a837-ce902d05f6ea test1 Created branch 'test1'\n",
      "14/02/2023 09:37:57 a32bc526-b9c6-4122-85b3-c30ad55658b4 test1 Remove square in shapes\n",
      "14/02/2023 09:38:00 847bf423-4dc4-4a04-9df4-d5e3fb34448f master Clone 'wc' in ag\n"
     ]
    }
   ],
   "source": [
    "pattern = NXGraph()\n",
    "pattern.add_nodes_from([\"wc\"])\n",
    "\n",
    "rule = Rule.from_transform(pattern)\n",
    "rule.inject_clone_node(\"wc\")\n",
    "\n",
    "_, clone_commit = h.rewrite(\n",
    "    \"metamodel\",\n",
    "    rule, {\"wc\": \"wc\"},\n",
    "    message=\"Clone 'wc' in ag\")\n",
    "\n",
    "h.print_history()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Clone commit ID:  847bf423-4dc4-4a04-9df4-d5e3fb34448f\n"
     ]
    }
   ],
   "source": [
    "print(\"Clone commit ID: \", clone_commit)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "pattern = NXGraph()\n",
    "pattern.add_nodes_from([\"wc1\"])\n",
    "\n",
    "rule = Rule.from_transform(pattern)\n",
    "rule.inject_add_node(\"new_node\")\n",
    "rule.inject_add_edge(\"new_node\", \"wc1\")\n",
    "\n",
    "_ = h.rewrite(\n",
    "    \"data\",\n",
    "    rule, {\"wc1\": \"wc1\"},\n",
    "    message=\"Add a new node to 'data'\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We merge the branch `test1` in into `master`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'e9f38cd4-15b8-4e2c-b9f5-229ff398c687'"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "h.merge_with(\"test1\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "14/02/2023 09:37:54 2e852899-2302-450e-b66d-b52044b07d8c master Initial commit\n",
      "14/02/2023 09:37:55 f9b151ba-d383-4958-a837-ce902d05f6ea test1 Created branch 'test1'\n",
      "14/02/2023 09:37:57 a32bc526-b9c6-4122-85b3-c30ad55658b4 test1 Remove square in shapes\n",
      "14/02/2023 09:38:00 847bf423-4dc4-4a04-9df4-d5e3fb34448f master Clone 'wc' in ag\n",
      "14/02/2023 09:38:01 9c7a217d-a1c2-4e44-9107-e966f35e3593 master Add a new node to 'data'\n",
      "14/02/2023 09:38:02 e9f38cd4-15b8-4e2c-b9f5-229ff398c687 master Merged branch 'test1' into 'master'\n"
     ]
    }
   ],
   "source": [
    "h.print_history()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now try to rollback to the commit `clone_commit`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Created the new head for 'test1'\n",
      "Created the new head for 'master'\n"
     ]
    }
   ],
   "source": [
    "h.rollback(clone_commit)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "14/02/2023 09:37:54 2e852899-2302-450e-b66d-b52044b07d8c master Initial commit\n",
      "14/02/2023 09:37:55 f9b151ba-d383-4958-a837-ce902d05f6ea test1 Created branch 'test1'\n",
      "14/02/2023 09:37:57 a32bc526-b9c6-4122-85b3-c30ad55658b4 test1 Remove square in shapes\n",
      "14/02/2023 09:38:00 847bf423-4dc4-4a04-9df4-d5e3fb34448f master Clone 'wc' in ag\n",
      "Branches:  ['master', 'test1']\n"
     ]
    }
   ],
   "source": [
    "h.print_history()\n",
    "print(\"Branches: \", h.branches())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the revision history came back to the previous state (right after the clone commit), and we still have two branches `master` and `test1`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
