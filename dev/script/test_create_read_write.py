import unittest
from unittest.mock import Mock

import rdfobj  as rdfo
from biopax_explorer.biopax import *
from biopax_explorer.biopax.utils.validate_utils import CValidateArgType,raise_error
from biopax_explorer.biopax.utils import gen_utils as gu
from biopax_explorer.query import  EntityNode
from biopax_explorer.biopax.utils import validate_utils as va



 

class TestSetControlled(unittest.TestCase):
    
    def setUp(self):
        self.control = Control()
        

    def test_valid_entity(self):
        try:
            self.control.set_controlled(Entity())
            self.control.set_controller(Protein())
        except Exception as e:
            self.fail(f"set_controlled or set_controller raised Exception unexpectedly: {e}")

    def test_unvalid_entity(self):
        te=0
        msg=""
        try:
            self.control.set_controlled(Xref())
        
        except Exception as e:
            te=1 
            msg=f"{e}"
        if te==0:
           self.fail(f"unallowed class Xref set by validator !  {msg}")

    def test_create_entity_from_gu(self):     
            
        try:
           for key in gu.classesDict().values():
                      inst=key()
                      entityNode = EntityNode("ALL", inst)  
                      print(inst.pk)
                      #print(inst.to_json())
           cdi=gu.modelPopulator().classDict
           for clsn,metaclass in cdi.items():
              att_lst=metaclass.attribute
              for pacls in metaclass.parent:
                 pmetaclass=cdi[pacls]
                 patt_lst=pmetaclass.attribute
                 for patt in patt_lst:    
                   att_lst.append(patt) 
              for att in att_lst:
                 print("class %s -> att: %s (list: %s, min:%s, max:%s, nullable:%s)"  %(clsn,att.name,att.list,att.min,att.max,att.nullable))
                 if att.list==True:
                     print(">>>!!!we have a list attribute : class %s -> att: %s " %(clsn,att.name))  
        except Exception as e:
            self.fail(f"test_create_entity_from_gu error: {e}") 
        
    def test_entity_with_list_attr(self):     
            
        try:
          cfg=va.get_validator_config()
          cfg['disable_list']=False
          print(cfg)
          va.update_validator_config(cfg)
          genein=GeneticInteraction()
          ent= Entity()
          #genein._participant=[ent]
          genein.set_participant([ent])

        except Exception as e:
            self.fail(f"error test_entity_with_list_attr  : {e}") 
        
    def test_entity_with_list_attr_without_list_val(self):
        te=0
        msg=""
        try:
          cfg=va.get_validator_config()
          cfg['disable_list']=True # not woking fixme
          print(cfg)
          va.update_validator_config(cfg)
          genein=GeneticInteraction()
          ent= Entity()
          genein.set_participant(ent)
        
        except Exception as e:
            te=1 
            msg=f"{e}"
            print("e:",msg)
        if te==0:
           self.fail(f"test_entity_with_list_attr_without_list_val  {msg}")

        
if __name__ == '__main__':
    unittest.main()



