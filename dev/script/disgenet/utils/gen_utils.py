import pickle
import base64

##########################################
##TODO remove dependencies to rdfobj here

from rdfobj import  ModelPopulator
# +from .utils , .meta_model
##########################################


#utilities to manipulate the classes generated 

from disgenet.somaticcausalmutation import SomaticCausalMutation
from disgenet.germlinemodifyingmutation import GermlineModifyingMutation
from disgenet.biomarker import Biomarker
from disgenet.somaticmodifyingmutation import SomaticModifyingMutation
from disgenet.genediseaseassociation import GeneDiseaseAssociation
from disgenet.causalmutation import CausalMutation
from disgenet.geneticvariation import GeneticVariation
from disgenet.posttranslationalmodification import PostTranslationalModification
from disgenet.fusiongene import FusionGene
from disgenet.chromosomalrearrangement import ChromosomalRearrangement
from disgenet.susceptibilitymutation import SusceptibilityMutation
from disgenet.germlinecausalmutation import GermlineCausalMutation
from disgenet.modifyingmutation import ModifyingMutation
from disgenet.genomicalterations import GenomicAlterations
from disgenet.therapeutic import Therapeutic
from disgenet.alteredexpression import AlteredExpression
from disgenet.association import Association
  



#return an instance of a class corresponding to the input keywork
def define_model_instance(clsn):


  if clsn is None:
    inst=None
  else:
    clsn=str(clsn).strip().lower()
    if clsn =='':
      inst=None 
    elif clsn.lower() == 'somaticcausalmutation':
      inst = SomaticCausalMutation() 
    elif clsn.lower() == 'germlinemodifyingmutation':
      inst = GermlineModifyingMutation() 
    elif clsn.lower() == 'biomarker':
      inst = Biomarker() 
    elif clsn.lower() == 'somaticmodifyingmutation':
      inst = SomaticModifyingMutation() 
    elif clsn.lower() == 'genediseaseassociation':
      inst = GeneDiseaseAssociation() 
    elif clsn.lower() == 'causalmutation':
      inst = CausalMutation() 
    elif clsn.lower() == 'geneticvariation':
      inst = GeneticVariation() 
    elif clsn.lower() == 'posttranslationalmodification':
      inst = PostTranslationalModification() 
    elif clsn.lower() == 'fusiongene':
      inst = FusionGene() 
    elif clsn.lower() == 'chromosomalrearrangement':
      inst = ChromosomalRearrangement() 
    elif clsn.lower() == 'susceptibilitymutation':
      inst = SusceptibilityMutation() 
    elif clsn.lower() == 'germlinecausalmutation':
      inst = GermlineCausalMutation() 
    elif clsn.lower() == 'modifyingmutation':
      inst = ModifyingMutation() 
    elif clsn.lower() == 'genomicalterations':
      inst = GenomicAlterations() 
    elif clsn.lower() == 'therapeutic':
      inst = Therapeutic() 
    elif clsn.lower() == 'alteredexpression':
      inst = AlteredExpression() 
    elif clsn.lower() == 'association':
      inst = Association() 
   
    else:
      inst=None
  

  return inst

#return an dictionary class_name->[children class_name]
def classes_children():

  mchildren=dict()
  mchildren['SomaticCausalMutation']= []
  mchildren['GermlineModifyingMutation']= []
  mchildren['Biomarker']= ['PostTranslationalModification', 'GenomicAlterations', 'GeneticVariation', 'CausalMutation', 'SomaticCausalMutation', 'GermlineCausalMutation', 'SusceptibilityMutation', 'ModifyingMutation', 'GermlineModifyingMutation', 'SomaticModifyingMutation', 'FusionGene', 'ChromosomalRearrangement', 'AlteredExpression']
  mchildren['SomaticModifyingMutation']= []
  mchildren['GeneDiseaseAssociation']= ['Biomarker', 'PostTranslationalModification', 'GenomicAlterations', 'GeneticVariation', 'CausalMutation', 'SomaticCausalMutation', 'GermlineCausalMutation', 'SusceptibilityMutation', 'ModifyingMutation', 'GermlineModifyingMutation', 'SomaticModifyingMutation', 'FusionGene', 'ChromosomalRearrangement', 'AlteredExpression', 'Therapeutic']
  mchildren['CausalMutation']= ['SomaticCausalMutation', 'GermlineCausalMutation']
  mchildren['GeneticVariation']= ['CausalMutation', 'SomaticCausalMutation', 'GermlineCausalMutation', 'SusceptibilityMutation', 'ModifyingMutation', 'GermlineModifyingMutation', 'SomaticModifyingMutation']
  mchildren['PostTranslationalModification']= []
  mchildren['FusionGene']= []
  mchildren['ChromosomalRearrangement']= []
  mchildren['SusceptibilityMutation']= []
  mchildren['GermlineCausalMutation']= []
  mchildren['ModifyingMutation']= ['GermlineModifyingMutation', 'SomaticModifyingMutation']
  mchildren['GenomicAlterations']= ['GeneticVariation', 'CausalMutation', 'SomaticCausalMutation', 'GermlineCausalMutation', 'SusceptibilityMutation', 'ModifyingMutation', 'GermlineModifyingMutation', 'SomaticModifyingMutation', 'FusionGene', 'ChromosomalRearrangement']
  mchildren['Therapeutic']= []
  mchildren['AlteredExpression']= []
  mchildren['Association']= ['GeneDiseaseAssociation', 'Biomarker', 'PostTranslationalModification', 'GenomicAlterations', 'GeneticVariation', 'CausalMutation', 'SomaticCausalMutation', 'GermlineCausalMutation', 'SusceptibilityMutation', 'ModifyingMutation', 'GermlineModifyingMutation', 'SomaticModifyingMutation', 'FusionGene', 'ChromosomalRearrangement', 'AlteredExpression', 'Therapeutic']
  
  return mchildren

def class_children(cln):
   mchildren=classes_children()
   if cln in mchildren.keys():
      return mchildren[cln]
   return None 

#list all classes of the model
def classesDict():
   model=dict() 
   model['SomaticCausalMutation']=SomaticCausalMutation
   model['GermlineModifyingMutation']=GermlineModifyingMutation
   model['Biomarker']=Biomarker
   model['SomaticModifyingMutation']=SomaticModifyingMutation
   model['GeneDiseaseAssociation']=GeneDiseaseAssociation
   model['CausalMutation']=CausalMutation
   model['GeneticVariation']=GeneticVariation
   model['PostTranslationalModification']=PostTranslationalModification
   model['FusionGene']=FusionGene
   model['ChromosomalRearrangement']=ChromosomalRearrangement
   model['SusceptibilityMutation']=SusceptibilityMutation
   model['GermlineCausalMutation']=GermlineCausalMutation
   model['ModifyingMutation']=ModifyingMutation
   model['GenomicAlterations']=GenomicAlterations
   model['Therapeutic']=Therapeutic
   model['AlteredExpression']=AlteredExpression
   model['Association']=Association
 
   return model 

#list all classes of the model
def classes():
   model=classesDict()
   return list(model.keys()) 

def createInstance(cln):
    model=classesDict()
    if cln in model.keys():
       cl=model[cln]
       return cl()
    return None 

def parentTree():
 parentdict=dict()
 chl=classes_children()
 for k in chl.keys():
    v=chl[k]
    for el in v:  
      if el in parentdict.keys():
        pad=parentdict[el]
      else:
        pad=dict()
      pad[k]=1
      parentdict[el]=pad
        
 for k in parentdict.keys():
    pad=parentdict[k]
    parentdict[k]=list(pad.keys())
 return parentdict




def modelPopulator():
    mp=PackageModelPopulator().mpop
    return mp


class  PackageModelPopulator():

  def __init__(self):
    self.package_name="disgenet"
    self.classDict=None
    self.mpop= None
    self.config()

  def config(self):
    self.classDict=self.classDictConf()
    self.mpop= ModelPopulator(self.classDict,self.package_name)

  def classDictConf(self)  :

     b64conf=b'gAWVDyAAAAAAAAB9lCiMFVNvbWF0aWNDYXVzYWxNdXRhdGlvbpSMFHJkZm9iamVjdC5tZXRhX21vZGVslIwLVENsYXNzTW9kZWyUk5QpgZR9lCiMBG5hbWWUaAGMBnBhcmVudJRdlIwOQ2F1c2FsTXV0YXRpb26UYYwHaXNfcm9vdJSJjAhjaGlsZHJlbpRdlIwJYXR0cmlidXRllF2UjBxfVENsYXNzTW9kZWxfX2F0dHJpYnV0ZV9uYW1llH2UjAdjb21tZW50lIxiIyMgICBSZWxhdGlvbnNoaXBzIG1hcHBlZCB0byB0aGlzIGNsYXNzOiAiRGlzZWFzZS1jYXVzaW5nIHNvbWF0aWMgbXV0YXRpb24ocykgaW4iCiMjICAgKE9ycGhhbmV0KQqUjApyYXdjb21tZW50lIxXUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczoKIkRpc2Vhc2UtY2F1c2luZyBzb21hdGljIG11dGF0aW9uKHMpIGluIiAoT3JwaGFuZXQplIwCcGuUjFpodHRwOi8vd3d3LmRpc2dlbmV0Lm9yZy9kcy9EaXNHZU5FVC9maWxlcy9HZW5lRGlzZWFzZUFzc29jaWF0aW9uLm93bCNTb21hdGljQ2F1c2FsTXV0YXRpb26UjAZkb21haW6UjEVodHRwOi8vd3d3LmRpc2dlbmV0Lm9yZy9kcy9EaXNHZU5FVC9maWxlcy9HZW5lRGlzZWFzZUFzc29jaWF0aW9uLm93bCOUjAZwcmVmaXiUjAhkaXNnZW5ldJSMCHJkZl90eXBllGgXjBVvYmplY3RfYXR0cmlidXRlX25hbWWUXZSME3R5cGVfYXR0cmlidXRlX25hbWWUXZSMDWF0dHJpYnV0ZV9hbGyUXZSMIF9UQ2xhc3NNb2RlbF9fYXR0cmlidXRlX25hbWVfYWxslH2UjBlvYmplY3RfYXR0cmlidXRlX25hbWVfYWxslF2UjBd0eXBlX2F0dHJpYnV0ZV9uYW1lX2FsbJRdlHVijBlHZXJtbGluZU1vZGlmeWluZ011dGF0aW9ulGgEKYGUfZQoaAdoKWgIXZSMEU1vZGlmeWluZ011dGF0aW9ulGFoC4loDF2UaA5dlGgQfZRoEoxVIyMgICBSZWxhdGlvbnNoaXBzIG1hcHBlZCB0byB0aGlzIGNsYXNzOiAiTW9kaWZ5aW5nIGdlcm1saW5lIG11dGF0aW9uIGluIiAoT3JwaGFuZXQpCpRoFIxPUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczoKIk1vZGlmeWluZyBnZXJtbGluZSBtdXRhdGlvbiBpbiIgKE9ycGhhbmV0KZRoFoxeaHR0cDovL3d3dy5kaXNnZW5ldC5vcmcvZHMvRGlzR2VORVQvZmlsZXMvR2VuZURpc2Vhc2VBc3NvY2lhdGlvbi5vd2wjR2VybWxpbmVNb2RpZnlpbmdNdXRhdGlvbpRoGGgZaBpoG2gcaDNoHV2UaB9dlGghXZRoI32UaCVdlGgnXZR1YowJQmlvbWFya2VylGgEKYGUfZQoaAdoOmgIXZSMFkdlbmVEaXNlYXNlQXNzb2NpYXRpb26UYWgLiWgMXZQojB1Qb3N0VHJhbnNsYXRpb25hbE1vZGlmaWNhdGlvbpSMEkdlbm9taWNBbHRlcmF0aW9uc5SMEUFsdGVyZWRFeHByZXNzaW9ulGVoDl2UaBB9lGgSWFUBAAAjIyAgIFJlbGF0aW9uc2hpcHMgbWFwcGVkIHRvIHRoaXMgY2xhc3M6ICJtYXJrZXIvbWVjaGFuaXNtIiAoQ1REKQojIyAgICJtYXJrZXIvbWVjaGFuaXNtfHRoZXJhcGV1dGljIiAoQ1REKSAibWFya2VyL21lY2hhbmlzbSIgKENURF9tb3VzZSkKIyMgICAibWFya2VyL21lY2hhbmlzbXx0aGVyYXBldXRpYyIgKENURF9tb3VzZSkgIm1hcmtlci9tZWNoYW5pc20iIChDVERfcmF0KQojIyAgICJtYXJrZXIvbWVjaGFuaXNtfHRoZXJhcGV1dGljIiAoQ1REX3JhdCkgIm1hcmtlciIgKE1HRCkgIm1hcmtlciIgKFJHRCkKIyMgICAiYW55X3JlbGF0aW9uIiAoTEhHRE4pICJiaW9tYXJrZXIiIChCZUZyZWUpCpRoFFg7AQAAUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczoKIm1hcmtlci9tZWNoYW5pc20iIChDVEQpCiJtYXJrZXIvbWVjaGFuaXNtfHRoZXJhcGV1dGljIiAoQ1REKQoibWFya2VyL21lY2hhbmlzbSIgKENURF9tb3VzZSkKIm1hcmtlci9tZWNoYW5pc218dGhlcmFwZXV0aWMiIChDVERfbW91c2UpCiJtYXJrZXIvbWVjaGFuaXNtIiAoQ1REX3JhdCkKIm1hcmtlci9tZWNoYW5pc218dGhlcmFwZXV0aWMiIChDVERfcmF0KQoibWFya2VyIiAoTUdEKQoibWFya2VyIiAoUkdEKQoiYW55X3JlbGF0aW9uIiAoTEhHRE4pCiJiaW9tYXJrZXIiIChCZUZyZWUplGgWjE5odHRwOi8vd3d3LmRpc2dlbmV0Lm9yZy9kcy9EaXNHZU5FVC9maWxlcy9HZW5lRGlzZWFzZUFzc29jaWF0aW9uLm93bCNCaW9tYXJrZXKUaBhoGWgaaBtoHGhHaB1dlGgfXZRoIV2UaCN9lGglXZRoJ12UdWKMGFNvbWF0aWNNb2RpZnlpbmdNdXRhdGlvbpRoBCmBlH2UKGgHaE5oCF2UjBFNb2RpZnlpbmdNdXRhdGlvbpRhaAuJaAxdlGgOXZRoEH2UaBKMVCMjICAgUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczogIk1vZGlmeWluZyBzb21hdGljIG11dGF0aW9uIGluIiAoT3JwaGFuZXQpCpRoFIxOUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczoKIk1vZGlmeWluZyBzb21hdGljIG11dGF0aW9uIGluIiAoT3JwaGFuZXQplGgWjF1odHRwOi8vd3d3LmRpc2dlbmV0Lm9yZy9kcy9EaXNHZU5FVC9maWxlcy9HZW5lRGlzZWFzZUFzc29jaWF0aW9uLm93bCNTb21hdGljTW9kaWZ5aW5nTXV0YXRpb26UaBhoGWgaaBtoHGhYaB1dlGgfXZRoIV2UaCN9lGglXZRoJ12UdWKMFkdlbmVEaXNlYXNlQXNzb2NpYXRpb26UaAQpgZR9lChoB2hfaAhdlIwLQXNzb2NpYXRpb26UYWgLiWgMXZQoaDqMC1RoZXJhcGV1dGljlGVoDl2UaBB9lGgSTmgUTmgWjFtodHRwOi8vd3d3LmRpc2dlbmV0Lm9yZy9kcy9EaXNHZU5FVC9maWxlcy9HZW5lRGlzZWFzZUFzc29jaWF0aW9uLm93bCNHZW5lRGlzZWFzZUFzc29jaWF0aW9ulGgYaBloGmgbaBxoaGgdXZRoH12UaCFdlGgjfZRoJV2UaCddlHVijA5DYXVzYWxNdXRhdGlvbpRoBCmBlH2UKGgHaG9oCF2UjBBHZW5ldGljVmFyaWF0aW9ulGFoC4loDF2UKGgBjBZHZXJtbGluZUNhdXNhbE11dGF0aW9ulGVoDl2UaBB9lGgSjFMjIyAgIFJlbGF0aW9uc2hpcHMgbWFwcGVkIHRvIHRoaXMgY2xhc3M6ICJQYXRob2dlbmljIiAoQ2xpblZhcikgInBoZW5vdHlwZSIgKE9NSU0pCpRoFIxNUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczoKIlBhdGhvZ2VuaWMiIChDbGluVmFyKQoicGhlbm90eXBlIiAoT01JTSmUaBaMU2h0dHA6Ly93d3cuZGlzZ2VuZXQub3JnL2RzL0Rpc0dlTkVUL2ZpbGVzL0dlbmVEaXNlYXNlQXNzb2NpYXRpb24ub3dsI0NhdXNhbE11dGF0aW9ulGgYaBloGmgbaBxoemgdXZRoH12UaCFdlGgjfZRoJV2UaCddlHVijBBHZW5ldGljVmFyaWF0aW9ulGgEKYGUfZQoaAdogWgIXZSMEkdlbm9taWNBbHRlcmF0aW9uc5RhaAuJaAxdlChob4wWU3VzY2VwdGliaWxpdHlNdXRhdGlvbpSMEU1vZGlmeWluZ011dGF0aW9ulGVoDl2UaBB9lGgSWDcBAAAjIyAgIFJlbGF0aW9uc2hpcHMgbWFwcGVkIHRvIHRoaXMgY2xhc3M6ICJnZW5ldGljX3ZhcmlhdGlvbiIgKFVuaVByb3QpICJDYW5kaWRhdGUKIyMgICBnZW5lIHRlc3RlZCBpbiIgKE9ycGhhbmV0KSAiQWZmZWN0cyIgKENsaW5WYXIpICJMaWtlbHkgcGF0aG9nZW5pYyIgKENsaW5WYXIpCiMjICAgInN1c2NlcHRpYmlsaXR5IiAoT01JTSkgImdlbmV0aWNfdmFyaWF0aW9uIiAoR1dBU0NBVCkgImdlbmV0aWNfdmFyaWF0aW9uIiAoR0FEKQojIyAgICJnZW5ldGljX3ZhcmlhdGlvbiIgKExIR0ROKSAiZ2VuZXRpY192YXJpYXRpb24iIChCZUZyZWUpCpRoFFgiAQAAUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczoKImdlbmV0aWNfdmFyaWF0aW9uIiAoVW5pUHJvdCkKIkNhbmRpZGF0ZSBnZW5lIHRlc3RlZCBpbiIgKE9ycGhhbmV0KQoiQWZmZWN0cyIgKENsaW5WYXIpCiJMaWtlbHkgcGF0aG9nZW5pYyIgKENsaW5WYXIpCiJzdXNjZXB0aWJpbGl0eSIgKE9NSU0pCiJnZW5ldGljX3ZhcmlhdGlvbiIgKEdXQVNDQVQpCiJnZW5ldGljX3ZhcmlhdGlvbiIgKEdBRCkKImdlbmV0aWNfdmFyaWF0aW9uIiAoTEhHRE4pCiJnZW5ldGljX3ZhcmlhdGlvbiIgKEJlRnJlZSmUaBaMVWh0dHA6Ly93d3cuZGlzZ2VuZXQub3JnL2RzL0Rpc0dlTkVUL2ZpbGVzL0dlbmVEaXNlYXNlQXNzb2NpYXRpb24ub3dsI0dlbmV0aWNWYXJpYXRpb26UaBhoGWgaaBtoHGiNaB1dlGgfXZRoIV2UaCN9lGglXZRoJ12UdWJoQGgEKYGUfZQoaAdoQGgIXZSMCUJpb21hcmtlcpRhaAuJaAxdlGgOXZRoEH2UaBKMeyMjICAgUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczogIm1ldGh5bGF0aW9ufHBob3NwaG9yeWxhdGlvbiIgKExIR0ROKQojIyAgICJtZXRoeWxhdGlvbnxwaG9zcGhvcnlsYXRpb24iIChCZUZyZWUpCpRoFIxwUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczoKIm1ldGh5bGF0aW9ufHBob3NwaG9yeWxhdGlvbiIgKExIR0ROKQoibWV0aHlsYXRpb258cGhvc3Bob3J5bGF0aW9uIiAoQmVGcmVlKZRoFoxiaHR0cDovL3d3dy5kaXNnZW5ldC5vcmcvZHMvRGlzR2VORVQvZmlsZXMvR2VuZURpc2Vhc2VBc3NvY2lhdGlvbi5vd2wjUG9zdFRyYW5zbGF0aW9uYWxNb2RpZmljYXRpb26UaBhoGWgaaBtoHGidaB1dlGgfXZRoIV2UaCN9lGglXZRoJ12UdWKMCkZ1c2lvbkdlbmWUaAQpgZR9lChoB2ikaAhdlIwSR2Vub21pY0FsdGVyYXRpb25zlGFoC4loDF2UaA5dlGgQfZRoEoxPIyMgICBSZWxhdGlvbnNoaXBzIG1hcHBlZCB0byB0aGlzIGNsYXNzOiAiUGFydCBvZiBhIGZ1c2lvbiBnZW5lIGluIiAoT3JwaGFuZXQpCpRoFIxJUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczoKIlBhcnQgb2YgYSBmdXNpb24gZ2VuZSBpbiIgKE9ycGhhbmV0KZRoFoxPaHR0cDovL3d3dy5kaXNnZW5ldC5vcmcvZHMvRGlzR2VORVQvZmlsZXMvR2VuZURpc2Vhc2VBc3NvY2lhdGlvbi5vd2wjRnVzaW9uR2VuZZRoGGgZaBpoG2gcaK5oHV2UaB9dlGghXZRoI32UaCVdlGgnXZR1YowYQ2hyb21vc29tYWxSZWFycmFuZ2VtZW50lGgEKYGUfZQoaAdotWgIXZSMEkdlbm9taWNBbHRlcmF0aW9uc5RhaAuJaAxdlGgOXZRoEH2UaBKMTyMjICAgUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczogIlJvbGUgaW4gdGhlIHBoZW5vdHlwZSBvZiIgKE9ycGhhbmV0KQqUaBSMSVJlbGF0aW9uc2hpcHMgbWFwcGVkIHRvIHRoaXMgY2xhc3M6CiJSb2xlIGluIHRoZSBwaGVub3R5cGUgb2YiIChPcnBoYW5ldCmUaBaMXWh0dHA6Ly93d3cuZGlzZ2VuZXQub3JnL2RzL0Rpc0dlTkVUL2ZpbGVzL0dlbmVEaXNlYXNlQXNzb2NpYXRpb24ub3dsI0Nocm9tb3NvbWFsUmVhcnJhbmdlbWVudJRoGGgZaBpoG2gcaL9oHV2UaB9dlGghXZRoI32UaCVdlGgnXZR1YmiHaAQpgZR9lChoB2iHaAhdlIwQR2VuZXRpY1ZhcmlhdGlvbpRhaAuJaAxdlGgOXZRoEH2UaBKMqSMjICAgUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczogIk1ham9yIHN1c2NlcHRpYmlsaXR5IGZhY3RvciBpbiIgKE9ycGhhbmV0KQojIyAgICJyaXNrIGZhY3RvciIgKENsaW5WYXIpICJjb25mZXJzIHNlbnNpdGl2aXR5IiAoQ2xpblZhcikgInN1c2NlcHRpYmlsaXR5IiAoUkdEKQqUaBSMnlJlbGF0aW9uc2hpcHMgbWFwcGVkIHRvIHRoaXMgY2xhc3M6CiJNYWpvciBzdXNjZXB0aWJpbGl0eSBmYWN0b3IgaW4iIChPcnBoYW5ldCkKInJpc2sgZmFjdG9yIiAoQ2xpblZhcikKImNvbmZlcnMgc2Vuc2l0aXZpdHkiIChDbGluVmFyKQoic3VzY2VwdGliaWxpdHkiIChSR0QplGgWjFtodHRwOi8vd3d3LmRpc2dlbmV0Lm9yZy9kcy9EaXNHZU5FVC9maWxlcy9HZW5lRGlzZWFzZUFzc29jaWF0aW9uLm93bCNTdXNjZXB0aWJpbGl0eU11dGF0aW9ulGgYaBloGmgbaBxoz2gdXZRoH12UaCFdlGgjfZRoJV2UaCddlHViaHVoBCmBlH2UKGgHaHVoCF2UjA5DYXVzYWxNdXRhdGlvbpRhaAuJaAxdlGgOXZRoEH2UaBKM/SMjICAgUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczogIkRpc2Vhc2UtY2F1c2luZyBnZXJtbGluZSBtdXRhdGlvbihzKSBpbiIKIyMgICAoT3JwaGFuZXQpICJEaXNlYXNlLWNhdXNpbmcgZ2VybWxpbmUgbXV0YXRpb24ocykgKGxvc3Mgb2YgZnVuY3Rpb24pIGluIgojIyAgIChPcnBoYW5ldCkgIkRpc2Vhc2UtY2F1c2luZyBnZXJtbGluZSBtdXRhdGlvbihzKSAoZ2FpbiBvZiBmdW5jdGlvbikgaW4iCiMjICAgKE9ycGhhbmV0KQqUaBSM6FJlbGF0aW9uc2hpcHMgbWFwcGVkIHRvIHRoaXMgY2xhc3M6CiJEaXNlYXNlLWNhdXNpbmcgZ2VybWxpbmUgbXV0YXRpb24ocykgaW4iIChPcnBoYW5ldCkKIkRpc2Vhc2UtY2F1c2luZyBnZXJtbGluZSBtdXRhdGlvbihzKSAobG9zcyBvZiBmdW5jdGlvbikgaW4iIChPcnBoYW5ldCkKIkRpc2Vhc2UtY2F1c2luZyBnZXJtbGluZSBtdXRhdGlvbihzKSAoZ2FpbiBvZiBmdW5jdGlvbikgaW4iIChPcnBoYW5ldCmUaBaMW2h0dHA6Ly93d3cuZGlzZ2VuZXQub3JnL2RzL0Rpc0dlTkVUL2ZpbGVzL0dlbmVEaXNlYXNlQXNzb2NpYXRpb24ub3dsI0dlcm1saW5lQ2F1c2FsTXV0YXRpb26UaBhoGWgaaBtoHGjfaB1dlGgfXZRoIV2UaCN9lGglXZRoJ12UdWJoiGgEKYGUfZQoaAdoiGgIXZSMEEdlbmV0aWNWYXJpYXRpb26UYWgLiWgMXZQoaCloTmVoDl2UaBB9lGgSjGkjIyAgIFJlbGF0aW9uc2hpcHMgbWFwcGVkIHRvIHRoaXMgY2xhc3M6ICJkaXNlYXNlX3Byb2dyZXNzaW9uIiAoUkdEKSAib25zZXQiIChSR0QpCiMjICAgInNldmVyaXR5IiAoUkdEKQqUaBSMXlJlbGF0aW9uc2hpcHMgbWFwcGVkIHRvIHRoaXMgY2xhc3M6CiJkaXNlYXNlX3Byb2dyZXNzaW9uIiAoUkdEKQoib25zZXQiIChSR0QpCiJzZXZlcml0eSIgKFJHRCmUaBaMVmh0dHA6Ly93d3cuZGlzZ2VuZXQub3JnL2RzL0Rpc0dlTkVUL2ZpbGVzL0dlbmVEaXNlYXNlQXNzb2NpYXRpb24ub3dsI01vZGlmeWluZ011dGF0aW9ulGgYaBloGmgbaBxo72gdXZRoH12UaCFdlGgjfZRoJV2UaCddlHViaEFoBCmBlH2UKGgHaEFoCF2UjAlCaW9tYXJrZXKUYWgLiWgMXZQoaIFopGi1ZWgOXZRoEH2UaBJOaBROaBaMWWh0dHA6Ly93d3cuZGlzZ2VuZXQub3JnL2RzL0Rpc0dlTkVUL2ZpbGVzL0dlbmVEaXNlYXNlQXNzb2NpYXRpb24ub3dsIydHZW5vbWljQWx0ZXJhdGlvbnMnlGgYaBloGmgbaBxo/WgdXZRoH12UaCFdlGgjfZRoJV2UaCddlHViaGVoBCmBlH2UKGgHaGVoCF2UjBZHZW5lRGlzZWFzZUFzc29jaWF0aW9ulGFoC4loDF2UaA5dlGgQfZRoElgJAQAAIyMgICBSZWxhdGlvbnNoaXBzIG1hcHBlZCB0byB0aGlzIGNsYXNzOiAidGhlcmFwZXV0aWMiIChDVEQpCiMjICAgIm1hcmtlci9tZWNoYW5pc218dGhlcmFwZXV0aWMiIChDVEQpICJ0aGVyYXBldXRpYyIgKENURF9tb3VzZSkKIyMgICAibWFya2VyL21lY2hhbmlzbXx0aGVyYXBldXRpYyIgKENURF9tb3VzZSkgInRoZXJhcGV1dGljIiAoQ1REX3JhdCkKIyMgICAibWFya2VyL21lY2hhbmlzbXx0aGVyYXBldXRpYyIgKENURF9yYXQpICJ0cmVhdG1lbnQiIChSR0QpCpRoFIz0UmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczoKInRoZXJhcGV1dGljIiAoQ1REKQoibWFya2VyL21lY2hhbmlzbXx0aGVyYXBldXRpYyIgKENURCkKInRoZXJhcGV1dGljIiAoQ1REX21vdXNlKQoibWFya2VyL21lY2hhbmlzbXx0aGVyYXBldXRpYyIgKENURF9tb3VzZSkKInRoZXJhcGV1dGljIiAoQ1REX3JhdCkKIm1hcmtlci9tZWNoYW5pc218dGhlcmFwZXV0aWMiIChDVERfcmF0KQoidHJlYXRtZW50IiAoUkdEKZRoFoxQaHR0cDovL3d3dy5kaXNnZW5ldC5vcmcvZHMvRGlzR2VORVQvZmlsZXMvR2VuZURpc2Vhc2VBc3NvY2lhdGlvbi5vd2wjVGhlcmFwZXV0aWOUaBhoGWgaaBtoHGoNAQAAaB1dlGgfXZRoIV2UaCN9lGglXZRoJ12UdWJoQmgEKYGUfZQoaAdoQmgIXZSMCUJpb21hcmtlcpRhaAuJaAxdlGgOXZRoEH2UaBKMaSMjICAgUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczogImFsdGVyZWRfZXhwcmVzc2lvbiIgKExIR0ROKQojIyAgICJhbHRlcmVkX2V4cHJlc3Npb24iIChCZUZyZWUpCpRoFIxeUmVsYXRpb25zaGlwcyBtYXBwZWQgdG8gdGhpcyBjbGFzczoKImFsdGVyZWRfZXhwcmVzc2lvbiIgKExIR0ROKQoiYWx0ZXJlZF9leHByZXNzaW9uIiAoQmVGcmVlKZRoFoxWaHR0cDovL3d3dy5kaXNnZW5ldC5vcmcvZHMvRGlzR2VORVQvZmlsZXMvR2VuZURpc2Vhc2VBc3NvY2lhdGlvbi5vd2wjQWx0ZXJlZEV4cHJlc3Npb26UaBhoGWgaaBtoHGodAQAAaB1dlGgfXZRoIV2UaCN9lGglXZRoJ12UdWKMC0Fzc29jaWF0aW9ulGgEKYGUfZQoaAdqJAEAAGgIXZRoC4loDF2UaF9haA5dlGgQfZRoEk5oFE5oFoxQaHR0cDovL3d3dy5kaXNnZW5ldC5vcmcvZHMvRGlzR2VORVQvZmlsZXMvR2VuZURpc2Vhc2VBc3NvY2lhdGlvbi5vd2wjQXNzb2NpYXRpb26UaBhoGWgaaBtoHGorAQAAaB1dlGgfXZRoIV2UaCN9lGglXZRoJ12UdWJ1Lg=='

     obj = pickle.loads(base64.b64decode(b64conf))
     return obj

def domain():
    return "http://www.disgenet.org/ds/DisGeNET/files/GeneDiseaseAssociation.owl#"

def prefix():
    return "disgenet"    


