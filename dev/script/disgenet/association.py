##generated class Association
#############################
##############################
##############################
 



from disgenet.utils.class_utils import tostring
from disgenet.utils.validate_utils import CValidateArgType,raise_error





validator = CValidateArgType(raise_error, logger=None)

@tostring
class Association() :
##########constructor
    def __init__(self, *args, **kwargs):
        #args -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        
        self.pk=kwargs.get('pk',None)    
        self.pop_state=kwargs.get('pop_state',None)  
        self.exhausted=kwargs.get('exhausted',None)  
        
        self.rdf_type="http://www.disgenet.org/ds/DisGeNET/files/GeneDiseaseAssociation.owl#Association"
  

##########getter
  
##########setter
  




    def object_attributes(self):

      object_attribute_list=list()
      return object_attribute_list
 

    def type_attributes(self):
 
      type_attribute_list=list()
      return type_attribute_list
 
#####get attributes types 
    def attribute_type_by_name(self):
      ma=dict()
      return ma
