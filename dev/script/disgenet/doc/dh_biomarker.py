
from disgenet.utils import gen_utils
 
###documentation helper
class biomarker_DocHelper():
    
  def __init__(self):
    self.dmap=self.definitions()
    self.cln='Biomarker'
    self.inst=gen_utils.define_model_instance(self.cln)
    self.tmap=self.attr_type_def()


  def classInfo(self):
    cln=self.cln
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       return m['class']
    return None
  
  def attributeNameString(self):
    cln=self.cln
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s\n" %(k)    
    return s

  def attributeNames(self):
    cln=self.cln
    al=[]
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         al.append(k)
    return al  

  def objectAttributeNames(self):
    cln=self.cln
    oa=self.inst.object_attributes()
    al=[]
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         if k in oa:
           al.append(k)
    return al    

  def typeAttributeNames(self):
    cln=self.cln
    ta=self.inst.type_attributes()
    al=[]
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         if k in ta:
           al.append(k)
    return al   


  def attributesInfo(self):
    cln=self.cln
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s:" %(k)
         s+="\n%s" %(atm[attn])
    return s

  def attributeInfo(self,attn):
    cln=self.cln
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       if attn in atm.keys():
          return atm[attn]
    return None

  def attributeType(self,attn):
    cln=self.cln
    if cln in self.dmap.keys():
       m=self.tmap[cln]
       atm= m['attribute']
       if attn in atm.keys():
          return atm[attn]
    return None


  def definitions(self):
    dmap=dict()
    ####################################
    # class Biomarker
    dmap['Biomarker']=dict()
    dmap['Biomarker']['class']="""
Relationships mapped to this class:
"marker/mechanism" (CTD)
"marker/mechanism|therapeutic" (CTD)
"marker/mechanism" (CTD_mouse)
"marker/mechanism|therapeutic" (CTD_mouse)
"marker/mechanism" (CTD_rat)
"marker/mechanism|therapeutic" (CTD_rat)
"marker" (MGD)
"marker" (RGD)
"any_relation" (LHGDN)
"biomarker" (BeFree)
    """
    dmap['Biomarker']['attribute']=dict()
  
  
    return dmap


  def attr_type_def(self):
    dmap=dict()
    ####################################
    # class Biomarker
    dmap['Biomarker']=dict()
    dmap['Biomarker']['attribute']=dict()
  
    return dmap    