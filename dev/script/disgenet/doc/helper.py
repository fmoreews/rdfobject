import textwrap

#utilities to manipulate the doc classes 

 
 
from disgenet.doc.dh_somaticcausalmutation import somaticcausalmutation_DocHelper
 
 
from disgenet.doc.dh_germlinemodifyingmutation import germlinemodifyingmutation_DocHelper
 
 
from disgenet.doc.dh_biomarker import biomarker_DocHelper
 
 
from disgenet.doc.dh_somaticmodifyingmutation import somaticmodifyingmutation_DocHelper
 
 
from disgenet.doc.dh_genediseaseassociation import genediseaseassociation_DocHelper
 
 
from disgenet.doc.dh_causalmutation import causalmutation_DocHelper
 
 
from disgenet.doc.dh_geneticvariation import geneticvariation_DocHelper
 
 
from disgenet.doc.dh_posttranslationalmodification import posttranslationalmodification_DocHelper
 
 
from disgenet.doc.dh_fusiongene import fusiongene_DocHelper
 
 
from disgenet.doc.dh_chromosomalrearrangement import chromosomalrearrangement_DocHelper
 
 
from disgenet.doc.dh_susceptibilitymutation import susceptibilitymutation_DocHelper
 
 
from disgenet.doc.dh_germlinecausalmutation import germlinecausalmutation_DocHelper
 
 
from disgenet.doc.dh_modifyingmutation import modifyingmutation_DocHelper
 
 
from disgenet.doc.dh_genomicalterations import genomicalterations_DocHelper
 
 
from disgenet.doc.dh_therapeutic import therapeutic_DocHelper
 
 
from disgenet.doc.dh_alteredexpression import alteredexpression_DocHelper
 
 
from disgenet.doc.dh_association import association_DocHelper
  

def entries():
      cl=list()
      cl.append("SomaticCausalMutation")
      cl.append("GermlineModifyingMutation")
      cl.append("Biomarker")
      cl.append("SomaticModifyingMutation")
      cl.append("GeneDiseaseAssociation")
      cl.append("CausalMutation")
      cl.append("GeneticVariation")
      cl.append("PostTranslationalModification")
      cl.append("FusionGene")
      cl.append("ChromosomalRearrangement")
      cl.append("SusceptibilityMutation")
      cl.append("GermlineCausalMutation")
      cl.append("ModifyingMutation")
      cl.append("GenomicAlterations")
      cl.append("Therapeutic")
      cl.append("AlteredExpression")
      cl.append("Association")
  
      return cl


 

def select(cln):
      lcn=cln.lower()
      if lcn is None :
        return None
 
      elif lcn=="somaticcausalmutation" :
        return  somaticcausalmutation_DocHelper()
 
      elif lcn=="germlinemodifyingmutation" :
        return  germlinemodifyingmutation_DocHelper()
 
      elif lcn=="biomarker" :
        return  biomarker_DocHelper()
 
      elif lcn=="somaticmodifyingmutation" :
        return  somaticmodifyingmutation_DocHelper()
 
      elif lcn=="genediseaseassociation" :
        return  genediseaseassociation_DocHelper()
 
      elif lcn=="causalmutation" :
        return  causalmutation_DocHelper()
 
      elif lcn=="geneticvariation" :
        return  geneticvariation_DocHelper()
 
      elif lcn=="posttranslationalmodification" :
        return  posttranslationalmodification_DocHelper()
 
      elif lcn=="fusiongene" :
        return  fusiongene_DocHelper()
 
      elif lcn=="chromosomalrearrangement" :
        return  chromosomalrearrangement_DocHelper()
 
      elif lcn=="susceptibilitymutation" :
        return  susceptibilitymutation_DocHelper()
 
      elif lcn=="germlinecausalmutation" :
        return  germlinecausalmutation_DocHelper()
 
      elif lcn=="modifyingmutation" :
        return  modifyingmutation_DocHelper()
 
      elif lcn=="genomicalterations" :
        return  genomicalterations_DocHelper()
 
      elif lcn=="therapeutic" :
        return  therapeutic_DocHelper()
 
      elif lcn=="alteredexpression" :
        return  alteredexpression_DocHelper()
 
      elif lcn=="association" :
        return  association_DocHelper()
 
      else:
        return None 



def describe(cln):
   
   prefix=""
   el="\n"
   preferredWidth=70
   wrapper = textwrap.TextWrapper(initial_indent="", width=preferredWidth,
                               subsequent_indent=' '*len(prefix))
   dh=select(cln)
   if dh is None:
     return None

   s="*"*20+el
   s+=str(cln)+el
   s+=dh.classInfo()+el
   s+="-"*20+el
   s+="primitive type attributes:"+el
   for n in  dh.typeAttributeNames():
       s+="-"*10+el
       s+="%s (%s): %s" %(n, dh.attributeType(n),el)
       s+=""+el
       s+=str(wrapper.fill(dh.attributeInfo(n)))
       s+=""+el
   s+="-"*20+el
   s+="object attributes:"+el
   for n in  dh.objectAttributeNames():
       s+="-"*10+el
       s+="%s (%s): %s" %(n, dh.attributeType(n),el)
       s+=""+el
       s+=str(wrapper.fill(dh.attributeInfo(n)))
       s+=""+el
   s+="*"*20+el
   return s

    