# -*- coding: utf-8 -*-
# MIT License
#
# Copyright (c) 2017 IRISA, Jean Coquet, Pierre Vignet, Mateo Boudet
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Contributor(s): Jean Coquet, Pierre Vignet, Mateo Boudet

"""
This module groups functions used to export BioPAX-processed data
to a Cabiom model file.
"""
from __future__ import unicode_literals
from __future__ import print_function

# Standard imports
import json
from lxml import etree as ET

# Custom imports
#import cadbiom.models.guard_transitions.analyser.model_corrections as mc


def format_condition(condition):
    """Build the string representation of the given sympy expression

    We just replace logical operators by their textual version in the string
    representation of the given condition.
    To see the previous version with tree parsing go to commits <= 9702ea8;
    this last method is cleaner but recursive and costly for BIG and COMPLEX
    conditions.

    :param condition: Sympy expression
    :type condition: <sympy.Or>, <sympy.And>, <sympy.Not>
    :return type: <str>
    """
    if condition == True:
        return ""

    return str(condition).replace("&", "and").replace("|", "or").replace("~", "not ")


def format_events_and_conditions(events_conditions):
    """Build the condition of a transition based on the given set of events and conditions

    :param events_conditions: Set of tuples (event name, sympy condition)
    :type events_conditions: <set <tuple <str>, <str>>>
    :return type: <str>
    """

    iter_conditions = iter(events_conditions)

    # Set of formatted strings for each event
    formatted_conditions = set()

    while True:
        try:
            event, condition = next(iter_conditions)

            if condition == True:
                # The condition is empty, just wrap the event name
                current_cond_str = "(%s)" % event
            else:
                # Wrap the event name and its condition
                current_cond_str = "({}) when ({})".format(
                    event, format_condition(condition)
                )

            formatted_conditions.add(current_cond_str)

        except StopIteration:
            break

    # Link conditions with the operator 'default'
    return "(" + ") default (".join(formatted_conditions) + ")"


def get_names_of_missing_physical_entities(dictPhysicalEntity):
    """Get URI and cadbiom name for each entity in the model.

    :param dictPhysicalEntity: Dictionnary of uris as keys and PhysicalEntities
        as values.
    :type dictPhysicalEntity: <dict>
    :return: Dictionnary of names as keys and uris as values.
    :rtype: <dict>
    """

    # Fix: Some components are created by our scripts
    # => ex: complexes with members involved that are classes
    # PS: classes are already in dictPhysicalEntity
    # PS: genes aren't in BioPAX format. getTransitions() from biopax2cadbiom
    # added these entities.
    cadbiom_names = {
        member_name: entity.uri
        for entity in dictPhysicalEntity.itervalues()
        for member_name in entity.cadbiom_names
    }

    # We want uri and cadbiom name for each entity in the model
    # Get all names and their uris
    # Fix: We overwrite NOW the previous dictionary, because data is less
    # accurate (all members/components inherit of the uri of their parent
    # which is less acurate for entities that are not created from sratch)
    cadbiom_names.update(
        {entity.cadbiom_name: entity.uri for entity in dictPhysicalEntity.itervalues()}
    )

    return cadbiom_names


def build_json_data(entity):
    """Build JSON data about the given entities.

    .. note:: We can handle reactions from dictReaction,
        or entities from dictPhysicalEntity

    .. note:: Return these attributes if they exist:

        - PhysicalEntity:
            - uri
            - entityType
            - name + synonyms
            - entityRef
            - location
            - modificationFeatures
            - members
            - reactions
        - Reaction:
            - uri
            - interactionType

    :param entity: URI of an entity or a list of reactions.
    :type entity: <str>
    :return: JSON formatted str.
    :rtype: <str>
    """

    def remove_none_attributes(data):
        """Remove default/empty values in given dict"""
        return {attr_name: attr for attr_name, attr in data.iteritems() if attr}

    # Handle PhysicalEntities
    if entity.__class__.__name__ == "PhysicalEntity":

        data = {
            "uri": entity.uri,
            "entityType": entity.entityType,
            "names": [
                name for name in entity.synonyms | set([entity.name])
                if name is not None
            ],  # Avoid None names that became null values in JSON
            "entityRef": entity.entityRef,
            "location": entity.location.name if entity.location else None,
            "modificationFeatures": dict(entity.modificationFeatures),
            "members": list(entity.members),
            "reactions": [reaction.uri for reaction in entity.reactions],
            "xrefs": entity.xrefs,
        }
        # Remove None attributes
        return json.dumps(remove_none_attributes(data))

    # Handle reactions
    else:

        data = list()
        for reaction in entity:
            # It must be a Reaction object
            if reaction.__class__.__name__ != "Reaction":
                raise Exception(
                    "Object <" + reaction + "> in transition is not supported!"
                )

            temp = {
                "uri": reaction.uri,
                "interactionType": reaction.interactionType,
                #"pathways": list(reaction.pathways),
            }
            # Remove None attributes
            data.append(remove_none_attributes(temp))

        return json.dumps(data)


def create_cadbiom_model(dictTransition, dictPhysicalEntity, dictReaction,
                         model_name, file_path):
    """Export data into a Cadbiom file format.

    :param dictTransition: Dictionnary of transitions and their respective
        set of events.

        Example:
            .. code:: python

                subDictTransition[(cadbiomL,right)].append({
                    'event': transition['event'],
                    'reaction': reaction,
                    'sympyCond': transitionSympyCond
                }

    :param dictPhysicalEntity: Dictionnary of biopax physicalEntities,
        created by the function query.get_biopax_physicalentities()
    :param dictReaction:
    :param model_name: Name of the model.
    :param file_path: File path.
    :type dictTransition: <dict <tuple <str>, <str>>: <list <dict>>>
    :type dictPhysicalEntity: <dict <str>: <PhysicalEntity>>
        keys: uris; values entity objects
    :type dictReaction: <dict>
    :type model_name: <str>
    :type file_path: <str>
    """
    # Header
    model = ET.Element(
        "model", xmlns="http://cadbiom.genouest.org/v2/", name=model_name
    )

    # Get all nodes in transitions

    # Put these nodes in the model
    # PS: why we don't do that in the following iteration of dictTransition ?
    # Because the cadbiom model is parsed from the top to the end ><
    # If nodes are at the end, the model will fail to be loaded...
    # Awesome.
    def write_nodes(name, uri):
        """Convenient func to add CSimpleNode entity"""
        ET.SubElement(
            model,
            "CSimpleNode",
            name=name,
            # xloc="0.0", yloc="0.0"
        ).text = build_json_data(dictPhysicalEntity[uri])

    cadbiomNodes = set()
    for ori_ext_nodes, transitions in dictTransition.iteritems():

        # In transitions (ori, ext)
        cadbiomNodes.update(ori_ext_nodes)
        # In conditions
        cadbiomNodes.update(
            str(atom)
            for transition in transitions
            for atom in transition["sympyCond"].atoms()
        )

    # We want uri and cadbiom name for each entity in the model
    cadbiom_names = get_names_of_missing_physical_entities(dictPhysicalEntity)

    [
        write_nodes(cadbiom_name, cadbiom_names[cadbiom_name])
        for cadbiom_name in cadbiomNodes
    ]

    ############################################################################
    # Anyway, next...
    # Get all transitions
    def write_transitions(ori_ext_nodes, event, condition, uris):
        """Convenient func to add a transition"""
        left_entity, right_entity = ori_ext_nodes
        # Note: use OrderedDict to have reproductible positions of xml attributes
        ET.SubElement(
            model, "transition",
            ori=left_entity, ext=right_entity,
            event=event,
            condition=condition,
        ).text = build_json_data([dictReaction[uri] for uri in uris])

    for ori_ext_nodes, transitions in dictTransition.iteritems():

        if len(transitions) == 1:
            # 1 transition = 1 event
            transition = transitions[0]
            write_transitions(
                ori_ext_nodes, transition["event"],  # nodes, event name
                format_condition(transition["sympyCond"]),  # condition
                [transition["reaction"]],  # reaction uri
            )

        else:
            # 1 transition = multiple events
            # => conditions go to event definition instead of condition of the transition
            events_conds = {
                tuple((transition["event"], transition["sympyCond"]))
                for transition in transitions
            }

            # Get all uris of reactions involved in this transition
            uris = [transition["reaction"] for transition in transitions]

            write_transitions(
                ori_ext_nodes,  # nodes
                format_events_and_conditions(events_conds),  # event
                "",  # condition
                uris,  # uris of reactions
            )

    # Add xml preamble and dump file
    tree = ET.ElementTree(model)
    tree.write(
        file_path,
        pretty_print=True, encoding="ASCII", xml_declaration=True, standalone=True
    )


def remove_scc_from_model(file_path):
    """Remove SCC (Strongly Connected Components) from a model

    The new model will be exported with "_without_scc" suffix in its filename.

    :param file_path: Path of a model (.bcx file)
    :type file_path: <str>
    """
    # Make a new model file
    ##mc.add_start_nodes(file_path)
    print("WARNING: remove for python 3 compatibility need to port cadbium core")


