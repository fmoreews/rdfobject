
import os,sys
import biopax2cadbium
#from rdflib import *
from rdflib.namespace import NamespaceManager
from rdflib import BNode, Namespace, Graph,URIRef

from rdflib.namespace import FOAF , XSD,RDF
from io import StringIO
from simplified_scrapy import utils, SimplifiedDoc, req
xml = '''
<rdf:RDF xmlns:rdf="http://w3.org/1999/02/22-rdf-syntax-ns#" 
         xmlns:foaf="http://xmlns.com/foaf/0.1/"
>

  <foaf:Person>
    <foaf:name>Peter Parker</foaf:name>
  </foaf:Person>

</rdf:RDF>
'''
# xml = utils.getFileContent('person_1.rdf')
doc = SimplifiedDoc(xml)
print(doc.select('foaf:Person>foaf:name>text()'))
# Or
print(doc.select('foaf:name>text()'))
# Or
print(doc.select('foaf:name'))

f=StringIO(xml)
g = Graph()
g.parse(f, format="xml")

print(len(g))



QB4O = Namespace('http://example.com/qb4o#')
n = Namespace('http://example.com/base-ns#')

#g = Graph()
g.namespace_manager = NamespaceManager(Graph())
g.namespace_manager.bind('qb4o', QB4O)
g.namespace_manager.bind('', n)

g.add((n['refPeriodStep1'], RDF.type, QB4O['HierarchyStep']))

g.serialize('test.ttl', format='turtle')



print(g.serialize(format="pretty-xml"))

print("============================")

def test_memory_store():
    g =  Graph("Memory")
    subj1 =  URIRef("http://example.org/foo#bar1")
    pred1 =  URIRef("http://example.org/foo#bar2")
    obj1 =  URIRef("http://example.org/foo#bar3")
    triple1 = (subj1, pred1, obj1)
    triple2 = (subj1, 
                URIRef("http://example.org/foo#bar4"),                
                URIRef("http://example.org/foo#bar5"))
    g.add(triple1)
    #self.assert_(len(g) == 1)
    g.add(triple2)
    #self.assert_(len(list(g.triples((subj1, None, None)))) == 2)
    #self.assert_(len(list(g.triples((None, pred1, None)))) == 1)
    #self.assert_(len(list(g.triples((None, None, obj1)))) == 1)
    g.remove(triple1)
    #self.assert_(len(g) == 1)
    g.serialize()
test_memory_store()
print("============================")


g = Graph()
fpath= os.path.abspath("./data/G6P.owl")
g.parse(fpath, format="xml")
#print(g.serialize(format="pretty-xml"))


print(len(g))


print("==end==")
 
