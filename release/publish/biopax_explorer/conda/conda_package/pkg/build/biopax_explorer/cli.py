from argparse import ArgumentParser
from biopax_explorer import __version__

def cli(args=None):
    p = ArgumentParser(
        description="BIOPAX-Explorer is a Python package, designed to ease the manipulation of BIOPAX datasets.It exploits RDF and SPARQL, using a simple object-oriented, domain specific syntax.",
        conflict_handler='resolve'
    )
    p.add_argument(
        '-V', '--version',
        action='version',
        help='Show the biopax_explorer version number and exit.',
        version="biopax_explorer %s" % __version__,
    )

    args = p.parse_args(args)


    print("CLI template")

    


if __name__ == '__main__':
    import sys
    cli(sys.argv[1:])
