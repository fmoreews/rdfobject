
from biopax.utils import gen_utils
 
###documentation helper
class score_DocHelper():
    
  def __init__(self):
    self.dmap=self.definitions()
    self.cln='Score'
    self.inst=gen_utils.define_model_instance(self.cln)
    self.tmap=self.attr_type_def()


  def classInfo(self):
    cln=self.cln
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       return m['class']
    return None
  
  def attributeNameString(self):
    cln=self.cln
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s\n" %(k)    
    return s

  def attributeNames(self):
    cln=self.cln
    al=[]
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         al.append(k)
    return al  

  def objectAttributeNames(self):
    cln=self.cln
    oa=self.inst.object_attributes()
    al=[]
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         if k in oa:
           al.append(k)
    return al    

  def typeAttributeNames(self):
    cln=self.cln
    ta=self.inst.type_attributes()
    al=[]
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         if k in ta:
           al.append(k)
    return al   


  def attributesInfo(self):
    cln=self.cln
    s=""
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       for k in atm.keys():
         s+="%s:" %(k)
         s+="\n%s" %(atm[attn])
    return s

  def attributeInfo(self,attn):
    cln=self.cln
    if cln in self.dmap.keys():
       m=self.dmap[cln]
       atm= m['attribute']
       if attn in atm.keys():
          return atm[attn]
    return None

  def attributeType(self,attn):
    cln=self.cln
    if cln in self.dmap.keys():
       m=self.tmap[cln]
       atm= m['attribute']
       if attn in atm.keys():
          return atm[attn]
    return None


  def definitions(self):
    dmap=dict()
    ####################################
    # class Score
    dmap['Score']=dict()
    dmap['Score']['class']="""
Definition: A score associated with a publication reference describing how the score was determined, the name of the method and a comment briefly describing the method.
Usage:  The xref must contain at least one publication that describes the method used to determine the score value. There is currently no standard way of describing  values, so any string is valid.
Examples: The statistical significance of a result, e.g. "p<0.05".
    """
    dmap['Score']['attribute']=dict()
  
    dmap['Score']['attribute']['scoreSource']="""
This property defines the source of the scoring methodology --
a publication or web site describing the scoring methodology and the range of values.
    """
    dmap['Score']['attribute']['value']="""
The value of the score. This can be a numerical or categorical value.
    """
    dmap['Score']['attribute']['comment']="""
Comment on the data in the container class. This property should be used instead of the OWL documentation elements (rdfs:comment) for instances because information in 'comment' is data to be exchanged, whereas the rdfs:comment field is used for metadata about the structure of the BioPAX ontology.
    """
    dmap['Score']['attribute']['scoreSource']="""
This property defines the source of the scoring methodology --
a publication or web site describing the scoring methodology and the range of values.
    """
    dmap['Score']['attribute']['value']="""
The value of the score. This can be a numerical or categorical value.
    """
    dmap['Score']['attribute']['comment']="""
Comment on the data in the container class. This property should be used instead of the OWL documentation elements (rdfs:comment) for instances because information in 'comment' is data to be exchanged, whereas the rdfs:comment field is used for metadata about the structure of the BioPAX ontology.
    """
  
    return dmap


  def attr_type_def(self):
    dmap=dict()
    ####################################
    # class Score
    dmap['Score']=dict()
    dmap['Score']['attribute']=dict()
    dmap['Score']['attribute']['scoreSource']="Provenance"
    dmap['Score']['attribute']['value']="str"
    dmap['Score']['attribute']['comment']="str"
    dmap['Score']['attribute']['scoreSource']="Provenance"
    dmap['Score']['attribute']['value']="str"
    dmap['Score']['attribute']['comment']="str"
  
    return dmap    