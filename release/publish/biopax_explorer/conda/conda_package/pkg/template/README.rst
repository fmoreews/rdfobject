===============================
biopax_explorer
===============================


.. image:: https://img.shields.io/travis/fjrmoreews/biopax_explorer.svg
        :target: https://travis-ci.org/fjrmoreews/biopax_explorer
.. image:: https://circleci.com/gh/fjrmoreews/biopax_explorer.svg?style=svg
    :target: https://circleci.com/gh/fjrmoreews/biopax_explorer
.. image:: https://codecov.io/gh/fjrmoreews/biopax_explorer/branch/master/graph/badge.svg
   :target: https://codecov.io/gh/fjrmoreews/biopax_explorer


BIOPAX manipulation in python
