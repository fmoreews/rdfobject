from setuptools import setup
import versioneer

requirements = [
     'matplotlib','numpy','pandas','lxml'
]

setup(
    name='biopax_explorer',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="A python library for BIOPAX manipulation",
    license="MIT",
    author="FJR Moreews",
    author_email='fjrmoreews@gmail.com',
    url='https://forgemia.inra.fr/pegase/biopax-explorer',
    packages=['biopax_explorer'],
    entry_points={
        'console_scripts': [
            'biopax_explorer=biopax_explorer.cli:cli'
        ]
    },
    install_requires=requirements,
    keywords='biopax_explorer',
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ]
)
