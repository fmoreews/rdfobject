#!/bin/bash



function copyd(){
  # copy  source to dest folder with excluded path
  sourcefolder=$1
  destfolder=$2
  excludefolder=$3
  echo rsync -av --progress $sourcefolder /$destfolder --exclude $excludefolder
  rsync -av --progress $sourcefolder /$destfolder --exclude $excludefolder

}



RDIR="/home/fmoreews/git"
DDIR="/home/fmoreews/git/rdfobject/release"


copyd "$RDIR/rdfobject/dev/script/rdfobj" "$DDIR/rdfobject/src/"  "__pycache__"
copyd "$RDIR/rdfobject/dev/script/template/python" "$DDIR/rdfobject/rdfobj_template/"  "__pycache__"


copyd "$RDIR/rdfobject/dev/script/biopax_explorer" "$DDIR/biopax_explorer/src/"  "__pycache__"


