from neo4j import GraphDatabase
from biopax_explorer.pattern.pattern import PatternExecutor, Pattern
from biopax_explorer.query import  EntityNode
from biopax_explorer.biopax import *
from biopax_explorer.biopax.utils import gen_utils as gu
import re
import json
import os
import random
import logging



class CypherScript :


    def __init__(self, server, port, ip, user, password, classesDict):
        self.SERVER = server
        self.PORT = port
        self.IP = ip
        self.USER = user
        self.PASSWORD = password
        self.classesDict = classesDict
        logging.basicConfig(level=logging.INFO)



    # -------> CONNECTION PART <-------


    # Connection to Neo4j database and return db session
    def connect(self):
        self.db_session = GraphDatabase.driver(self.IP,  auth=(self.USER, self.PASSWORD)).session()
        logging.info("Connection complete")  


    # add in Database new entities of a file
    def insert_or_update(self,filename):
        # collect file data
        data = self.collect_data(filename)

        prefix = chr(97 + random.randint(0, 25))

        # Create table to memorize all relations
        relationsTab = []

        # add entity in database
        for entity_row in data:
            for entity in entity_row:
                if self.is_present(entity):
                    command1 = """MATCH (e:%s {uri:"%s"})
                                SET e.uid="%s"%s
                                RETURN e
                              """ 
                    query = command1 %(entity.__class__.__name__, entity.pk, prefix+ ":" + entity.get_uri_string(), self.set_attribut(entity,entity.type_attributes()))
                    self.db_session.run(query)  
                else :
                    command2 = """CREATE (e:%s {uid:$pk, uri:$uri%s}) RETURN e"""
                    query = command2 %(entity.__class__.__name__,self.build_attribut(entity,entity.type_attributes()))
                    self.db_session.run(query, pk=str(prefix+ ":" + entity.get_uri_string()), uri=entity.get_uri_string())
                    relationsTab.append((entity, entity.object_attributes()))

        logging.info("Nodes created")

        # build in neo4j all relation in table
        for (ent, attributes) in relationsTab:
            self.build_relation(ent,attributes)

        logging.info("Relations created")



    # -------> COLLECT FILE <-------


    # collect all data in file
    def collect_data(self,filename):

        p=Pattern()
        for key in gu.classesDict().values():
            entityNode = EntityNode("ALL", key())
            p.define(entityNode)

        dataset = "g6p"
        db = "http://db:3030"
        pe = PatternExecutor(db,dataset)
        pe.datasetFile(filename)

        logging.info("DataSet File import")

        return pe.fetchEntities(p, level=1, max_count=1000)



    # -------> BUILD NODE AND GRAPH PART <-------


    # build attribute part command in cypher
    def build_attribut(self, entity, entity_attr):
        command=""
        for attr in entity_attr:
            addAttr = """, %s: %s"""
            attrValue = getattr(entity,"get_" + attr)()
            if attrValue == None :
                command += addAttr %(attr, """null""")
            else :
                elemSkip = re.escape('"')
                elemInsert = re.sub(elemSkip, '' ,str(attrValue))
                command += addAttr %(attr, '"' + str(elemInsert) + '"')

        # pop is a attribute wich allow to fix node duplications
        command += ", pop:true"

        return command

    # build the set part
    def set_attribut(self, entity, entity_attr):
        command=""
        for attr in entity_attr:
            addAttr = """, e.%s= %s"""
            attrValue = getattr(entity,"get_" + attr)()
            if attrValue == None :
                command += addAttr %(attr, """null""")
            else :
                elemSkip = re.escape('"')
                elemInsert = re.sub(elemSkip, '' ,str(attrValue))
                command += addAttr %(attr, '"' + str(elemInsert) + '"')

        command += """, e.pop = true"""

        return command


    # build relations between two nodes
    def build_relation(self, entity, entity_attrs) :
        command1 = """MERGE (e1:%s {uri:"%s"}) 
                     MERGE (e2:%s {uri:"%s"}) 
                     CREATE (e1)-[:%s]->(e2)"""

        command2 = """MERGE (e1:%s {uri:"%s"}) 
                     MERGE (e2 {uri:"%s"}) 
                     CREATE (e1)-[:%s]->(e2)"""


        for attr in entity_attrs :
            
            entity_attr = getattr(entity,"_" + attr)

            if entity_attr == None :
                continue
            if isinstance(entity_attr, list) :
                for inst in entity_attr:
                    if self.is_present(inst): # attribute list part (not tested)
                        query = command2 %(entity.__class__.__name__, entity.pk, inst.pk, attr)
                        self.db_session.run(query)
                    else :
                        query = command1 %(entity.__class__.__name__, entity.pk, inst.__class__.__name__, inst.pk, attr)
                        self.db_session.run(query)
            else :
                if self.is_present(entity_attr):
                    query = command2 %(entity.__class__.__name__, entity.pk, entity_attr.pk, attr)
                    self.db_session.run(query)
                else :
                    query = command1 %(entity.__class__.__name__, entity.pk, entity_attr.__class__.__name__, entity_attr.pk, attr)
                    self.db_session.run(query)



        # insert pop = false forAll new node
        command = """ MATCH (n)
                      WHERE n.pop IS NULL
                      SET n.pop = false
                  """
        self.db_session.run(command)

    # check if entity is present in the database
    def is_present(self, entity):

        check = """MATCH (e {uri:"%s"}) return e"""

        result = self.db_session.run(check %(entity.pk))
        return not not result.single()


    # -------> SELECT PART <-------


    # return all neo4j database
    def select_all(self, with_relations):
        command = self.db_session.run("""Match (e) RETURN e""")
        
        objects = self.convert_to_obj(command)

        if with_relations == True :
            relation_command = self.db_session.run("""MATCH (e1)-[r]->(e2)
                                                      Return r,e1,e2""")
            relation_results = [record["r"] for record in relation_command]
            for i in relation_results:
                obj = i.nodes[0]['uri']
                ref = i.nodes[1]['uri']
                if isinstance(getattr(objects[obj],"_" + i.type), list): # pb classeDict
                    getattr(objects[obj],"_" + i.type).append(objects[ref])
                else :
                    setattr(objects[obj],"_" + i.type, objects[ref])


        return objects.values()



    # select all entity with entityName
    def select_type(self, entity_type):
        if entity_type in gu.classesDict().keys() :
            logging.error("No instance for %s" %(entity_type))
        else :
            command ="""Match (e:%s) return e""" %(entity_type)
                
            return self.convert_to_obj(self.db_session.run(command)).values()


    # select with equal condition
    def select_where_equal_condition(self, attr, attrValue, entity_type = None):
        command = ""
        if entity == None:
            command = """Match (e{%s:%s}) return e""" %(attr, '"' + attrValue + '"')
        else :
            if entity_type in gu.classesDict().keys() :
                logging.error("No instance for %s" %(entity_type))
            else :
                command = """Match (e:%s{%s:%s}) return e""" %(entity_type, attr, '"' + attrValue + '"')
        return self.convert_to_obj(self.db_session.run(command)).values()



    # convert a query result to a list of object
    def convert_to_obj(self, query_result):

        listObj = {}

        results = [record["e"] for record in query_result]

        if not (not results):
            
            for i in results:
                if i != None:
                    label = list(i.labels)
                    obj = gu.define_model_instance(label[0]) # Create an new instance of the entity name
                    if obj != None :

                        obj.pk = i['uri']
                        obj.pop_state = i['pop']

                        for attr in obj.type_attributes():
                            setattr(obj, f"_{attr}", i[attr])
                        
                        listObj[obj.pk] = obj
        
        return listObj



    # -------> DELETE PART <-------


    # Delete all Neo4j Database
    def delete_all(self):
        self.db_session.run("""MATCH ()-[r]->() DELETE r""")
        self.db_session.run("""Match (a) Delete a""")



    # -------> CLOSE PART <-------


    # close the current session
    def close_DB(self):
        self.db_session.close()