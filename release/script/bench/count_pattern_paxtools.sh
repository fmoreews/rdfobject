#BIOPAXFILE=G6P.owl 

BIOPAXFILE="$1"
ALLREP="$2"

 



function prp {

  LPATTERN=$1
  ADDQUOTE=$2
  PLABEL=$3
  LBIOPAXFILE=$BIOPAXFILE
  RPFILE=ptreport.txt
  LOGFILE=log.txt
 
  PAXTOOLS="paxtools-5.2.1.jar"
  CMDJAR="paxtool_pattern_cmd.jar"
  
  rm $RPFILE  2> /dev/null || true
  CMD="java -Xmx1024m  -Dpaxtools.model.safeset=list -cp $PWD:$CMDJAR:$PAXTOOLS  org.dyliss.Cmd2  $LPATTERN  $LBIOPAXFILE $RPFILE $PLABEL"
  echo $CMD
  $CMD > $LOGFILE
  echo "{\"$LPATTERN\":"  >> $ALLREP
  echo "cat $LOGFILE  | grep 'matches'  >> $ALLREP"
  cat $LOGFILE  | grep 'matches'  >> $ALLREP
   echo "}"  >> $ALLREP
  re='^[1-9]+$'
  if ! [[ $ADDQUOTE =~ $re ]] ; then
     #echo "error: Not a number" >&2; exit 1
     echo ","  >> $ALLREP
  fi
  
 }

echo "{\"count_report_by_pattern\":["  > $ALLREP

#prp  directed-relations 
#prp  controls-state-change-of
#prp  controls-state-change-of-but-a-participant
#prp  controls-state-change-of-both-ctrl-part
#prp  controls-state-change-of-through-controlling-small-mol
#prp  controls-state-change-of-through-binding-small-mol
#prp  controls-state-change-detailed
#prp  controls-state-change-ofphosphorylation
#prp  controls-transport-of
#prp  controls-expression-of
#prp  controls-expression-of-with-conversion
#prp  controls-state-change-of
#prp  controls-state-change-of-indirectly
#prp  consumption-controlled-by
#prp  controls-production-of
#prp  catalysis-precedes
#prp  chemical-affects-through-binding
#prp  chemical-affects-through-control
#prp  controls-transport-of-chemical
#prp  in-complex-with
#prp  interacts-with
#prp  neighbor-of
#prp  reacts-with
#prp  used-to-produce
#prp  related-genes-of-interactions
#prp  ubiquitous-molecule-lister 1


prp  controls-state-change-of 0 Control
prp  controls-state-change-ofphosphorylation 0  Control
prp  state-change 0 Conversion
prp  reacts-with 0  SMR1
prp  used-to-produce 0 Conv
prp  controls-expression-of 0 Control
prp  in-complex-with 0 Complex
prp  consumption-controlled-by 1 Control

 
echo "]}"  >> $ALLREP


 