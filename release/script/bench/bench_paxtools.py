import subprocess
import os
import json
import resource
import time

def run_pattern(biopaxfile, outputfile ):
    # Compile Java file
    
    command = "bash count_pattern_paxtools.sh \"%s\" \"%s\" " %(biopaxfile,outputfile)
    
 
    print("------------------------")
    print(command)
    print("------------------------")
    #subprocess.run(command, shell=True, check=True)

    start_time = time.time()
    start_memory = resource.getrusage(resource.RUSAGE_CHILDREN).ru_maxrss

    # Run the subprocess
    process = subprocess.Popen(command, shell=True)
    process.wait()

    end_time = time.time()
    end_memory = resource.getrusage(resource.RUSAGE_CHILDREN).ru_maxrss

    elapsed_time = end_time - start_time
    memory_usage = end_memory - start_memory

    print(f"Elapsed Time: {elapsed_time} seconds")
    print(f"Memory Usage: {memory_usage} KB")

cwd = os.getcwd()

outputfile="count_px.json"
datadir="data/biopax/"

"""
biopaxfile_dict={
"pc12_netpath":"pc12_netpath.owl",
"pc12_panther":"pc12_panther.owl",
"pc12_reactome":"pc12_reactome.owl",
"reactome_homo":"reactome_homo_sapiens.owl",
"reactome_mus":"reactome_mus_musculus.owl"
}
"""

biopaxfile_dict={
"reactome_mus":"reactome_mus_musculus.owl",
"reactome_homo":"reactome_homo_sapiens.owl"
}


report={}

for dataset, bfile in biopaxfile_dict.items():
  row=[]
  biopaxfile=datadir+bfile
  run_pattern(biopaxfile, outputfile )
  dataj=None
  with open(outputfile) as jf:
    file_contents = jf.read()
    dataj = json.loads(file_contents)
  print(dataj["count_report_by_pattern"])
  row=dataj["count_report_by_pattern"]
  report[dataset]=row

 

jsonfile="report_bench_PX.json"
with open(jsonfile, 'w') as f:
    json.dump(report, f)