package org.biopax.paxtools.pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.biopax.paxtools.controller.*;
import org.biopax.paxtools.converter.LevelUpgrader;
import org.biopax.paxtools.converter.psi.PsiToBiopax3Converter;
import org.biopax.paxtools.io.BioPAXIOHandler;
import org.biopax.paxtools.io.SimpleIOHandler;
import org.biopax.paxtools.io.gsea.GSEAConverter;
import org.biopax.paxtools.io.sbgn.L3ToSBGNPDConverter;
import org.biopax.paxtools.io.sbgn.ListUbiqueDetector;
import org.biopax.paxtools.io.sbgn.UbiqueDetector;
import org.biopax.paxtools.model.BioPAXElement;
import org.biopax.paxtools.model.BioPAXLevel;
import org.biopax.paxtools.model.Model;
import org.biopax.paxtools.model.level2.entity;
import org.biopax.paxtools.model.level3.*;
import org.biopax.paxtools.pattern.miner.*;
import org.biopax.paxtools.pattern.util.Blacklist;
import org.biopax.paxtools.query.QueryExecuter;
import org.biopax.paxtools.query.algorithm.Direction;
import org.biopax.paxtools.util.ClassFilterSet;


//
  
import java.util.Map.Entry;
  
import org.biopax.paxtools.pattern.PatternBox;
 
import org.biopax.paxtools.model.Model;
import org.biopax.paxtools.pattern.Match;
import org.biopax.paxtools.pattern.Pattern;
import org.biopax.paxtools.pattern.Searcher;
import org.biopax.paxtools.pattern.util.Blacklist;
import org.biopax.paxtools.pattern.util.ProgressWatcher;



import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;


import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryType;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;






 
import org.biopax.paxtools.controller.Fetcher;
import org.biopax.paxtools.controller.ModelUtils;
import org.biopax.paxtools.controller.SimpleEditorMap;


import org.biopax.paxtools.impl.level3.BiochemicalReactionImpl;
import org.biopax.paxtools.impl.level3.CatalysisImpl;
import org.biopax.paxtools.impl.level3.ComplexAssemblyImpl;
import org.biopax.paxtools.impl.level3.ComplexImpl;
import org.biopax.paxtools.impl.level3.ControlImpl;
import org.biopax.paxtools.impl.level3.ConversionImpl;
import org.biopax.paxtools.impl.level3.DegradationImpl;
import org.biopax.paxtools.impl.level3.EntityImpl;
import org.biopax.paxtools.impl.level3.ModulationImpl;
import org.biopax.paxtools.impl.level3.PhysicalEntityImpl;
import org.biopax.paxtools.impl.level3.ProteinImpl;
import org.biopax.paxtools.impl.level3.ProteinReferenceImpl;
import org.biopax.paxtools.impl.level3.SmallMoleculeReferenceImpl;
import org.biopax.paxtools.impl.level3.TemplateReactionImpl;
import org.biopax.paxtools.impl.level3.TemplateReactionRegulationImpl;
import org.biopax.paxtools.impl.level3.TransportImpl;

 

import org.biopax.paxtools.model.BioPAXElement;
import org.biopax.paxtools.model.Model;
import org.biopax.paxtools.model.level3.BioSource;
import org.biopax.paxtools.model.level3.BiochemicalReaction;
import org.biopax.paxtools.model.level3.CellularLocationVocabulary;
import org.biopax.paxtools.model.level3.ComplexAssembly;
import org.biopax.paxtools.model.level3.Control;
import org.biopax.paxtools.model.level3.ControlType;
import org.biopax.paxtools.model.level3.Controller;
import org.biopax.paxtools.model.level3.Conversion;
import org.biopax.paxtools.model.level3.Entity;
 

import org.biopax.paxtools.model.level3.Gene;
import org.biopax.paxtools.model.level3.Interaction;
import org.biopax.paxtools.model.level3.Named;
import org.biopax.paxtools.model.level3.NucleicAcid;
import org.biopax.paxtools.model.level3.PhysicalEntity;
import org.biopax.paxtools.model.level3.Process;
import org.biopax.paxtools.model.level3.Provenance;
import org.biopax.paxtools.model.level3.SequenceEntity;
import org.biopax.paxtools.model.level3.SequenceEntityReference;
import org.biopax.paxtools.model.level3.SimplePhysicalEntity;
import org.biopax.paxtools.model.level3.SmallMolecule;
import org.biopax.paxtools.model.level3.SmallMoleculeReference;
import org.biopax.paxtools.model.level3.Stoichiometry;
import org.biopax.paxtools.model.level3.TemplateReactionRegulation;
import org.biopax.paxtools.model.level3.XReferrable;
import org.biopax.paxtools.model.level3.Xref;


 
 
    
/*
 * custom util to read private prop 
 */
public class PUtils {

    private static String getLabel(Pattern p,int i)
	{
		for (String label : p.labelMap.keySet())
		{
			if (p.labelMap.get(label) == i) return label;
		}
		return null;
	}

    public static void displayLabels(Pattern p){
        int i=-1;
        Boolean goon=true;
        System.out.println("====START_L====");
        while (goon==true){
            i=i+1;
            try {
                String label=getLabel(p,i);
                System.out.println("LABEL:"+label);
                if( label==null){
                    goon=false;
                }
            } catch (Exception e) {
                goon=false;
            }
             
        }

    }
 
  


}
