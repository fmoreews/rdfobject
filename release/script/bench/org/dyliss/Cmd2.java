package org.dyliss;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.biopax.paxtools.controller.*;
import org.biopax.paxtools.converter.LevelUpgrader;
import org.biopax.paxtools.converter.psi.PsiToBiopax3Converter;
import org.biopax.paxtools.io.BioPAXIOHandler;
import org.biopax.paxtools.io.SimpleIOHandler;
import org.biopax.paxtools.io.gsea.GSEAConverter;
import org.biopax.paxtools.io.sbgn.L3ToSBGNPDConverter;
import org.biopax.paxtools.io.sbgn.ListUbiqueDetector;
import org.biopax.paxtools.io.sbgn.UbiqueDetector;
import org.biopax.paxtools.model.BioPAXElement;
import org.biopax.paxtools.model.BioPAXLevel;
import org.biopax.paxtools.model.Model;
import org.biopax.paxtools.model.level2.entity;
import org.biopax.paxtools.model.level3.*;
import org.biopax.paxtools.pattern.miner.*;
import org.biopax.paxtools.pattern.util.Blacklist;
import org.biopax.paxtools.query.QueryExecuter;
import org.biopax.paxtools.query.algorithm.Direction;
import org.biopax.paxtools.util.ClassFilterSet;


//
  
import java.util.Map.Entry;
  
import org.biopax.paxtools.pattern.PatternBox;
 
import org.biopax.paxtools.model.Model;
import org.biopax.paxtools.pattern.Match;
import org.biopax.paxtools.pattern.Pattern;
import org.biopax.paxtools.pattern.Searcher;
import org.biopax.paxtools.pattern.util.Blacklist;
import org.biopax.paxtools.pattern.util.ProgressWatcher;



import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;


import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryType;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;






 
import org.biopax.paxtools.controller.Fetcher;
import org.biopax.paxtools.controller.ModelUtils;
import org.biopax.paxtools.controller.SimpleEditorMap;


import org.biopax.paxtools.impl.level3.BiochemicalReactionImpl;
import org.biopax.paxtools.impl.level3.CatalysisImpl;
import org.biopax.paxtools.impl.level3.ComplexAssemblyImpl;
import org.biopax.paxtools.impl.level3.ComplexImpl;
import org.biopax.paxtools.impl.level3.ControlImpl;
import org.biopax.paxtools.impl.level3.ConversionImpl;
import org.biopax.paxtools.impl.level3.DegradationImpl;
import org.biopax.paxtools.impl.level3.EntityImpl;
import org.biopax.paxtools.impl.level3.ModulationImpl;
import org.biopax.paxtools.impl.level3.PhysicalEntityImpl;
import org.biopax.paxtools.impl.level3.ProteinImpl;
import org.biopax.paxtools.impl.level3.ProteinReferenceImpl;
import org.biopax.paxtools.impl.level3.SmallMoleculeReferenceImpl;
import org.biopax.paxtools.impl.level3.TemplateReactionImpl;
import org.biopax.paxtools.impl.level3.TemplateReactionRegulationImpl;
import org.biopax.paxtools.impl.level3.TransportImpl;

//import org.biopax.paxtools.model.level3.EntityReferenceImpl;



import org.biopax.paxtools.model.BioPAXElement;
import org.biopax.paxtools.model.Model;
import org.biopax.paxtools.model.level3.BioSource;
import org.biopax.paxtools.model.level3.BiochemicalReaction;
import org.biopax.paxtools.model.level3.CellularLocationVocabulary;
import org.biopax.paxtools.model.level3.ComplexAssembly;
import org.biopax.paxtools.model.level3.Control;
import org.biopax.paxtools.model.level3.ControlType;
import org.biopax.paxtools.model.level3.Controller;
import org.biopax.paxtools.model.level3.Conversion;
import org.biopax.paxtools.model.level3.Entity;
 

import org.biopax.paxtools.model.level3.Gene;
import org.biopax.paxtools.model.level3.Interaction;
import org.biopax.paxtools.model.level3.Named;
import org.biopax.paxtools.model.level3.NucleicAcid;
import org.biopax.paxtools.model.level3.PhysicalEntity;
import org.biopax.paxtools.model.level3.Process;
import org.biopax.paxtools.model.level3.Provenance;
import org.biopax.paxtools.model.level3.SequenceEntity;
import org.biopax.paxtools.model.level3.SequenceEntityReference;
import org.biopax.paxtools.model.level3.SimplePhysicalEntity;
import org.biopax.paxtools.model.level3.SmallMolecule;
import org.biopax.paxtools.model.level3.SmallMoleculeReference;
import org.biopax.paxtools.model.level3.Stoichiometry;
import org.biopax.paxtools.model.level3.TemplateReactionRegulation;
import org.biopax.paxtools.model.level3.XReferrable;
import org.biopax.paxtools.model.level3.Xref;

//custom
 import  org.biopax.paxtools.pattern.PUtils;
 
    
/*
 * BioPAX data tools.
 * This is only called from Main class.
 */
public class Cmd2 {

 
 
 
	private static Blacklist blacklist;
    private static String outputFileName;
    // private static String modelFileName;
    private static String urlName;
    private static Boolean doCustomURL=false;
   // private static Boolean doCustomFile=true;
    private static Boolean doPC=false;      

    
public static void error(String msg) {
    System.out.println("-------------------");
    System.out.println(msg);
    System.out.println("-------------------");
    System.exit(0);
}   
public static void jsonresult(String msg) {
    System.out.println("jsonresult:");
    System.out.println(msg);
    System.out.println("");
    System.exit(0);
}     

public static void display(String msg) {
    System.out.println("   #-----  "+msg);
    

}

public   Pattern selectPattern(String ps){


          Pattern p = null;
 
          Blacklist blacklist =null;
	 if(ps.equals("controls-state-change-of")){
	    p = PatternBox.controlsStateChange();
	 }
	 if(ps.equals("state-change")){
	    p = PatternBox.stateChange(null,null);
	 }
	if(ps.equals( "controls-state-change-ofphosphorylation")){
	    p = PatternBox.controlsPhosphorylation();
	 }
	if(ps.equals( "reacts-with")){
	   p = PatternBox.reactsWith(blacklist);
	 }
	 if(ps.equals(  "used-to-produce")){
	 p = PatternBox.usedToProduce(blacklist);
	 }
	if(ps.equals("controls-expression-of")){
	       p = PatternBox.controlsExpressionWithTemplateReac();
		 
	 }
	 		
	if(ps.equals( "in-complex-with")){
	    p = PatternBox.inComplexWith();
	 }
	if(ps.equals( "consumption-controlled-by")){
   	      p=PatternBox.controlsMetabolicCatalysis(blacklist, true);
   	      
	 }
	 if(ps.equals("controls-expression-of-with-conv")){
	      p = PatternBox.controlsExpressionWithConversion();
		
	 }
	 //	p = PatternBox.inSameComplex();
	 //p = PatternBox.controlsMetabolicCatalysis(blacklist, true);


  return p;

}

 


public static void main(String[] args) {
    System.out.println("start processing");
    if (args==null || args.length<3){
        error("parameter not defined, usage: cmd pattername biopaxFileName");
    }

    else{
        String pattername=args[0];

        String biopaxFileName=args[1];
        String outputFileName=args[2];
        String selectedLabel=args[3];
        Cmd2 cmd = new Cmd2();
        Pattern  selectedPattern=cmd.selectPattern(pattername);
        if (selectedPattern ==null){
			 
            error("unknown or undefined  pattern " + pattername);
        } else{
            display("selected  pattern is : " + pattername);
            //PUtils.displayLabels(selectedPattern);

        }  

        if (biopaxFileName ==null){
            error("undefined  file parameter " + biopaxFileName);
            
        } else{
            File f1 = new File(biopaxFileName);
            if (! f1.exists()) {
                error("file not found : " + biopaxFileName);
            }
            display("selected  file is : " + biopaxFileName);
        }  

        display("selected  pattern   is : " + selectedPattern);
        cmd.run( selectedPattern,biopaxFileName ,outputFileName,selectedLabel);
 }



}



/*

		public static List<Match> searchPlain(Model model, Pattern pattern)
	{
		List<Match> list = new LinkedList<Match>();

		Map<BioPAXElement, List<Match>> map = search(model, pattern);
		for (List<Match> matches : map.values())
		{
			list.addAll(matches);
		}
		return list;
	}
	
*/






/* 
public static boolean keepit(BioPAXElement k) {

 
						boolean doadd=false;
						
						if(k.getClass().isAssignableFrom(EntityReference.class)  || k  instanceof EntityReference  )   {
							  doadd=true;
						}
						else if(k.getClass().isAssignableFrom(PhysicalEntity.class)  || k  instanceof PhysicalEntity  ){
							  doadd=true;
						} 
						 
						else if(k.getClass().isAssignableFrom(Interaction.class)  || k  instanceof Interaction  ){
							  doadd=true;
						}
						 																							
                                              						 
						
                      return 	doadd;					

}*/


/*  
    public static boolean keepitByLabel(Pattern p,Match ma,String splabel) {
		boolean doadd=false;
		if(splabel!=null){
		    BioPAXElement el = ma.get(splabel,p);
            if(el!=null){
			    doadd=true;
			}
		}

		return doadd;
	}*/
   /*  
	public static boolean keepitall(BioPAXElement k) {

 
						boolean doadd=false;
						
						if(k.getClass().isAssignableFrom(SequenceEntityReference.class) || k  instanceof SequenceEntityReference ){
							//SequenceEntityReference o = (SequenceEntityReference) k;
							//klbl+=": "+o.getDisplayName();
							 doadd=true;
						}
						else if(k.getClass().isAssignableFrom(ProteinReferenceImpl.class)){
							//ProteinReferenceImpl o = (ProteinReferenceImpl) k;
							//klbl+=":: "+o.getDisplayName();
							 doadd=true;
						}
						else if(k.getClass().isAssignableFrom(SmallMoleculeReference.class)  || k  instanceof SmallMoleculeReference  ){
							//SmallMoleculeReference o = (SmallMoleculeReference) k;
							//klbl+=":: "+o.getDisplayName();
							 doadd=true;
						}
						else if(k.getClass().isAssignableFrom(EntityReference.class)  || k  instanceof EntityReference  )   {
							  doadd=true;
						}
						else if(k.getClass().isAssignableFrom(Entity.class)  || k  instanceof Entity  ){
							  doadd=true;
						}
						//else if(k.getClass().isAssignableFrom(EntityReferenceImpl.class)  || k  instanceof EntityReferenceImpl  )   {
						//	  doadd=true;
						//}
						else if(k.getClass().isAssignableFrom(EntityImpl.class)  || k  instanceof EntityImpl  ){
							  doadd=true;
						}
						else if(k.getClass().isAssignableFrom(PhysicalEntity.class)  || k  instanceof PhysicalEntity  ){
							  doadd=true;
						}
						else if(k.getClass().isAssignableFrom(PhysicalEntityImpl.class)  || k  instanceof PhysicalEntityImpl  ){
							  doadd=true;
						}
						 else if(k.getClass().isAssignableFrom(ProteinImpl.class)  || k  instanceof ProteinImpl  ){
							  doadd=true;
						}
						 else if(k.getClass().isAssignableFrom(ComplexImpl.class)  || k  instanceof ComplexImpl  ){
							  doadd=true;
						}
						 else if(k.getClass().isAssignableFrom(BiochemicalReactionImpl.class)  || k  instanceof BiochemicalReactionImpl  ){
							  doadd=true;
						}
						 else if(k.getClass().isAssignableFrom(Conversion.class)  || k  instanceof Conversion  ){
							  doadd=true;
						}
						
						 else if(k.getClass().isAssignableFrom(Control.class)  || k  instanceof Control  ){
							  doadd=true;
						}
						
						 else if(k.getClass().isAssignableFrom(PhysicalEntityImpl.class)  || k  instanceof PhysicalEntityImpl  ){
							  doadd=true;
						}
						
						 else if(k.getClass().isAssignableFrom(PhysicalEntityImpl.class)  || k  instanceof PhysicalEntityImpl  ){
							  doadd=true;
						}
						else {
						  
						}																								
                                              						 
						
                        return 	doadd;					
}
*/

	public static String id( BioPAXElement o) {
  	    String id=o.toString();
	    
	     
	    return id;
	}
	public static int summaryCount(Pattern p, Map<BioPAXElement,
	 List<Match>> map, String selectedplabel) {
		 
               HashMap<String, Integer> uniq = new HashMap<String, Integer>();		
	 
		if(map==null){return 0;}
 
		for(Entry<BioPAXElement, List<Match>> ent  : map.entrySet()){
			
			BioPAXElement k = ent.getKey();
			if(k!=null){
	            /*
                boolean doadd=keepitByLabel(p,k,selectedplabel);
          		
				if(doadd==true){ 
						  String idk=id(k);
						  
						  if (idk!=null && !uniq.containsKey(idk)){
  						    uniq.put(idk,1);
						   System.out.println(idk) ;						    
						  }
				}*/


                List<Match> matches = ent.getValue();
				  
				for(Match ma : matches){

					BioPAXElement el = ma.get(selectedplabel,p);
                    if(el!=null){
						String idm=id(el);

                        if (idm!=null  &&  !uniq.containsKey(idm)){
  									    uniq.put(idm,1);
									   // System.out.println(idm) ;
						 }
					}

							/*for(BioPAXElement b:ma.getVariables()){
								if(b!=null){
								        
								        
									boolean doaddm=keepitByLabel(p,ma,selectedplabel); 
									if(doaddm==true){ 
									  String idm=id(b);

									  if (idm!=null  &&  !uniq.containsKey(idm)){
  									    uniq.put(idm,1);
									    System.out.println(idm) ;
									  }
									 
									
									}
								 
								}
							}*/
		        }

				}
		}
		 
		return uniq.size();
	}



 public void run(Pattern p, String modelFileName,String outputFileName, String selectedplabel){


        File modFile = new File(modelFileName);
        if (!modFile.exists())
	   		{
				display("Cannot find the model file:"+modelFileName);
		 
				return;
			}

           SimpleIOHandler io = new SimpleIOHandler();
		Model model;

		try
		{
			model = io.convertFromOWL(new FileInputStream(modFile));
			 
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			 
			display("File not found: " + modFile.getPath());
			return;
		}
      Map<BioPAXElement,List<Match>> matches = Searcher.search(model, p, null);
  
  
      File outFile = new File(outputFileName);
 
 		if (matches.isEmpty())
		{
			display("No results found!");
			
			try
			{
				BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
				writer.write("");
				writer.close();
				 
			}
			catch (IOException e)
			{
				e.printStackTrace();
				 
			}
			jsonresult("{\"matches\":"+0+"}");

		}
		else
		{
		
			try
			{
			
				display("Writing result");
				
				 
				FileOutputStream os = new FileOutputStream(outFile);
	//			writeResult(matches, os);
				os.close();
				 
			}
			catch (IOException e)
			{
				e.printStackTrace();
				 
				display("Error occurred while writing the results");
				return;
			}
 
			display("Success!    ");
			
			Integer size=summaryCount( p,matches,selectedplabel);
			//jsonresult("{\"matches\":"+matches.size()+"}");
			jsonresult("{\"matches\":"+size+"}");
			
			 
			
		}
		
		
		
		
		
 
 
 }
 
 
 /*
 public void writeResult(Map<BioPAXElement, List<Match>> matches, OutputStream out)
		throws IOException
	{
		OutputStreamWriter writer = new OutputStreamWriter(out);


		for (BioPAXElement ele : matches.keySet())
		{
			Set<String> syms = new HashSet<>();

			for (Match m : matches.get(ele))
			{
			//	ProteinReference pr = (ProteinReference) m.get("PR", getPattern());

			//	String sym = getGeneSymbol(pr);
			//	if (sym != null) syms.add(sym);
			}

			if (syms.size() > 1)
			{
				writer.write("\n" + ele.getUri());

				for (Object o : controlAcc.getValueFromBean(ele))
				{
					Control ctrl = (Control) o;
					writer.write(" " + ctrl.getUri());
				}

				for (String sym : syms)
				{
					writer.write("\t" + sym);
				}
			}
		}
		
		writer.flush();
	}

  */
	
	


}
