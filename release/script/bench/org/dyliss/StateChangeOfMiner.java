

package org.dyliss;


import org.biopax.paxtools.pattern.miner.*;
import org.biopax.paxtools.pattern.*;


import org.biopax.paxtools.pattern.Pattern;
import org.biopax.paxtools.pattern.PatternBox;



import org.biopax.paxtools.model.level3.*;
import org.biopax.paxtools.pattern.constraint.*;
import org.biopax.paxtools.pattern.util.Blacklist;
import org.biopax.paxtools.pattern.util.RelType;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.biopax.paxtools.pattern.constraint.ConBox.*;


/**
 * Miner for the controls-state-change pattern.
 * @author Ozgun Babur
 */
public class StateChangeOfMiner extends AbstractSIFMiner
{
	/**
	 * Constructor for extension purposes.
	 * @param name name of the miner
	 * @param description description of the miner
	 */
	 /*
	protected StateChangeOfMiner(String name, String description)
	{
		super(SIFEnum.STATE_CHANGE_OF, name, description);
	}
	*/

	/**
	 * Empty constructor.
	 */
	public StateChangeOfMiner()
	{
		super(SIFEnum.CONTROLS_STATE_CHANGE_OF);
		this.name="state-change";
	}

	/**
	 * Constructor for extension purposes.
	 * @param type relation type
	 */
	 /*
	protected StateChangeOfMiner(SIFType type)
	{
		super(type);
	}
*/
	/**
	 * Constructs the pattern.
	 * @return pattern
	 */
	@Override
	public Pattern constructPattern()
	{
		return cut_stateChange(null,null);
	}


	public static Pattern cut_stateChange(Pattern p, String ctrlLabel)
	{
	        System.out.println("-------------cut_stateChange------------------"); 
		if (p == null) p = new Pattern(Conversion.class, "Conversion");

		if (ctrlLabel == null) p.add(new Participant(RelType.INPUT), "Conversion", "input PE");
		else p.add(new Participant(RelType.INPUT, true), ctrlLabel, "Conversion", "input PE");

		p.add(linkToSpecific(), "input PE", "input simple PE");
		p.add(peToER(), "input simple PE", "changed generic ER");
		p.add(new ConversionSide(ConversionSide.Type.OTHER_SIDE), "input PE", "Conversion", "output PE");
		p.add(equal(false), "input PE", "output PE");
		p.add(linkToSpecific(), "output PE", "output simple PE");
		p.add(peToER(), "output simple PE", "changed generic ER");
		p.add(linkedER(false), "changed generic ER", "changed ER");
		System.out.println(p);
		return p;
	}
	
	


	@Override
	public String getTargetLabel()
	{
		return "input PE";
	}
	
	@Override
	public String getSourceLabel()
	{
		return "output PE";
	}


/*



	@Override
	public String[] getMediatorLabels()
	{
		return new String[]{"Control", "Conversion"};
	}

	@Override
	public String[] getSourcePELabels()
	{
		return new String[]{"controller simple PE", "controller PE"};
	}

	@Override
	public String[] getTargetPELabels()
	{
		return new String[]{"input PE", "input simple PE", "output PE", "output simple PE"};
	}
	
	*/
}
