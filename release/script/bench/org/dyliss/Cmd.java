package org.dyliss;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.biopax.paxtools.controller.*;
import org.biopax.paxtools.converter.LevelUpgrader;
import org.biopax.paxtools.converter.psi.PsiToBiopax3Converter;
import org.biopax.paxtools.io.BioPAXIOHandler;
import org.biopax.paxtools.io.SimpleIOHandler;
import org.biopax.paxtools.io.gsea.GSEAConverter;
import org.biopax.paxtools.io.sbgn.L3ToSBGNPDConverter;
import org.biopax.paxtools.io.sbgn.ListUbiqueDetector;
import org.biopax.paxtools.io.sbgn.UbiqueDetector;
import org.biopax.paxtools.model.BioPAXElement;
import org.biopax.paxtools.model.BioPAXLevel;
import org.biopax.paxtools.model.Model;
import org.biopax.paxtools.model.level2.entity;
import org.biopax.paxtools.model.level3.*;
import org.biopax.paxtools.pattern.miner.*;
import org.biopax.paxtools.pattern.util.Blacklist;
import org.biopax.paxtools.query.QueryExecuter;
import org.biopax.paxtools.query.algorithm.Direction;
import org.biopax.paxtools.util.ClassFilterSet;


//
 
import org.biopax.paxtools.model.Model;
import org.biopax.paxtools.pattern.Match;
import org.biopax.paxtools.pattern.Pattern;
import org.biopax.paxtools.pattern.Searcher;
import org.biopax.paxtools.pattern.util.Blacklist;
import org.biopax.paxtools.pattern.util.ProgressWatcher;



import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;


import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryType;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;


/*
 * BioPAX data tools.
 * This is only called from Main class.
 */
public class Cmd {


	/**
	 * User specified miners to use.
	 */
	private Miner[] miners;

	/**
	 * Checkbox for downloading and using PC data.
	 */
	  

	/**
	 * Prefix of URL of the Pathway Commons data.
	 */
	private static final String PC_DATA_URL_PREFIX =
		"http://www.pathwaycommons.org/pc2/downloads/Pathway%20Commons.4.";
//		"http://webservice.baderlab.org:48080/downloads/Pathway%20Commons%202%20";

	/**
	 * Suffix of URL of the Pathway Commons data.
	 */
	private static final String PC_DATA_URL_SUFFIX = ".BIOPAX.owl.gz";

 
	/**
	 * Names of Pathway Commons resources.
	 */
	private static final Object[] PC_RES_NAMES = new Object[]{
		"All-Data", "Reactome", "NCI-PID", "HumanCyc", "PhosphoSitePlus", "PANTHER"};

	/**
	 * The URL components of the Pathway Commons resources.
	 */
	private static final String[] PC_RES_URL = new String[]{
		"All", "Reactome", "NCI_Nature", "HumanCyc", "PhosphoSitePlus", "PANTHER%20Pathway"};

	/**
	 * The name of the file for IDs of ubiquitous molecules.
	 */
	private static final String UBIQUE_FILE = "blacklist.txt";

	/**
	 * The url of the file for IDs of ubiquitous molecules.
	 */
	private static final String UBIQUE_URL = "http://www.pathwaycommons.org/pc2/downloads/blacklist.txt";

	/**
	 * Blacklist for detecting ubiquitous small molecules.
	 */
	private static Blacklist blacklist;
    private static String outputFileName;
    // private static String modelFileName;
    private static String urlName;
    private static Boolean doCustomURL=false;
   // private static Boolean doCustomFile=true;
    private static Boolean doPC=false;      

    
public static void error(String msg) {
    System.out.println("-------------------");
    System.out.println(msg);
    System.out.println("-------------------");
    System.exit(0);
}   
public static void jsonresult(String msg) {
    System.out.println("jsonresult:");
    System.out.println(msg);
    System.out.println("");
    System.exit(0);
}     

public static void main(String[] args) {
    System.out.println("start processing");
    if (args==null || args.length<3){
        error("parameter not defined, usage: cmd pattername biopaxFileName");
    }

    else{
        String pattername=args[0];
        String biopaxFileName=args[1];
        String outputFileName=args[2];
    
        Cmd cmd = new Cmd();
        List<Miner> minerList =  cmd.getAvailableMinerList();
        Miner selectedMiner=null;
        for(  Miner mi : minerList){
            //System.out.println(mi.getName());
            if (pattername.equals(mi.getName())){
                selectedMiner=mi;
            }
        }
        if (selectedMiner ==null){
			for(  Miner mi : minerList){
				System.out.println(mi.getName());
				if (pattername.equals(mi.getName())){
					selectedMiner=mi;
				}
			}
            error("unknown or undefined  pattern " + pattername);
        } else{
            display("selected  pattern is : " + pattername);
        }  

        if (biopaxFileName ==null){
            error("undefined  file parameter " + biopaxFileName);
            
        } else{
            File f1 = new File(biopaxFileName);
            if (! f1.exists()) {
                error("file not found : " + biopaxFileName);
            }
            display("selected  file is : " + biopaxFileName);
        }  

        display("selected  miner name is : " + selectedMiner.getName());
        cmd.run( selectedMiner,biopaxFileName ,outputFileName);
 }



}


private List<Miner> getAvailableMinerList()
{
    List<Miner> minerList = new ArrayList<>();
    if (miners != null && miners.length > 0)
    {
        minerList.addAll(Arrays.asList(miners));
    }
    else
    {
        minerList.add(new StateChangeOfMiner());
         
         
        minerList.add(new DirectedRelationMiner());
        minerList.add(new ControlsStateChangeOfMiner());
        minerList.add(new CSCOButIsParticipantMiner());
        minerList.add(new CSCOBothControllerAndParticipantMiner());
        minerList.add(new CSCOThroughControllingSmallMoleculeMiner());
        minerList.add(new CSCOThroughBindingSmallMoleculeMiner());
        minerList.add(new ControlsStateChangeDetailedMiner());
        minerList.add(new ControlsPhosphorylationMiner());
        minerList.add(new ControlsTransportMiner());
        minerList.add(new ControlsExpressionMiner());
        minerList.add(new ControlsExpressionWithConvMiner());
        minerList.add(new CSCOThroughDegradationMiner());
        minerList.add(new ControlsDegradationIndirectMiner());
        minerList.add(new ConsumptionControlledByMiner());
        minerList.add(new ControlsProductionOfMiner());
        minerList.add(new CatalysisPrecedesMiner());
        minerList.add(new ChemicalAffectsThroughBindingMiner());
        minerList.add(new ChemicalAffectsThroughControlMiner());
        minerList.add(new ControlsTransportOfChemicalMiner());
        minerList.add(new InComplexWithMiner());
        minerList.add(new InteractsWithMiner());
        minerList.add(new NeighborOfMiner());
        minerList.add(new ReactsWithMiner());
        minerList.add(new UsedToProduceMiner());
        minerList.add(new RelatedGenesOfInteractionsMiner());
        minerList.add(new UbiquitousIDMiner());
    }

    for (Miner miner : minerList)
    {
        if (miner instanceof MinerAdapter) ((MinerAdapter) miner).setBlacklist(blacklist);
    }

    return minerList;
}

private Object[] getAvailablePatterns()
{
    List<Miner> minerList =   getAvailableMinerList() ;

    return minerList.toArray(new Object[minerList.size()]);
}
public static void display(String msg) {
    System.out.println("   #-----  "+msg);
    

}

/**
	 * Executes the pattern search in a new thread.
	 */
	private void run(Miner miner, String modelFileName, String outputFileName)
	{
		Thread t = new Thread(new Runnable(){public void run(){mine(miner,modelFileName,outputFileName);}});
		t.start();
	}

	/**
	 * Executes the pattern search.
	 */
  
	private void mine(Miner miner, String modelFileName, String outputFileName)
	{
        Boolean doCustomFile=true;
		if (miner instanceof MinerAdapter){
            ((MinerAdapter) miner).setIDFetcher(new CommonIDFetcher());
        }
			

		// Constructing the pattern before loading any model for a debug friendly code. Otherwise if
		// loading model takes time and an exception occurs in pattern construction, it is just too
		// much wait for nothing.
        miner.getPattern();

		// Prepare progress bar

		ProgressWatcher prg = new ProgressWatcher()
		{
			@Override
			public synchronized  void setTotalTicks(int total)
			{
				//prgBar.setMaximum(total);
			}

			@Override
			public synchronized  void tick(int times)
			{
				//prgBar.setValue(prgBar.getValue() + times);
			}
		};

	

		// Get the model file

		File modFile;

		if (doPC)
		{
			if (getMaxMemory() < 4000)
			{
				display( "Maximum memory not large enough for handling\n" +
					"Pathway Commons data. But will try anyway.\n" +
					"Please consider running this application with the\n" +
					"virtual machine parameter \"-Xmx5G\".");
			}

			modFile = new File(getPCFilename());
			if (!modFile.exists())
			{
				display("Downloading model");
				if (!downloadPC(prg))
				{
				 
					display( 
						"Cannot download Pathway Commons data for some reason. Sorry.");
					return;
				}
				assert modFile.exists();
			}
		}
		else if (doCustomFile)
		{
			modFile = new File(modelFileName);
		}
		else if (doCustomURL)
		{
			String url = urlName.trim();
			display("Downloading model"); 
			if (url.endsWith(".gz")) downloadCompressed(prg, url, "temp.owl", true);
			else if (url.endsWith(".zip")) downloadCompressed(prg, url, "temp.owl", false);
			else downloadPlain(url, "temp.owl");

			modFile = new File("temp.owl");

			if (!modFile.exists())
			{
				display( 
					"Cannot download the model at the given URL.");
		 
				return;
			}
		}
		else
		{
			throw new RuntimeException("Code should not be able to reach here!");
		}

		// Get the output file

		File outFile = new File(outputFileName);

		try
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
			writer.write("x");
			writer.close();
			outFile.delete();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			
			display("Cannot write to file: " + outFile.getPath());
			return;
		}

		// Load model

		display("Loading the model");
		
		SimpleIOHandler io = new SimpleIOHandler();
		Model model;

		try
		{
			model = io.convertFromOWL(new FileInputStream(modFile));
			 
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			 
			display("File not found: " + modFile.getPath());
			return;
		}

		// Search

		Miner min = (Miner) miner;

		Pattern p = min.getPattern();
		
		Map<BioPAXElement,List<Match>> matches = Searcher.search(model, p, prg);

		if (matches.isEmpty())
		{
			display("No results found!");
			try
			{
				BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
				writer.write("");
				writer.close();
				 
			}
			catch (IOException e)
			{
				e.printStackTrace();
				 
			}
			jsonresult("{\"matches\":"+0+"}");

		}
		else
		{
			try
			{
				display("Writing result");
				
				 
				FileOutputStream os = new FileOutputStream(outFile);
				min.writeResult(matches, os);
				os.close();
				 
			}
			catch (IOException e)
			{
				e.printStackTrace();
				 
				display("Error occurred while writing the results");
				return;
			}

			display("Success!    ");
			jsonresult("{\"matches\":"+matches.size()+"}");
			
			 
			
		}
	}



	private boolean downloadCompressed(ProgressWatcher prg, String urlString, String filename,
    boolean gz)
{
    try
    {
        URL url = new URL(urlString);
        URLConnection con = url.openConnection();
        InputStream in = gz ? new GZIPInputStream(con.getInputStream()) :
            new ZipInputStream(con.getInputStream());

        prg.setTotalTicks(con.getContentLength() * 8);

        // Open the output file
        OutputStream out = new FileOutputStream(filename);
        // Transfer bytes from the compressed file to the output file
        byte[] buf = new byte[1024];

        int lines = 0;
        int len;
        while ((len = in.read(buf)) > 0)
        {
            prg.tick(len);
            out.write(buf, 0, len);
            lines++;
        }

        // Close the file and stream
        in.close();
        out.close();

        return lines > 0;
    }
    catch (IOException e)
    {
        e.printStackTrace();
        return false;
    }
}


    private static boolean downloadPlain(String urlString, String file)
	{
		try
		{
			URL url = new URL(urlString);
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();

			// Open the output file
			OutputStream out = new FileOutputStream(file);
			// Transfer bytes from the compressed file to the output file
			byte[] buf = new byte[1024];

			int lines = 0;
			int len;
			while ((len = in.read(buf)) > 0)
			{
				out.write(buf, 0, len);
				lines++;
			}

			// Close the file and stream
			in.close();
			out.close();

			return lines > 0;
		}
		catch (IOException e){return false;}
	}



	private boolean downloadPC(ProgressWatcher prg)
	{
		return downloadCompressed(prg, getPCDataURL(), getPCFilename(), true);
	}


	private String getPCFilename()
	{
		return  "put_release_file_here.owl";
	}
	private String getPCDataURL()
	{
		return PC_DATA_URL_PREFIX + "url_here" + PC_DATA_URL_SUFFIX;


    }


	private int getMaxMemory()
	{
		int total = 0;
		for (MemoryPoolMXBean mpBean: ManagementFactory.getMemoryPoolMXBeans())
		{
			if (mpBean.getType() == MemoryType.HEAP)
			{
				total += mpBean.getUsage().getMax() >> 20;
			}
		}
		return total;
	}


}
