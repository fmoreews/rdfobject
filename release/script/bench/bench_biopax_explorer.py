from biopax_explorer.pattern.rack import Rack
from biopax_explorer.pattern.pattern import PatternExecutor
from biopax_explorer.biopax.utils import gen_utils as gu
import json

print("---start---")


#dataset = "netpath"

#dataset_list=["reactome_mus","reactome_homo","pc12_netpath","pc12_panther","pc12_reactome"]
dataset_list=["reactome_mus","reactome_homo"]
db = "http://db:3030"  

parentClasses=["PhysicalEntity","EntityReference","Interaction"]

sma={}
for cl, children in gu.classes_children().items():
   if cl in parentClasses:
      sma[cl.lower()]=1
 
      for child in children:
         sma[child.lower()]=1
   
selectedClasses=list(sma.keys())

r = Rack()

#pe.verbose() # display logs 
 
def count_uniq(matches,pmetalabels): 
  global selectedClasses
  uniq={}
  for entity_row in matches:
    for entity in entity_row:
      if entity.cls.lower() in selectedClasses:
        if entity.meta_label is not None and entity.meta_label in pmetalabels:
            if entity.pk in uniq.keys():
             uniq[entity.pk]=uniq[entity.pk]+1
            else:
             uniq[entity.pk]=1
  return len(list(uniq.keys()))      

def prp(pe,p,label,pmetalabels):
  res = pe.executePattern(p)

  data={label:{"matches":count_uniq(res,pmetalabels)}}
  
  print(data)
  querylist=pe.queries(p)


  for q in querylist:
      print("#---generated sparql query---\n\n")
      print(q)


  return data
 
report={}
for dataset in dataset_list:
  print("create a Pattern executor for a dataset %s "%(dataset))
  pe = PatternExecutor(db,dataset) # create a Pattern executor for a dataset
  row=[]
  
  data= prp(pe,r.controlsStateChange(),"controls-state-change-of",["C1"])
  row.append(data)
  data= prp(pe,r.stateChange(),"state-change",["C1","c"])
  row.append(data)
  data=prp(pe,r.controlsPhosphorylation(),"controls-state-change-ofphosphorylation",["C1"])
  row.append(data)
  data=prp(pe,r.reactsWith(),"reacts-with",["SMR1"])
  row.append(data)
  data=prp(pe,r.usedToProduce(),"used-to-produce",["BR"])
  row.append(data)
  data=prp(pe,r.controlsExpressionWithTemplateReac(),"controls-expression-of",["Control"])
  row.append(data)
  data=prp(pe,r.inComplexWith(),"in-complex-with",["COMPLEX"])
  row.append(data)
  data=prp(pe,r.controlsMetabolicCatalysis(),"consumption-controlled-by",["Control"])
  row.append(data)
 
  report[dataset]=row

jsonfile="report_bench_BE.json"
with open(jsonfile, 'w') as f:
    json.dump(report, f)

 