import subprocess
import os

def list_files(directory, suffix):
    files_with_suffix = [file for file in os.listdir(directory) if file.endswith(suffix)]
    return files_with_suffix

def compile_class(java_file,  classpath="." ):
    # Compile Java file
    
    compile_command = "javac -cp \"%s\" " %(classpath)
    
    compile_command=compile_command+" %s" %(java_file)
    print("------------------------")
    print(compile_command)
    print("------------------------")
    subprocess.run(compile_command, shell=True, check=True)

    


def create_jar( output_jar ):

    # Create JAR file
    create_jar_command = f"jar cf {output_jar} org"
    subprocess.run(create_jar_command, shell=True, check=True)

    # Clean up generated class files
     #os.remove(f"{os.path.splitext(java_file)[0]}.class")


# Example usage
cwd = os.getcwd()

source_files=list_files("./org/dyliss/", ".java")
print(source_files)
for source_file in source_files:
  java_file = cwd+"/org/dyliss/%s" %(source_file)
  output_jar = cwd+"/paxtool_pattern_cmd.jar"
  classpath = cwd+"/lib/*:"+cwd+"/:*"   # Add your directory with JAR dependencies
  #classpath2 = cwd+"/libcore/* "
  compile_class(java_file, classpath)
create_jar(output_jar)
