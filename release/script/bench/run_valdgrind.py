import subprocess
import re


def run_valgrind(command):
    valgrind_cmd = ['valgrind', '--trace-children=yes', '--tool=memcheck'] + command.split()
    process = subprocess.Popen(valgrind_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    return stdout.decode('utf-8'), stderr.decode('utf-8')




def parse_massif_output(filename):
    with open(filename, 'r') as file:
        data = file.read()

    # Find peak memory usage
    peak_match = re.search(r"mem_heap_B=(\d+)", data)
    if peak_match:
        peak_memory_usage_bytes = int(peak_match.group(1))
        peak_memory_usage_kb = peak_memory_usage_bytes / 1024
        print("Peak Memory Usage:", peak_memory_usage_kb, "KB")

    # Extract other relevant information as needed

 

if __name__ == "__main__":
    command_to_run = "./your_program arg1 arg2"
    massif_output_file = "massif_custom.out"  # Replace with desired filename
    valgrind_stdout, valgrind_stderr = run_valgrind(command_to_run,massif_output_file)
    print("Valgrind stdout:")
    print(valgrind_stdout)
    print("Valgrind stderr:")
    print(valgrind_stderr)

    parse_massif_output(massif_output_file)