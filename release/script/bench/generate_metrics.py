import json
import matplotlib.pyplot as plt
import numpy as np


 

def plot_pattern_by_databases(data_file, pattern):
    # Read JSON data from the file
    with open(data_file, 'r') as f:
        data = json.load(f)
    
    # Initialize the plot
    fig, ax = plt.subplots()

    # Create lists to store the match values for each database and tool
    databases = list(data.keys())
    be_matches = []
    px_matches = []

    # Extract the match values for the specified pattern and databases
    for db in databases:
        be_match = 0
        px_match = 0
        for item in data[db].get('be', []):
            if pattern in item:
                be_match += item[pattern]['matches']
        for item in data[db].get('px', []):
            if pattern in item:
                px_match += item[pattern]['matches']
        be_matches.append(be_match)
        px_matches.append(px_match)

    # Create the bar plot
    bar_width = 0.35
    index = np.arange(len(databases))

    ax.bar(index, be_matches, bar_width, label='be')
    ax.bar(index + bar_width, px_matches, bar_width, label='px')

    # Add labels, title, and legend
    ax.set_xlabel('Databases')
    ax.set_ylabel('Matches')
    ax.set_title(f'Matches for pattern "{pattern}" by Databases and Tools')
    ax.set_xticks(index + bar_width / 2)
    print(databases)
    ax.set_xticklabels(databases)
    ax.legend()

    # Display the plot
    # plt.tight_layout()
    # plt.show()

   # Example usage
data_file = 'table.json'
pattern = "controls-expression-of"
plot_pattern_by_databases(data_file, pattern)


    # Display the plot
#    plt.tight_layout()
#    plt.show()
exfile="result_benchmark.pdf" 
plt.savefig(exfile)

 
