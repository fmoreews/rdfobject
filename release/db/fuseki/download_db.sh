#!/bin/bash
CDIR=$PWD
if [ -f /staging/dbtropes.nt ]
then 
    echo 'Dbtropes data already downloaded.'
else
    echo 'Dbtropes data not found. Downloading...'
    cd /staging
    wget -O dbtropes.zip http://dbtropes.org/static/dbtropes.zip
    unzip dbtropes.zip
    rm dbtropes.zip stat*
    #mv dbtropes.nt /staging/dbtropes.nt
    mv dbtropes*.nt dbtropes.nt
    echo 'Removing broken statement in the dbtropes input file...'
    sed -i '18012846d' /staging/dbtropes.nt
fi

if [ -f /staging/linkedmdb.nt ]
then 
    echo 'Linkedmdb data already downloaded.'
else
    echo 'Linkedmdb data not found. Downloading...'
    wget --no-check-certificate -O /staging/linkedmdb.nt https://query.data.world/s/n5acuyvz4gmvjguqidnz6jjuhjewdn
fi

cd $CDIR

#echo 'Loading dbtropes database'
#$FUSEKI_HOME//load.sh dbtropes /staging/dbtropes.nt

#echo 'Loading linkedmdb database'
#$FUSEKI_HOME//load.sh linkedmdb /staging/linkedmdb.nt


