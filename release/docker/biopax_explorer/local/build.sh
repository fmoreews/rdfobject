#!/bin/bash

IMAGENAME=biopax-explorer-local
rm -rf ./tmp
mkdir ./tmp/
mkdir ./tmp/src_rdfobj
mkdir ./tmp/src_biopax_explorer

cp -r ../../../rdfobject/src ./tmp/src_rdfobj/
cp -r ../../../biopax_explorer/src ./tmp/src_biopax_explorer/

sudo docker build  -t $IMAGENAME  ./
