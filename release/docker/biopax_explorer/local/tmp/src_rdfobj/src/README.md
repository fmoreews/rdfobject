# RDFObject 

## Features

A python library for Object Oriented RDF data manipulation
It includes:

- OWL to python code generation
- Object to Triple Store mapping 
- CRUD features (Create, Read, Update, Delete)
- browsing API based on object model and  SPARQL 

##### Documentation


TODO

##### Installation

TODO

### Source repository

TODO
