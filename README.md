**RDFOBJECT**

![](image/logo.png)

A python library for Object Oriented RDF data manipulation
It includes:

- OWL to python code generation
- Object to Triple Store mapping 
- CRUD features (Create, Read, Update, Delete)
- browsing API based on object model and  SPARQL 



The goal of this library is to facilitate the exploration of large scale RDF dataset.

It works as an ORM like library or Object model to RDF mapper using SPARQL generated queries 


The model represented as python classes is automatically extracted from the OWL specification.

***RoapMap***

We plan to add feature regarding visualization app backend integration 

Most applied features like domain specific queries must be integrated in external  packages.


***Use cases***
Example for system biology : BIOPAX lib generated with RDFOBJECT :

BIOPAX python classes with SPARQL mapping: [rdfobject:biopax library](script/biopax).


![](image/architecture.png)


**Installation**

***Demo environment using jupiter and fuseki***
```
 docker-compose -f  docker-compose.yml build

 docker-compose -f  docker-compose.yml up

 docker-compose exec db bash /fuseki/load_db.sh

```


url : http://localhost:8888/  

password:pass

url :http://localhost:3030


***Quick start***
example of python classes generated from on existing OWL specification, including embdeded CRUD oriented sparql quieries

open the notebook test_rdfobject.ipynb in jupyter 


example of helper python functions to facilite the generation of advanced SPARQL queries :

open the notebook querybox.ipynb in jupyter 

<!-- 2024-06-04 09:27:36 #test-mod 1 lambda x: 2*x**2 + -1*x + 2 -->
